#version 450

in vec3 textureCoordinates;

out vec4 color;

uniform samplerCube cubeMap;
uniform vec3 fogColor;
uniform float fogHigh;
uniform float fogLow;

void main(){

    vec4 diffuse = texture(cubeMap, textureCoordinates);

	float multiplier = clamp((textureCoordinates.y - fogLow) / (fogHigh - fogLow), 0.0, 1.0);

    color = mix(vec4(fogColor, 1.0), diffuse, multiplier);
}