#version 330

layout(location = 0) in vec3 position;

uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

out vec3 textureCoordinates;

void main(void){

    gl_Position = projectionMatrix*viewMatrix*vec4(position, 1.0);
    textureCoordinates = position;
}