#version 450

const int MAX_LIGHTS = 4;

in float fogMultiplier;
in vec2 textureCoordinates;
in vec3 viewDirection;
in vec3 toLight[MAX_LIGHTS];
in mat3 toWorldSpace;

out vec4 color;

layout(binding = 0) uniform sampler2D diffuseTexture;
layout(binding = 1) uniform sampler2D normalTexture;

uniform float reflectivity;
uniform float ambientFactor;
uniform vec3 ambientColor;
uniform vec3 fogColor;
uniform vec3 lightColor[MAX_LIGHTS];
uniform float lightPower[MAX_LIGHTS];
uniform int activeLights;

const vec3 specColor = vec3(1.0, 1.0, 1.0);

void main(){

    vec3 diffuseColor = texture(diffuseTexture, textureCoordinates).rgb;

    vec3 normal = normalize(toWorldSpace*(2.0 * texture(normalTexture, textureCoordinates) - 1.0).rgb);

    vec3 totalColor = diffuseColor * ambientFactor;

    for (int i = 0; i < MAX_LIGHTS && i < activeLights; i++){
        vec3 lightDir = toLight[i];
        float distance = dot(lightDir, lightDir);
        lightDir = normalize(lightDir);

        float lambertian = max(dot(lightDir, normal), 0.0);

        vec3 viewDir = normalize(viewDirection);
        vec3 halfDir = normalize(lightDir+viewDir);
        float specAngle = max(dot(halfDir, normal), 0.0);
        float specular = pow(specAngle, reflectivity);

        vec3 lightContribution = lightColor[i] * lightPower[i] / distance;

        totalColor += (diffuseColor * lambertian + specColor * specular) * lightContribution;
    }

    color = mix(vec4(totalColor, 1.0), vec4(fogColor, 1.0), fogMultiplier);
}