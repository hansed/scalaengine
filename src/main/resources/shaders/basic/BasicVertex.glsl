#version 450

const int MAX_LIGHTS = 4;

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 inTextureCoordinate;
layout(location = 2) in vec3 normal;
layout(location = 3) in vec3 tangent;
layout(location = 4) in vec3 bitangent;

uniform float fogLow;
uniform float fogHigh;
uniform vec3 cameraPosition;
uniform vec3 lightPosition[MAX_LIGHTS];
uniform mat4 modelMatrix;
uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;

out float fogMultiplier;
out vec2 textureCoordinates;
out vec3 viewDirection;
out vec3 toLight[MAX_LIGHTS];
out mat3 toWorldSpace;

float calculateFog(float distance){
	return clamp(1.0 - (fogHigh-distance)/(fogHigh-fogLow), 0.0, 1.0);
}

void main() {

    vec4 worldPosition = modelViewMatrix*vec4(position, 1.0);

    gl_Position = projectionMatrix*worldPosition;

	textureCoordinates = inTextureCoordinate;

	vec3 tang = normalize(vec3(modelMatrix * vec4(tangent, 0.0)));
	vec3 norm = normalize(vec3(modelMatrix * vec4(normal, 0.0)));
	vec3 bitang = normalize(vec3(modelMatrix * vec4(bitangent, 0.0)));

	toWorldSpace = mat3(tang,bitang,norm);

	vec4 vertPos4 = (modelMatrix*vec4(position, 1.0));
	vec3 vertPos = vec3(vertPos4)/vertPos4.w;

	for(int i = 0; i < MAX_LIGHTS; i++){
		toLight[i] = (lightPosition[i] - vertPos);
	}

	viewDirection = cameraPosition-vertPos;
	fogMultiplier = calculateFog(length(viewDirection));

}
