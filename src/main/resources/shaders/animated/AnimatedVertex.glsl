#version 450

const int MAX_JOINTS = 50;
const int MAX_LIGHTS = 4;
const int MAX_WEIGHTS = 3;

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 inTextureCoordinates;
layout(location = 2) in vec3 normal;
layout(location = 3) in vec3 tangent;
layout(location = 4) in vec3 bitangent;
layout(location = 5) in ivec3 joints;
layout(location = 6) in vec3 weights;

uniform float fogLow;
uniform float fogHigh;
uniform vec3 cameraPosition;
uniform vec3 lightPosition[MAX_LIGHTS];
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 jointTransforms[MAX_JOINTS];

out float fogMultiplier;
out vec2 textureCoordinates;
out vec3 viewDirection;
out vec3 toLight[MAX_LIGHTS];
out mat3 toWorldSpace;

float calculateFog(float distance){
    return clamp(1.0 - (fogHigh-distance)/(fogHigh-fogLow), 0.0, 1.0);
}

void main(void){

    mat4 projectionViewMatrix = projectionMatrix*viewMatrix;

    textureCoordinates = inTextureCoordinates;

    vec4 vertPos4 = vec4(0.0);

	vec3 tang = vec3(0.0);
	vec3 norm = vec3(0.0);
	vec3 bitang = vec3(0.0);

	for(int i=0; i<MAX_WEIGHTS; i++){
		mat4 jointTransform = jointTransforms[joints[i]];
		vec4 posePosition = jointTransform*vec4(position, 1.0);
		vertPos4 += posePosition * weights[i];

        tang += vec3(jointTransform * vec4(tangent, 0.0)) * weights[i];
		norm += vec3(jointTransform * vec4(normal, 0.0)) * weights[i];
		bitang += vec3(jointTransform * vec4(bitangent, 0.0)) * weights[i];
	}

    tang = tang;
    bitang = bitang;
    norm = norm;

	toWorldSpace = mat3(tang, bitang, norm);

    vec3 vertPos = vec3(vertPos4)/vertPos4.w;


    for(int i = 0; i < MAX_LIGHTS; i++){
        toLight[i] = (lightPosition[i] - vertPos);
    }

    viewDirection = cameraPosition-vertPos;
    fogMultiplier = calculateFog(length(viewDirection));
    gl_Position = projectionViewMatrix*vertPos4;
}