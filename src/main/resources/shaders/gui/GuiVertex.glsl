#version 330

in vec2 position;

uniform mat4 transformationMatrix;

out vec2 textureCoords;

void main() {

    gl_Position = transformationMatrix*vec4(position, 0.0, 1.0);

    textureCoords = vec2((position.x)/2.0, 1.0 - (position.y)/2.0);
}