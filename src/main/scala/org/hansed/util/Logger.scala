package org.hansed.util

import java.io.{BufferedWriter, FileOutputStream, OutputStreamWriter}

import org.hansed.engine.Flags
import org.hansed.engine.Flags.DEBUG
import org.hansed.engine.util.timers.{Timer, Timers}
import org.hansed.util.PrintColors.PrintColor

/**
  * Contains method used for debugging code such as logging
  *
  * @author Hannes Sederholm
  */
object Logger {

  Timers.add(new Timer(() => {
    out.flush()
  }))

  val out = new BufferedWriter(new OutputStreamWriter(new
      FileOutputStream(java.io.FileDescriptor.out), "ASCII"), 2048)

  def print(message: String, color: PrintColor): Unit = {
    out.write(color.value + message)
  }

  /**
    * Print an error message if debug (or condition) evaluates to true
    *
    * @param message   the message
    * @param source    the source of the message
    * @param condition the condition that needs to apply for the message to be printed
    */
  def err(message: String, source: Any, condition: () => Boolean = () => Flags(DEBUG)): Unit = {
    log(message, source, condition, PrintColors.RED)
    //flush the output after error as a crash is very much possible
    out.flush()
  }


  /**
    * Print a warning message if debug (or condition) evaluates to true
    *
    * @param message   the message
    * @param source    the source of the message
    * @param condition the condition that needs to apply for the message to be printed
    */
  def warn(message: String, source: Any, condition: () => Boolean = () => Flags(DEBUG)): Unit = {
    log(message, source, condition, PrintColors.YELLOW)
  }

  /**
    * Print a debug message if debug is enabled
    *
    * @param message the message
    * @param source  source of the message
    */
  def log(message: String, source: Any, condition: () => Boolean = () => Flags(DEBUG),
          color: PrintColors.PrintColor = PrintColors.CYAN): Unit = {
    if (condition()) {
      print(s"[$source] $message\n", color)
    }
  }

  /**
    * Log a given function with a message and output of time taken
    *
    * @param f      the function
    * @param prefix message
    * @param source the source of the function for debug printout
    * @tparam T return type of the function
    * @return the result of the function
    */
  def logAction[T](prefix: String, source: Any, condition: () => Boolean = () => Flags(DEBUG),
                   color: PrintColors.PrintColor = PrintColors.CYAN)(f: => T): T = {
    val startTime = System.currentTimeMillis()
    log("Start " + prefix, source, condition)
    val result = f
    val stopTime = System.currentTimeMillis()
    log("Finish " + prefix + " took " + (stopTime - startTime) + " ms", source, condition)
    result
  }

  /**
    * Log a given function with a message and output of time taken
    *
    * @param f      the function
    * @param prefix message
    * @param source the source of the function for debug printout
    * @tparam T return type of the function
    * @return the result of the function
    */
  def logTime[T](prefix: String, source: Any, condition: () => Boolean = () => Flags(DEBUG))(f: => T): T = {
    val startTime = System.currentTimeMillis()
    val result = f
    val stopTime = System.currentTimeMillis()
    log(prefix + " took " + (stopTime - startTime) + " ms", source, condition)
    result
  }

}
