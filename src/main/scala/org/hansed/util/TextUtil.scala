package org.hansed.util

object TextUtil {

  /**
    * Construct a split array off arbitrary formatted text
    *
    * @param uncleanArray input
    * @return split text
    */
  def getArray(uncleanArray: String): Array[String] = {
    uncleanArray.replaceAll(" +", " ").replaceAll("\n", "").trim.split(" ")
  }

}
