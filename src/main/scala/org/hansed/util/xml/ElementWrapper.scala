package org.hansed.util.xml

import org.hansed.util.TextUtil.getArray
import org.jdom2.Element

import scala.collection.mutable
import scala.jdk.CollectionConverters._

/**
  * Element wrapper for [[org.jdom2.Element]]
  *
  * Provides a more convenient access to its methods
  *
  * @author Hannes Sederholm
  */
case class ElementWrapper(element: Element) {

  def getChild(name: String, inputID: String): ElementWrapper = {
    val id = if (inputID.startsWith("#")) inputID.replaceFirst("#", "")
    else inputID
    getChildren(name).filter((child: ElementWrapper) => child.id.equalsIgnoreCase(id)).head
  }

  def getChildWithIdContaining(name: String, inputID: String): ElementWrapper = {
    val id = (if (inputID.startsWith("#")) inputID.replaceFirst("#", "")
    else inputID).toLowerCase
    getChildren(name).filter((child: ElementWrapper) => child.id.toLowerCase.contains(id)).head
  }


  def getChild(name: String): ElementWrapper = {
    element.getChildren.asScala.filter(_.getName.equalsIgnoreCase(name)).map(ElementWrapper).head
  }

  def getChildOption(name: String): Option[ElementWrapper] = {
    element.getChildren.asScala.filter(_.getName.equalsIgnoreCase(name)).map(ElementWrapper).headOption
  }

  def getChildren(name: String): mutable.Buffer[ElementWrapper] = {
    element.getChildren.asScala.filter(_.getName.equalsIgnoreCase(name)).map(ElementWrapper)
  }

  def textArray: Array[String] = getArray(element.getText)

  def id: String = element.getAttributeValue("id")

  def attribute(attribute: String): String = element.getAttributeValue(attribute)
}
