package org.hansed.util

import org.hansed.engine.Engine
import org.hansed.engine.rendering.resources.VaoResource
import org.lwjgl.opengl.GL11._
import org.lwjgl.opengl.GL13._
import org.lwjgl.opengl.GL15.{GL_ARRAY_BUFFER, GL_STATIC_DRAW, glBindBuffer, glBufferData}
import org.lwjgl.opengl.GL20.glVertexAttribPointer
import org.lwjgl.opengl.GL30.glVertexAttribIPointer

/**
 * Contains utility methods for dealing with different OpenGL features
 *
 * @author Hannes Sederholm
 */
object GLUtil {

  /**
   * Attempt to capture / release the cursor
   *
   * @param captured whether or not the cursor is to be captured
   */
  def captureCursor(captured: Boolean): Unit = {
    Engine.window.foreach(_.captureCursor(captured))
  }

  def multisampling(value: Boolean): Unit = {
    property(GL_MULTISAMPLE, value)
  }

  def depthTest(value: Boolean): Unit = {
    property(GL_DEPTH_TEST, value)
  }

  def culling(value: Boolean): Unit = {
    property(GL_CULL_FACE, value)
    glCullFace(GL_BACK)
  }

  private def property(property: Int, toggle: Boolean): Unit = {
    if (toggle) {
      glEnable(property)
    } else
      glDisable(property)
  }

  /**
   * Store some data in a given attribute array
   *
   * @param resource  VAO resource
   * @param attribute attribute array index
   * @param dimension dimension of data vector
   * @param data      data
   */
  def storeAttribute(resource: VaoResource, attribute: Int, dimension: Int, data: Array[Float]): Unit = {
    glBindBuffer(GL_ARRAY_BUFFER, resource.vbo(attribute))
    glBufferData(GL_ARRAY_BUFFER, data, GL_STATIC_DRAW)
    glVertexAttribPointer(attribute, dimension, GL_FLOAT, false, 0, 0)
  }

  /**
   * Store some data in a given attribute array
   *
   * @param resource  VAO resource
   * @param attribute attribute array index
   * @param dimension dimension of data vector
   * @param data      data
   */
  def storeAttribute(resource: VaoResource, attribute: Int, dimension: Int, data: Array[Int]): Unit = {
    glBindBuffer(GL_ARRAY_BUFFER, resource.vbo(attribute))
    glBufferData(GL_ARRAY_BUFFER, data, GL_STATIC_DRAW)
    glVertexAttribIPointer(attribute, dimension, GL_INT, 0, 0)
  }

}
