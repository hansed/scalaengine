package org.hansed.util

object PrintColors {

  case class PrintColor(code: Int) {
    val value = s"${27.toChar}[${code}m"
  }

  val RED: PrintColor = PrintColor(91)
  val CYAN: PrintColor = PrintColor(96)
  val YELLOW: PrintColor = PrintColor(93)

}
