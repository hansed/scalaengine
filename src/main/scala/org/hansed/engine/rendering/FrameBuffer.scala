package org.hansed.engine.rendering

import java.nio.ByteBuffer

import org.hansed.engine.Engine
import org.hansed.engine.core.resources.ResourceUser
import org.hansed.engine.rendering.resources.FrameBufferResource
import org.lwjgl.opengl.GL11._
import org.lwjgl.opengl.GL30._
import org.lwjgl.opengl.GL32._

/**
  * Represents an OpenGL frame buffer object
  *
  * @see [[https://www.khronos.org/opengl/wiki/Framebuffer_Object]] for more information
  * @param name   name of the buffer
  * @param width  width of buffer in pixels
  * @param height height of buffer in pixels
  * @author Hannes Sederholm
  */
class FrameBuffer(name: String, width: Int, height: Int) extends ResourceUser[FrameBufferResource](name) {

  /**
    * Loads the resource
    * Called when no cached resource was found
    *
    * @param key resource key
    * @return resource wrapped in Option
    */
  override def loadResource(key: String): Option[FrameBufferResource] = {
    val res = new FrameBufferResource(key)

    //create the buffer
    glBindFramebuffer(GL_FRAMEBUFFER, res.frameBufferId)
    glDrawBuffer(GL_COLOR_ATTACHMENT0)


    //color texture
    glBindTexture(GL_TEXTURE_2D, res.textureId)
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, null.asInstanceOf[ByteBuffer])
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, res.textureId, 0)

    //depth texture
    glBindTexture(GL_TEXTURE_2D, res.depthTextureId)
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, null
      .asInstanceOf[ByteBuffer])
    glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, res.depthTextureId, 0)

    glBindFramebuffer(GL_FRAMEBUFFER, 0)
    val windowSize = Engine.windowSize
    glViewport(0, 0, windowSize._1, windowSize._2)

    Some(res)
  }

  /**
    * Set this resource as current, whatever that might mean depends on the type of the resource
    */
  override def bind(): Unit = {
    glBindFramebuffer(GL_FRAMEBUFFER, resource.frameBufferId)
    glViewport(0, 0, width, height)
  }

  /**
    * Return from what bind does to the default state
    * Note that if the system related to the resource wasn't at default state before bind the state will be different
    */
  override def unbind(): Unit = {
    glBindFramebuffer(GL_FRAMEBUFFER, 0)
    val windowSize = Engine.windowSize
    glViewport(0, 0, windowSize._1, windowSize._2)
  }
}
