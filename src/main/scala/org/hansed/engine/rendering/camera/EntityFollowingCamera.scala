package org.hansed.engine.rendering.camera

import org.hansed.engine.core.eventSystem.events.Event
import org.hansed.engine.core.eventSystem.events.InputEvents.{KeyHold, MouseScroll}
import org.hansed.engine.core.eventSystem.{EventListener, Events}
import org.hansed.engine.core.{GameComponent, Transform}
import org.hansed.engine.rendering.RenderingEvents.{EntityCameraUpdate, FocusEntityCamera}
import org.joml.{Matrix4f, Quaternionf, Vector3f}
import org.lwjgl.glfw.GLFW._

/**
  * A camera that follows a game object
  *
  * @param projection camera projection
  * @param radius     radius that the camera moves around
  * @author Hannes Sederholm
  */
class EntityFollowingCamera(projection: Matrix4f, var radius: Float = 5.0f, var controllable: Boolean = false,
                            zoomable: Boolean = true, rotationSpeed: Float = 0.05f)
  extends Camera(projection) with EventListener {

  /**
    * What the camera is looking at
    */
  var target: Option[Transform[_]] = None

  /**
    * The horizontal movement around a sphere around the target object
    *
    * 0 is right behind the target
    */
  var verticalSpin: Float = 0.0f

  /**
    * Whether or not the camera should update itself
    */
  var autoUpdate: Boolean = controllable

  /**
    * The vertical movement around a sphere around the target object
    *
    * 0 is at the level of the target
    */
  var horizontalSpin: Float = 0f

  addComponent(new GameComponent {
    /**
      * Called once every update tick for all components
      *
      * Because the camera is usually added before other elements its likely to be updated before the component its
      * following is updated. This causes unexpected behaviour when following the target as its always "one step ahead"
      * Therefore we pass the update request to the events stack as its processed after all the entities are updated
      */
    override def update(): Unit = {
      if (autoUpdate) Events.signal(EntityCameraUpdate())
    }
  })

  private val PI: Float = Math.PI.toFloat

  /**
    * Update the position of the camera relative to target entity
    */
  def updateCamera(): Unit = {

    target.foreach(target => {

      val radiusVector =
        new Vector3f(0, 0, radius)
          .rotate(new Quaternionf(target.rotation).rotateAxis(horizontalSpin, Transform.X_AXIS))
          .rotate(new Quaternionf(target.rotation).rotateAxis(verticalSpin, Transform.Y_AXIS))
          .rotate(new Quaternionf(target.rotation).invert())

      val upVector = Transform.Y_AXIS.rotate(target.rotation, new Vector3f())

      position = radiusVector.add(target.position)

      val matrix = new Matrix4f().lookAt(new Vector3f(position), target.position, upVector).invert()

      localRotation.set(matrix.getNormalizedRotation(new Quaternionf()))
    })
  }

  /**
    * Deal with an event
    *
    * @param event an event sent by an EventDispatcher
    */
  override def processEvent(event: Event): Unit = {
    event match {
      case event: FocusEntityCamera =>
        target = Some(event.target)
        autoUpdate = event.autoUpdate || controllable
      case _: EntityCameraUpdate =>
        updateCamera()
      case event: KeyHold =>
        if (controllable) {
          event.keyCode match {
            case GLFW_KEY_UP =>
              horizontalSpin = Math.max(horizontalSpin - rotationSpeed, -PI / 2.0f * 0.99f)
            case GLFW_KEY_DOWN =>
              horizontalSpin = Math.min(horizontalSpin + rotationSpeed, PI / 2.0f * 0.99f)
            case GLFW_KEY_LEFT =>
              verticalSpin += rotationSpeed
            case GLFW_KEY_RIGHT =>
              verticalSpin -= rotationSpeed
            case _ =>
          }
        }
      case event: MouseScroll =>
        if (zoomable)
          radius -= event.amount
      case _ =>
    }
  }
}
