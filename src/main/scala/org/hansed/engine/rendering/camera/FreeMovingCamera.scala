package org.hansed.engine.rendering.camera

import org.hansed.engine.physics.movement.FreeKeyboardMovement
import org.joml.Matrix4f

/**
  * A camera that may be moved freely
  *
  * @param projection camera projection
  * @author Hannes Sederholm
  */
class FreeMovingCamera(projection: Matrix4f) extends Camera(projection) {

  addComponent(new FreeKeyboardMovement)

}
