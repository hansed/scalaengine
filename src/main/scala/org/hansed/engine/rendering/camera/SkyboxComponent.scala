package org.hansed.engine.rendering.camera

import org.hansed.engine.core.{GameComponent, GameObject}
import org.hansed.engine.rendering.models.stationary.Skybox
import org.hansed.engine.rendering.shaders.SkyboxShader

/**
  * A component that renders a skybox around the camera
  *
  * @param texturePath the path to the folder where the skybox parts reside
  * @author Hannes Sederholm
  */
class SkyboxComponent(val texturePath: String) extends GameComponent {

  /**
    * The skybox to render
    */
  val skybox: Skybox = new Skybox(texturePath)

  /**
    * Called when the component is created. Should be used for initiating the component
    *
    * @param parent the parent object of this component
    */
  override def prepare(parent: GameObject): Unit = {
    parent.shaders += SkyboxShader.key
  }

  /**
    * Called once every update tick for all components
    */
  override def update(): Unit = {

  }

}
