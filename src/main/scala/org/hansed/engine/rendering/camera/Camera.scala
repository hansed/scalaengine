package org.hansed.engine.rendering.camera

import org.hansed.engine.physics.PhysicsObject
import org.joml.Matrix4f

/**
  * Represents a camera that a player is looking through
  *
  * @param projection camera projection
  * @author Hannes Sederholm
  */
class Camera(val projection: Matrix4f) extends PhysicsObject() {

  /**
    * Constructs the view projection of this camera
    *
    * We don't actually move the camera in the world, but we move the world around the camera, so to get the view
    * matrix we do the transforms that have been applied to the camera and do them backwards
    *
    * @return view matrix
    */
  def viewMatrix: Matrix4f = matrix.invert()
}

object Camera {

  /**
    * Create a projection matrix with default values
    *
    * @param windowWidth  the width of the view
    * @param windowHeight the height of the view
    * @return new camera
    */
  def perspective(windowWidth: Float, windowHeight: Float, fov: Float = 45.0f): Matrix4f = {
    new Matrix4f().perspective(Math.toRadians(fov).toFloat, windowWidth /
      windowHeight, 0.01f, 500.0f)
  }

}