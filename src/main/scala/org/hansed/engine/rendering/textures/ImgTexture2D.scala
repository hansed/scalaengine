package org.hansed.engine.rendering.textures

import org.hansed.engine.core.resources.ResourceLoader
import org.lwjgl.opengl.GL11._
import org.lwjgl.opengl.GL30.glGenerateMipmap

/**
  * Represents a normal 2D texture resource
  *
  * @param key the path to image
  * @author Hannes Sederholm
  */
class ImgTexture2D(key: String)
  extends ImgTextureResource(key, () => Array(ResourceLoader.readImageToByteBuffer(key))) {

  override def updateResource(): Unit = {
    glBindTexture(GL_TEXTURE_2D, textureID)
    val image = images.head
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.width, image.height, 0, GL_RGBA,
      GL_UNSIGNED_BYTE, image.buffer)

    glGenerateMipmap(GL_TEXTURE_2D)

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
    glBindTexture(GL_TEXTURE_2D, 0)
  }
}
