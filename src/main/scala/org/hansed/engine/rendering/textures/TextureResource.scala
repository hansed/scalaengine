package org.hansed.engine.rendering.textures

import org.hansed.engine.core.resources.Resource
import org.lwjgl.opengl.GL11.{glDeleteTextures, glGenTextures}

/**
  * Texture resource contains a handle for an OpenGL texture sampler
  *
  * @author Hannes Sederholm
  */
class TextureResource(key: String) extends Resource(key) {

  private val _textureID: Int = glGenTextures()

  def textureID: Int = _textureID

  /**
    * Free the resources reserved for this resource
    */
  override def clearResource(): Unit = {
    glDeleteTextures(textureID)
  }
}
