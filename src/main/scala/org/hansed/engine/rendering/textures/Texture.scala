package org.hansed.engine.rendering.textures

import org.hansed.engine.core.resources.ResourceUser
import org.lwjgl.opengl.GL11._
import org.lwjgl.opengl.GL13.{GL_TEXTURE0, glActiveTexture}

/**
  * Represents a texture that can be loaded to the gpu
  *
  * @param path path to texture file
  * @author Hannes Sederholm
  */
class Texture(path: String) extends ResourceUser[ImgTexture2D](path) {

  /**
    * Bind the texture on a given texture slot
    *
    * @param slot id
    */
  def bind(slot: Int = GL_TEXTURE0): Unit = {
    glActiveTexture(slot)
    bind()
  }

  /**
    * Bind this texture on a current active texture slot
    */
  override def bind(): Unit = {
    glBindTexture(GL_TEXTURE_2D, resource.textureID)
  }

  /**
    * Return from what bind does to the default state
    * Note that if the system related to the resource wasn't at default state before bind the state will be different
    */
  override def unbind(): Unit = {
    glBindTexture(GL_TEXTURE_2D, 0)
  }

  /**
    * Loads the resource
    * Called when no cached resource was found
    *
    * @param key resource key
    * @return resource wrapped in Option
    */
  override def loadResource(key: String): Option[ImgTexture2D] = Some(new ImgTexture2D(path))
}

