package org.hansed.engine.rendering.textures

import org.hansed.engine.core.resources.ResourceLoader.ByteBufferImage

import scala.concurrent.Future

/**
  * A texture that concurrently loads an Image from disk
  *
  * @param key       the path to image
  * @param imgLoader the method that returns a list of images, will be executed on a different thread
  * @author Hannes Sederholm
  */
abstract class ImgTextureResource(key: String, imgLoader: () => Array[ByteBufferImage]) extends TextureResource(key) {

  def loaded: Boolean = futureImages.isCompleted

  var images: Array[ByteBufferImage] = _

  import scala.concurrent.ExecutionContext.Implicits.global

  val futureImages: Future[Unit] = Future({
    images = imgLoader()
  })

  var loadComplete = false

  override def textureID: Int = {
    if (loaded && !loadComplete) {
      loadComplete = true
      updateResource()
    }
    super.textureID
  }

  def updateResource(): Unit
}
