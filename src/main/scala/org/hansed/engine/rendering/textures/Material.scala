package org.hansed.engine.rendering.textures

import org.hansed.engine.saving.DefaultSaveProtocol.MaterialLoader
import org.hansed.engine.saving.MaterialDefinition

/**
  * A material that describes what a surface looks like
  *
  * @param config the config of this material
  */
class Material(config: MaterialDefinition) {

  def this(configPath: String) = {
    this(MaterialLoader.config(configPath))
  }

  def reflectivity: Float = config.reflectivity

  /**
    * The diffuse colors of this material
    */
  val diffuse = new Texture(config.diffuse)

  /**
    * Normal map for this material
    */
  val normal = new Texture(config.normalPath)

}
