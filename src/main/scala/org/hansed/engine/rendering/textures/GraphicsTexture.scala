package org.hansed.engine.rendering.textures

import java.awt.Graphics2D
import java.awt.image.BufferedImage
import java.util.UUID

import org.hansed.engine.core.resources.{ResourceLoader, ResourceUser}
import org.lwjgl.opengl.GL11._
import org.lwjgl.opengl.GL13.glActiveTexture
import org.lwjgl.opengl.GL30.glGenerateMipmap

/**
  * Represents a texture that may be edited using [[java.awt.Graphics2D]]
  *
  * @author Hannes Sederholm
  */
class GraphicsTexture(width: Int, height: Int) extends ResourceUser[TextureResource](UUID.randomUUID().toString) {

  /**
    * The image used for drawing
    */
  val image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB)

  //update _

  /**
    * Update the graphics of this texture
    *
    * @param drawFunction function that does the updating
    */
  def update(drawFunction: Graphics2D => Unit): Unit = {
    val graphics = image.createGraphics()
    drawFunction(graphics)
    graphics.dispose()
    val textureImage = ResourceLoader.convertImageToByteBuffer(image)
    bind()
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, textureImage.width, textureImage.height, 0, GL_RGBA,
      GL_UNSIGNED_BYTE, textureImage.buffer)

    glGenerateMipmap(GL_TEXTURE_2D)

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
  }

  /**
    * Bind this texture to a given texture slot
    *
    * @param slot sampler slot
    */
  def bind(slot: Int): Unit = {
    glActiveTexture(slot)
    bind()
  }

  /**
    * Set this resource as current, whatever that might mean depends on the type of the resource
    */
  override def bind(): Unit = glBindTexture(GL_TEXTURE_2D, resource.textureID)

  /**
    * Return from what bind does to the default state
    * Note that if the system related to the resource wasn't at default state before bind the state will be different
    */
  override def unbind(): Unit = glBindTexture(GL_TEXTURE_2D, 0)

  /**
    * Loads the resource
    * Called when no cached resource was found
    *
    * @param key resource key
    * @return resource wrapped in Option
    */
  override def loadResource(key: String): Option[TextureResource] = Some(new TextureResource(key))

}
