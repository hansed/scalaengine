package org.hansed.engine.rendering.textures

import org.hansed.engine.core.resources.ResourceLoader.ByteBufferImage
import org.hansed.engine.core.resources.{ResourceLoader, ResourceUser}
import org.hansed.engine.rendering.textures.CubeMapTexture._
import org.lwjgl.opengl.GL11._
import org.lwjgl.opengl.GL12.{GL_CLAMP_TO_EDGE, GL_TEXTURE_WRAP_R}
import org.lwjgl.opengl.GL13.{GL_TEXTURE0, GL_TEXTURE_CUBE_MAP, GL_TEXTURE_CUBE_MAP_POSITIVE_X, glActiveTexture}

import scala.concurrent.Future

/**
  * A texture that consists of 6 sides that together create a cube
  *
  * @param path path to texture
  * @author Hannes Sederholm
  */
class CubeMapTexture(path: String) extends ResourceUser[TextureResource](path) {

  def loaded: Boolean = futureImages.isCompleted

  var images: Array[ByteBufferImage] = _

  import scala.concurrent.ExecutionContext.Implicits.global

  val futureImages: Future[Unit] = Future({
    images = PARTS.map(part => ResourceLoader.readImageToByteBuffer("textures/cubeMaps/" + path + "/" + path + "_"
      + part + ".png"))
  })

  var loadComplete = false

  /**
    * Load the cube map texture
    */
  def loadTexture(): Unit = {
    bind()
    images.zipWithIndex.foreach({ case (image, index) =>
      glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + index, 0, GL_RGBA, image.width, image.height, 0,
        GL_RGBA, GL_UNSIGNED_BYTE, image.buffer)
    })
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE)
    unbind()
  }

  /**
    * Bind the cube map
    *
    * @param slot texture slot to bind to
    */
  def bind(slot: Int = GL_TEXTURE0): Unit = {
    if (loaded && !loadComplete) {
      loadComplete = true
      loadTexture()
    }
    glActiveTexture(slot)
    glBindTexture(GL_TEXTURE_CUBE_MAP, resource.textureID)
  }

  /**
    * Bind the cube map to currently active texture slot
    */
  override def bind(): Unit = {
    bind(GL_TEXTURE0)
  }

  /**
    * Unbind the cube map
    */
  override def unbind(): Unit = glBindTexture(GL_TEXTURE_CUBE_MAP, 0)

  /**
    * Loads the resource
    * Called when no cached resource was found
    *
    * @param key resource key
    * @return resource wrapped in Option
    */
  override def loadResource(key: String): Option[TextureResource] = {
    Some(new TextureResource(key))
  }
}

object CubeMapTexture {

  val PARTS: Array[String] = Array[String]("RIGHT", "LEFT", "TOP", "BOTTOM", "BACK", "FRONT")
}
