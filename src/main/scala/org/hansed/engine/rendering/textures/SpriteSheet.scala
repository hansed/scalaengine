package org.hansed.engine.rendering.textures

import java.awt.image.BufferedImage

import org.hansed.engine.core.resources.ResourceUser
import org.hansed.engine.rendering.resources.ImageResource

/**
  * A sprite sheet of sprites contained in an image
  *
  * @param imagePath path to the image file
  * @author Hannes Sederholm
  */
class SpriteSheet(imagePath: String, spriteWidth: Int, spriteHeight: Int, rowLength: Int, xOffset: Int = 0, yOffset: Int = 0) extends ResourceUser[ImageResource](imagePath) {

  /**
    * Get a single sprite from the sprite sheet
    *
    * @param index   index of the sprite counting from 0 to rowLength*rowLength
    * @param colSpan how many columns from the index does the sprite span
    * @param rowSpan how many rows from the index does the sprite span
    * @return a sub image that is the sprite desired
    */
  def apply(index: Int, colSpan: Int = 1, rowSpan: Int = 1): BufferedImage = {
    resource.image.getSubimage(
      (index % rowLength) * (spriteWidth + xOffset), (index / rowLength) * (spriteHeight + yOffset),
      colSpan * spriteWidth, rowSpan * spriteHeight)
  }

  /**
    * Loads the resource
    * Called when no cached resource was found
    *
    * @param key resource key
    * @return resource wrapped in Option
    */
  override def loadResource(key: String): Option[ImageResource] = {
    Some(new ImageResource(key))
  }

  /**
    * Set this resource as current, whatever that might mean depends on the type of the resource
    */
  override def bind(): Unit = {
    //Nothing
  }

  /**
    * Return from what bind does to the default state
    * Note that if the system related to the resource wasn't at default state before bind the state will be different
    */
  override def unbind(): Unit = {
    //Nothing
  }
}
