package org.hansed.engine.rendering.shaders

import org.hansed.engine.Engine
import org.hansed.engine.core.GameObject
import org.hansed.engine.rendering.models.stationary.ModelComponent
import org.joml.Vector3f

/**
  * A shader for rendering the hit boxes of objects
  * Encodes the id of the objects into the color of the hit box
  * Hit boxes here means click areas of the objects
  *
  * @author Hannes Sederholm
  */
object HitBoxShader extends Shader[ModelComponent]("HitBoxShader", "shaders/basic/BasicVertex.glsl",
  "shaders/hitBox/HitBoxFragment.glsl") {

  override def render(renderer: ModelComponent): Unit = {

    if (renderer.model.resource.futureShape.isCompleted) {
      setUniform("modelMatrix", renderer.matrix)
      setUniform("modelViewMatrix", Engine.camera.viewMatrix.mul(renderer.matrix))
      setUniform("projectionMatrix", Engine.camera.projection)
      //encode the parents id into the color rendered
      renderer.parent.foreach(parent => {

        val i = parent.clickId
        val r = i & 0x000000FF
        val g = (i & 0x0000FF00) >> 8
        val b = (i & 0x00FF0000) >> 16

        setUniform("boxColor", new Vector3f(r / 255.0f, g / 255.0f, b / 255.0f))
        renderer.model.draw()
      })
    }
  }

  override protected def collectData(o: GameObject): Option[ModelComponent] = {
    //if this object isn't clickable there is no point drawing it
    if (o.clickId == -1) None else {
      o.findComponent[ModelComponent]
    }
  }
}
