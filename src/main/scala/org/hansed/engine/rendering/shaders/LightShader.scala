package org.hansed.engine.rendering.shaders

import org.hansed.engine.core.{GameComponent, GameObject}
import org.hansed.engine.rendering.RenderEngine
import org.hansed.engine.rendering.lights.PointLight
import org.joml.Vector3f

import scala.collection.mutable

/**
  * An abstract super class for a shader that cares about the lights shining on the render components
  *
  * @param key          name of the shader
  * @param vertexPath   path to vertex shader source code
  * @param fragmentPath path to fragment shader source code
  * @tparam T the type of the render component
  * @author Hannes Sederholm
  */
abstract class LightShader[T <: GameComponent](key: String, vertexPath: String, fragmentPath: String)
  extends Shader[T](key, vertexPath, fragmentPath) {

  private val lights: mutable.Map[Int, Seq[PointLight]] = mutable.HashMap()
  private val positions: mutable.Map[Int, Vector3f] = mutable.HashMap()

  override def includeObject(o: GameObject): Unit = {
    super.includeObject(o)
    updateLights(o.uniqueId)
  }


  override def clearObjects(): Unit = {
    super.clearObjects()
    lights.clear()
    positions.clear()
  }

  /**
    * Remove object from rendering
    */
  override def removeObject(o: GameObject): Unit = {
    super.removeObject(o)
    lights.remove(o.uniqueId)
    positions.remove(o.uniqueId)
  }

  def updateAllLights(): Unit = {
    renderData.keys.foreach(updateLights)
  }

  def updateLights(objectId: Int): Unit = {
    import scala.Ordering.Float.TotalOrdering
    renderData.get(objectId).foreach(renderer => {
      val rendererPosition = new Vector3f(renderer.position)
      positions.put(objectId, rendererPosition)
      lights.put(objectId, RenderEngine.lights.toSeq.sortBy(light => {
        -light.power / rendererPosition.distanceSquared(light.position)
      }).take(RenderEngine.MAX_LIGHTS))
    })
  }

  def getLights(objectId: Int): Seq[PointLight] = {
    positions.get(objectId).foreach(position => {
      renderData.get(objectId).foreach(r => {
        if (!position.equals(r.position)) {
          updateLights(objectId)
        }
      })
    })
    lights.getOrElse(objectId, Seq())
  }

}
