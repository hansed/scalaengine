package org.hansed.engine.rendering.shaders

import org.hansed.engine.Engine
import org.hansed.engine.core.GameObject
import org.hansed.engine.rendering.models.stationary.ModelComponent
import org.lwjgl.opengl.GL13.{GL_TEXTURE0, GL_TEXTURE1}

/**
  * A shader for rendering static models
  *
  * @author Hannes Sederholm
  */
object StaticShader extends LightShader[ModelComponent]("StaticShader", "shaders/basic/BasicVertex.glsl",
  "shaders/basic/BasicFragment.glsl") {


  override protected def collectData(o: GameObject): Option[ModelComponent] = {
    o.findComponent[ModelComponent]
  }

  /**
    * Prepare the shader for rendering given component
    *
    * @param renderer about to be rendered
    */
  override def render(renderer: ModelComponent): Unit = {
    if (renderer.model.resource.futureShape.isCompleted) {
      setUniform("modelMatrix", renderer.matrix)
      setUniform("modelViewMatrix", Engine.camera.viewMatrix.mul(renderer.matrix))
      setUniform("projectionMatrix", Engine.camera.projection)
      setUniform("reflectivity", renderer.material.reflectivity)

      val id = renderer.parent.map(_.uniqueId).getOrElse(0)

      val lights = getLights(id)
      setUniform("activeLights", lights.size)
      var index = 0
      lights.foreach(light
      => {
        setUniform(s"lightPosition[$index]", light.position)
        setUniform(s"lightColor[$index]", light.color)
        setUniform(s"lightPower[$index]", light.power)
        index += 1
      }
      )

      renderer.material.diffuse.bind(GL_TEXTURE0)
      renderer.material.normal.bind(GL_TEXTURE1)
      renderer.model.draw()
    }
  }

  /**
    * Set this shader as the active OpenGL shader
    */
  override def bind(): Unit = {
    super.bind()
    //because the skybox shouldn't change during the render call just set the uniforms once
    val config = Engine.scene.atmosphere
    setUniform("fogColor", config.fogColor)
    setUniform("fogLow", config.terrainFogLow)
    setUniform("fogHigh", config.terrainFogHigh)

    setUniform("ambientFactor", config.ambientFactor)

    setUniform("cameraPosition", Engine.camera.position)
  }
}