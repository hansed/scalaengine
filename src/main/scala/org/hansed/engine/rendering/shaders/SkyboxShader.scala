package org.hansed.engine.rendering.shaders

import org.hansed.engine.Engine
import org.hansed.engine.core.GameObject
import org.hansed.engine.rendering.camera.SkyboxComponent

/**
  * A shader for drawing skyboxes
  *
  * @author Hannes Sederholm
  */
object SkyboxShader extends Shader[SkyboxComponent]("SkyboxShader", "shaders/skybox/SkyboxVertex.glsl",
  "shaders/skybox/SkyboxFragment.glsl") {

  /**
    * Prepare the shader for rendering given component
    *
    * @param component about to be rendered
    */
  override def render(component: SkyboxComponent): Unit = {

    setUniform("viewMatrix", Engine.camera.viewMatrix.translate(Engine.camera.position))
    setUniform("projectionMatrix", Engine.camera.projection)

    val config = Engine.scene.atmosphere
    setUniform("fogColor", config.fogColor)
    setUniform("fogLow", config.skyboxFogLow)
    setUniform("fogHigh", config.skyboxFogHigh)

    component.skybox.draw()

  }

  override protected def collectData(o: GameObject): Option[SkyboxComponent] = {
    o.findComponent[SkyboxComponent]
  }
}
