package org.hansed.engine.rendering.shaders

import org.hansed.engine.core.resources.Resource
import org.lwjgl.opengl.GL20._
import org.lwjgl.opengl.GL32.GL_GEOMETRY_SHADER

/**
  * Shader resource contains handles for vertex, fragment and geometry shaders and a shader program that these can be
  * linked to
  *
  * @author Hannes Sederholm
  */
class ShaderResource(key: String) extends Resource(key) {

  val vertexId: Int = glCreateShader(GL_VERTEX_SHADER)
  val fragmentId: Int = glCreateShader(GL_FRAGMENT_SHADER)
  val geometryId: Int = glCreateShader(GL_GEOMETRY_SHADER)
  var programId: Int = glCreateProgram()

  /**
    * Free the resources reserved for this resource
    */
  override def clearResource(): Unit = {
    glDeleteShader(vertexId)
    glDeleteShader(fragmentId)
    glDeleteShader(geometryId)
    glDeleteProgram(programId)
  }


}
