package org.hansed.engine.rendering.shaders

import org.hansed.engine.Engine
import org.hansed.engine.core.GameObject
import org.hansed.engine.rendering.models.animated.{AnimatedRenderer, Animator}
import org.hansed.engine.rendering.models.data.animation.Joint
import org.joml.Matrix4f
import org.lwjgl.opengl.GL13.{GL_TEXTURE0, GL_TEXTURE1}

import scala.collection.mutable

/**
  * A shader for rendering animated models
  *
  * @author Hannes Sederholm
  */
object AnimatedShader extends LightShader[AnimatedRenderer]("AnimatedShader", "shaders/animated/AnimatedVertex.glsl",
  "shaders/basic/BasicFragment.glsl") {
  /**
    * Prepare the shader for rendering given component
    *
    * @param renderer about to be rendered
    */
  override def render(renderer: AnimatedRenderer): Unit = {
    Animator.applyPoseToJoints(renderer.model.resource.shape.skeleton, renderer.matrix, renderer.animator.poses())
    setUniform("viewMatrix", Engine.camera.viewMatrix)
    setUniform("projectionMatrix", Engine.camera.projection)
    val matrices = new Array[Matrix4f](50)
    var transforms = mutable.Buffer[Joint]()
    renderer.model.resource.shape.skeleton.collectTransforms(transforms)
    transforms = transforms.sortBy(_.id)
    for (i <- transforms.indices) {
      matrices(i) = transforms(i).currentTransform
    }
    setUniform("jointTransforms", matrices)

    val id = renderer.parent.map(_.uniqueId).getOrElse(0)

    val lights = getLights(id)
    setUniform("activeLights", lights.size)
    var index = 0
    lights.foreach(light
    => {
      setUniform(s"lightPosition[$index]", light.position)
      setUniform(s"lightColor[$index]", light.color)
      setUniform(s"lightPower[$index]", light.power)
      index += 1
    }
    )

    renderer.material.diffuse.bind(GL_TEXTURE0)
    renderer.material.normal.bind(GL_TEXTURE1)
    renderer.model.draw()

  }

  /**
    * Set this shader as the active OpenGL shader
    */
  override def bind(): Unit = {
    super.bind()

    //because the skybox shouldn't change during the render call just set the uniforms once
    val config = Engine.scene.atmosphere
    setUniform("fogColor", config.fogColor)
    setUniform("fogLow", config.terrainFogLow)
    setUniform("fogHigh", config.terrainFogHigh)

    setUniform("ambientFactor", config.ambientFactor)
    setUniform("cameraPosition", Engine.camera.position)
  }

  override protected def collectData(o: GameObject): Option[AnimatedRenderer] = {
    o.findComponent[AnimatedRenderer]
  }
}

