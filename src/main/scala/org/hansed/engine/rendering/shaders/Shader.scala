package org.hansed.engine.rendering.shaders

import org.hansed.engine.core.GameObject
import org.hansed.engine.core.resources.{ResourceLoader, ResourceUser}
import org.hansed.util.Logger
import org.joml.{Matrix3f, Matrix4f, Vector3f}
import org.lwjgl.BufferUtils
import org.lwjgl.opengl.GL11.GL_FALSE
import org.lwjgl.opengl.GL20._

import scala.collection.mutable

/**
  * Represents an OpenGL shader and deals with the programs interaction with the OpenGL shaders
  *
  * @see [[https://www.khronos.org/opengl/wiki/Shader]]
  * @param key          name of the shader
  * @param vertexPath   path to vertex shader source code
  * @param fragmentPath path to fragment shader source code
  * @param geometryPath path to geometry shader source code (optional)
  * @author Hannes Sederholm
  */
abstract class Shader[T](val key: String, vertexPath: String, fragmentPath: String,
                         geometryPath: Option[String] = None)
  extends ResourceUser[ShaderResource](key) {

  /**
    * Contains all the uniform variable ids mapped by their name
    */
  private val uniforms = mutable.HashMap[String, Int]()

  protected val renderData: mutable.Map[Int, T] = mutable.HashMap[Int, T]()

  /**
    * Include object for rendering
    *
    * @param o the object
    */
  def includeObject(o: GameObject): Unit = {
    val data = collectData(o)
    if (data.nonEmpty) {
      renderData.put(o.uniqueId, data.get)
    } else {
      renderData.remove(o.uniqueId)
      Logger.warn(
        s"Object flagged for the shader '$key', is missing components and thus not rendered.", this
      )
    }
  }

  /**
    * Remove object from rendering
    */
  def removeObject(o: GameObject): Unit = {
    renderData.remove(o.uniqueId)
  }

  def clearObjects(): Unit = {
    renderData.clear()
  }

  protected def collectData(o: GameObject): Option[T]

  def renderAll(): Unit = {
    renderData.values.foreach(render)
  }

  /**
    * Prepare the shader for rendering given component
    *
    * @param component about to be rendered
    */
  def render(component: T): Unit

  /**
    * Checks if an uniform has been given a location yet and generates the location if it hasn't
    * (uniform variable is an OpenGL variable that is the same for all vertices and thus doesn't need to be update
    * for each vertex)
    *
    * @param name name of uniform
    * @return uniform location for given uniform name
    */
  private def checkUniform(name: String): Int = {
    uniforms.getOrElseUpdate(name, glGetUniformLocation(resource.programId, name))
  }

  /**
    * Set a value of a matrix uniform
    *
    * @param name  name of uniform
    * @param value the matrix to be stored in the variable
    */
  protected def setUniform(name: String, value: Matrix4f): Unit = {
    val buffer = BufferUtils.createFloatBuffer(16)
    value.get(buffer)
    glUniformMatrix4fv(checkUniform(name), false, buffer)
  }

  /**
    * Set a value of a matrix uniform
    *
    * @param name  name of uniform
    * @param value the matrix to be stored in the variable
    */
  protected def setUniform(name: String, value: Matrix3f): Unit = {
    val buffer = BufferUtils.createFloatBuffer(16)
    value.get(buffer)
    glUniformMatrix3fv(checkUniform(name), false, buffer)
  }

  /**
    * Set a value of a vector uniform
    *
    * @param name  name of uniform
    * @param value the vector3f to be stored in the variable
    */
  protected def setUniform(name: String, value: Vector3f): Unit = {
    glUniform3fv(checkUniform(name), value.get(BufferUtils.createFloatBuffer(3)))
  }

  /**
    * Set the value of a float uniform
    *
    * @param name  name of uniform
    * @param value the float to be stored in the variable
    */
  protected def setUniform(name: String, value: Float): Unit = {
    glUniform1f(checkUniform(name), value)
  }

  /**
    * Set the value of an integer uniform
    *
    * @param name  name of uniform
    * @param value the float to be stored in the variable
    */
  protected def setUniform(name: String, value: Int): Unit = {
    glUniform1i(checkUniform(name), value)
  }

  /**
    * Set the value of a boolean uniform
    *
    * @param name  name of uniform
    * @param value the boolean to be stored in the variable
    */
  protected def setUniform(name: String, value: Boolean): Unit = {
    glUniform1i(checkUniform(name), if (value) 1 else 0)
  }

  /**
    * Set a matrix array uniform
    *
    * @param name     name of uniform
    * @param matrices array of matrices to load
    */
  protected def setUniform(name: String, matrices: Array[Matrix4f]): Unit = {
    val buffer = BufferUtils.createFloatBuffer(matrices.length * 16)
    for (i <- matrices.indices) {
      if (matrices(i) == null) {
        new Matrix4f().get(i * 16, buffer)
      } else {
        matrices(i).get(i * 16, buffer)
      }
    }
    glUniformMatrix4fv(checkUniform(name), false, buffer)
  }


  /**
    * Set this shader as the active OpenGL shader
    */
  override def bind(): Unit = glUseProgram(resource.programId)

  /**
    * Disable shader
    */
  override def unbind(): Unit = glUseProgram(0)

  /**
    * Load the shader resource
    *
    * @param key resource key
    * @return resource wrapped in Option
    */
  override def loadResource(key: String): Option[ShaderResource] = {
    Some(Shader.createShader(key, vertexPath, fragmentPath, geometryPath))
  }
}

object Shader {

  /**
    * Create a new shader resource
    *
    * @param key          shader key
    * @param vertexPath   vertex shader file path
    * @param fragmentPath fragment shader file path
    * @param geometryPath geometry shade file path wrapped in option
    * @return a new resource
    */
  private def createShader(key: String, vertexPath: String, fragmentPath: String, geometryPath: Option[String])
  : ShaderResource = {
    val resource = new ShaderResource(key)

    loadShader(vertexPath, resource.vertexId)
    loadShader(fragmentPath, resource.fragmentId)

    glAttachShader(resource.programId, resource.vertexId)
    glAttachShader(resource.programId, resource.fragmentId)

    //load the geometry shader if it's defined
    geometryPath.foreach(path => {
      loadShader(path, resource.geometryId)
      glAttachShader(resource.programId, resource.geometryId)
    })

    glLinkProgram(resource.programId)
    glValidateProgram(resource.programId)

    resource
  }

  /**
    * Compile and load a shader into an OpenGL shader handle
    *
    * @param filePath path of the shader file
    * @param shaderId the handle of the shader
    */
  private def loadShader(filePath: String, shaderId: Int): Unit = {
    glShaderSource(shaderId, ResourceLoader.lines(filePath).mkString("\n"))
    glCompileShader(shaderId)
    if (glGetShaderi(shaderId, GL_COMPILE_STATUS) == GL_FALSE) {
      Logger.err(s"Could not compile shader!\n£$filePath\n${glGetShaderInfoLog(shaderId, 500)}", this)
      System.exit(-1)
    }
  }
}
