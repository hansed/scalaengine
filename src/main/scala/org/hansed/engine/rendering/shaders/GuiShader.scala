package org.hansed.engine.rendering.shaders

import org.hansed.engine.core.GameObject
import org.hansed.engine.rendering.ui.GuiRenderer
import org.lwjgl.opengl.GL11._

/**
  * A shader for rendering graphical user interfaces
  *
  * @author Hannes Sederholm
  */
object GuiShader extends Shader[GuiRenderer]("GuiShader", "shaders/gui/GuiVertex.glsl",
  "shaders/gui/GuiFragment.glsl") {

  /**
    * Prepare the shader for rendering given component
    *
    */
  override def render(renderer: GuiRenderer): Unit = {
    setUniform("transformationMatrix", renderer.matrix)
    renderer.render()
  }

  /**
    * Set this shader as the active OpenGL shader
    */
  override def bind(): Unit = {
    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    glDisable(GL_DEPTH_TEST)
    super.bind()
  }

  /**
    * Disable shader
    */
  override def unbind(): Unit = {
    glDisable(GL_BLEND)
    glEnable(GL_DEPTH_TEST)
    super.unbind()
  }

  override protected def collectData(o: GameObject): Option[GuiRenderer] = {
    o.findComponent[GuiRenderer]
  }
}
