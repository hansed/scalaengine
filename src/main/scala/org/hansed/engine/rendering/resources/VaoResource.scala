package org.hansed.engine.rendering.resources

import org.hansed.engine.core.resources.Resource
import org.lwjgl.opengl.GL15._
import org.lwjgl.opengl.GL30.{glDeleteVertexArrays, glGenVertexArrays}

import scala.collection.mutable

/**
  * Contains the OpenGL handles for an ibo and a vbo and helps keep track of vbos related to the vao
  *
  * @see [[https://www.khronos.org/opengl/wiki/Vertex_Specification]] for more information on these
  * @param key an unique key for the type of the resource. This is used to identify whether the resource needs to be
  *            loaded or is already loaded
  * @author Hannes Sederholm
  */
class VaoResource(key: String) extends Resource(key) {

  val ibo: Int = glGenBuffers()
  private val _vao: Int = glGenVertexArrays()

  /**
    * @return the vao if for this resource
    */
  def vao: Int = _vao

  /**
    * List of attribute buffer ids mapped by index
    */
  private val attributeList = mutable.HashMap[Int, Int]()

  /**
    * Get the vertex buffer object in the vertex attribute array for a given index
    * If the object doesn't exist yet it's automatically created
    *
    * @param index in array
    * @return vbo
    */
  def vbo(index: Int): Int = {
    attributeList.getOrElseUpdate(index, glGenBuffers())
  }

  /**
    * Get an iterator over all the attributes that have been enabled for this vao
    *
    * @return iterator
    */
  def enabledAttributes: Iterable[Int] = attributeList.keys

  /**
    * Free the resources reserved for this resource
    */
  override def clearResource(): Unit = {
    attributeList.values.foreach(glDeleteBuffers)
    glDeleteBuffers(ibo)
    glDeleteVertexArrays(vao)
  }
}

