package org.hansed.engine.rendering.resources

import java.awt.image.BufferedImage

import org.hansed.engine.core.resources.{Resource, ResourceLoader}

/**
  * Represents a buffered image loaded from disk
  *
  * @param path the path to the image file
  */
class ImageResource(path: String) extends Resource(path) {

  val image: BufferedImage = ResourceLoader.loadImage(path)

  /**
    * Free the resources reserved for this resource
    */
  override def clearResource(): Unit = {
    image.flush()
  }
}
