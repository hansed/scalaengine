package org.hansed.engine.rendering.resources

import org.hansed.engine.core.resources.Resource
import org.lwjgl.opengl.GL11._
import org.lwjgl.opengl.GL30._

/**
  * Frame buffer resource, contains an id for the fbo and a texture
  *
  * @param name name of buffer
  * @author Hannes Sederholm
  */
class FrameBufferResource(name: String) extends Resource(name) {

  val frameBufferId: Int = glGenFramebuffers
  val textureId: Int = glGenTextures
  val depthTextureId: Int = glGenTextures

  /**
    * Free the resources reserved for this resource
    */
  override def clearResource(): Unit = {
    glDeleteFramebuffers(frameBufferId)
    glDeleteTextures(textureId)
    glDeleteTextures(depthTextureId)
  }
}
