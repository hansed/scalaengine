package org.hansed.engine.rendering.resources

import org.hansed.engine.rendering.textures.CubeMapTexture

/**
  * A resource for a skybox
  *
  * @param path to skybox texture
  * @author Hannes Sederholm
  */
class SkyboxResource(path: String) extends VaoResource(path) {

  /**
    * The skybox texture
    */
  val texture = new CubeMapTexture(path)

}
