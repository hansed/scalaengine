package org.hansed.engine.rendering.resources

import org.hansed.engine.rendering.models.data.Shape
import org.hansed.engine.rendering.models.loaders.ModelLoader
import org.hansed.util.GLUtil.storeAttribute
import org.lwjgl.opengl.GL15._
import org.lwjgl.opengl.GL20.{glDisableVertexAttribArray, glEnableVertexAttribArray}
import org.lwjgl.opengl.GL30.glBindVertexArray

import scala.concurrent.Future

/**
  * A resource that contains the data for the model and the data for its vao
  *
  * @param key path of model file
  * @author Hannes Sederholm
  */
class ModelResource(key: String) extends VaoResource(key) {

  var shape: Shape = _

  import scala.concurrent.ExecutionContext.Implicits.global

  val futureShape: Future[Unit] = Future({
    shape = loadShape
  })

  var loadCompleted = false

  /**
    * A method for loading the shape to allow overriding for alternate loading methods
    *
    * @return the shape stored in this resource
    */
  protected def loadShape: Shape = {
    ModelLoader.loadModel(key)
  }

  /**
    * @return the vao if for this resource
    */
  override def vao: Int = {
    if (futureShape.isCompleted && !loadCompleted) {
      loadCompleted = true
      updateData()
    }
    super.vao
  }

  def updateData(): Unit = {
    glBindVertexArray(vao)
    enabledAttributes.foreach(glEnableVertexAttribArray)

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, shape.indices, GL_STATIC_DRAW)

    storeAttribute(this, 0, 3, shape.vertices)
    storeAttribute(this, 1, 2, shape.textures)
    storeAttribute(this, 2, 3, shape.normals)
    storeAttribute(this, 3, 3, shape.tangents)
    storeAttribute(this, 4, 3, shape.bitangents)

    enabledAttributes.foreach(glDisableVertexAttribArray)
    glBindBuffer(GL_ARRAY_BUFFER, 0)
    glBindVertexArray(0)
  }

}
