package org.hansed.engine.rendering.resources

import org.hansed.engine.rendering.models.data.SkinnedShape
import org.hansed.engine.rendering.models.loaders.AnimatedModelLoader

/**
  * A Vao resource for animated models contains a skinned shape of the model
  *
  * @param key an unique key for the type of the resource. This is used to identify whether the resource needs to be
  *            loaded or is already loaded
  * @author Hannes Sederholm
  */
class AnimatedModelResource(key: String) extends VaoResource(key) {

  var shape: SkinnedShape = AnimatedModelLoader.loadModel(key)

}
