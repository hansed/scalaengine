package org.hansed.engine.rendering.ui.containers

import java.awt.{Font, Rectangle}

import org.hansed.engine.rendering.ui.UI
import org.hansed.engine.rendering.ui.graphics.Context2D

/**
  * A Panel element with a title written on top of the element
  *
  * @param x           coordinate
  * @param y           coordinate
  * @param width       width of panel
  * @param height      height of panel
  * @param text        title text
  * @param titleHeight the height of the area that contains the title
  * @author Hannes Sederholm
  */
class TitledPanel(x: Int, y: Int, width: Int, height: Int, text: String, titleHeight: Int = 30)
  extends Panel(x, y, width, height) {

  private val font = UI.font.deriveFont(Font.BOLD)

  private val titleColor = UI.outline

  private val textArea = new Rectangle(area) {
    height = titleHeight
  }

  /**
    * Repaint this component with given graphics context
    *
    * @param g the context
    */
  override def paint(g: Context2D): Unit = {
    super.paint(g)
    g.fillRect(0, 0, width, titleHeight, titleColor, area)
    g.alignText(text, font, foreground, textArea)
  }
}
