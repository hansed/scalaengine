package org.hansed.engine.rendering.ui.info

import java.awt.image.BufferedImage

import org.hansed.engine.core.resources.ResourceLoader
import org.hansed.engine.rendering.ui.Component
import org.hansed.engine.rendering.ui.graphics.Context2D

/**
  * An icon UI component
  *
  * @param icon the icon
  * @param _x   coordinate
  * @param _y   coordinate
  * @param w    width
  * @param h    height
  * @author Hannes Sederholm
  */
class Icon(icon: BufferedImage, _x: Int, _y: Int, w: Int, h: Int) extends Component(_x, _y, w, h) {

  //icon images may be transparent
  hasTransparency = true

  private var ownIcon = false

  def this(path: String, _x: Int, _y: Int, w: Int, h: Int) {
    this(ResourceLoader.loadImage(path), _x, _y, w, h)
    ownIcon = true
  }

  /**
    * Repaint this component with given graphics context
    *
    * @param g the context
    */
  override protected def paint(g: Context2D): Unit = {
    g.drawImage(icon, area)
  }

  override def finalize(): Unit = {
    //if we loaded the image we should clear it
    if (ownIcon) {
      icon.flush()
    }
  }
}
