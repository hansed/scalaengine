package org.hansed.engine.rendering.ui.containers

import org.hansed.engine.rendering.ui.{Component, UI}

/**
  * A Panel that has vertical and horizontal scrolling
  *
  * @param x           coordinate in pixels from the left
  * @param y           coordinate in pixels from top
  * @param content     the ui that is scrollable inside this panel
  * @param scrollWidth the width of the vertical scroll bar
  * @author Hannes Sederholm
  */
class ScrollPane(x: Int, y: Int, width: Int, height: Int, val content: Component, val scrollWidth: Int = 20)
  extends UI(x, y, width, height) {

  private val backgroundPane = new ScrollBarPane(width, height, content, scrollWidth)

  {
    val borderSize = backgroundPane.border.width
    // Add the content view
    addChild(new UI(0, 0, width - scrollWidth, height - borderSize * 2) {
      add(content)
    })
  }

  // Add the vertical scroll bar
  add(backgroundPane)
}
