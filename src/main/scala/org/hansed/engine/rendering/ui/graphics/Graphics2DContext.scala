package org.hansed.engine.rendering.ui.graphics

import java.awt._
import java.awt.geom.Rectangle2D
import java.awt.image.BufferedImage

import org.hansed.engine.rendering.ui.UI.{TEXT_ALIGN_CENTER, TEXT_ALIGN_LEFT, TEXT_ALIGN_RIGHT, TextAlign}

/**
  * A 2D graphics context implemented using [[java.awt.Graphics2D]]
  *
  * @param g the graphics context for rendering
  * @author Hannes Sederholm
  */
class Graphics2DContext(g: Graphics2D) extends Context2D {

  //set AA for text
  g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON)
  g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)
  g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY)

  /**
    * Fill a 2d rectangle with a given color
    *
    * @param x      coordinate
    * @param y      coordinate
    * @param width  in pixels
    * @param height in pixels
    * @param color  to fill with
    */
  override def fillRect(x: Int, y: Int, width: Int, height: Int, color: Color, area: Rectangle): Unit = {
    g.setColor(color)
    g.fillRect(area.x + x, area.y + y, width, height)
  }


  /**
    * Fill a 2d rectangle with a given color inside a given reference area
    *
    * @param x      coordinate
    * @param y      coordinate
    * @param width  in pixels
    * @param height in pixels
    * @param radius in pixels
    * @param color  to fill with
    * @param area   a 2d rectangle that the coordinates are relative to
    */
  override def fillRoundRect(x: Int, y: Int, width: Int, height: Int, radius: Int, color: Color, area: Rectangle)
  : Unit = {
    g.setColor(color)
    g.fillRoundRect(area.x + x, area.y + y, width, height, radius, radius)
  }


  /**
    * Draw a 2d rectangle with a given color
    *
    * @param x      coordinate
    * @param y      coordinate
    * @param width  in pixels
    * @param height in pixels
    * @param color  to fill with
    */
  override def drawRect(x: Int, y: Int, width: Int, height: Int, color: Color, area: Rectangle): Unit = {
    g.setColor(color)
    g.drawRect(area.x + x, area.y + y, width, height)
  }


  /**
    * Draw a 2d rectangle with a given color inside a given reference area
    *
    * @param x      coordinate
    * @param y      coordinate
    * @param width  in pixels
    * @param height in pixels
    * @param radius in pixels
    * @param color  to fill with
    * @param area   a 2d rectangle that the coordinates are relative to
    */
  override def drawRoundRect(x: Int, y: Int, width: Int, height: Int, radius: Int, color: Color, area: Rectangle)
  : Unit = {
    g.setColor(color)
    g.drawRoundRect(area.x + x, area.y + y, width, height, radius, radius)
  }


  /**
    * Draw a string of text at given position
    *
    * @param x    coordinate
    * @param y    coordinate
    * @param text the text
    * @param font font to be used
    */
  override def drawText(text: String, x: Int, y: Int, font: Font, color: Color, area: Rectangle): Unit = {
    g.setColor(color)
    g.setFont(font)
    g.drawString(text, x + area.x, y + area.y)
  }

  /**
    * Draw a text centered based on a given rectangular area
    *
    * @param text the text
    * @param area the area
    * @param font the font
    */
  override def alignText(text: String, font: Font, color: Color, area: Rectangle, textAlign: TextAlign): Unit = {
    val metrics = g.getFontMetrics(font)
    val textHeight = metrics.getHeight
    val ascent = metrics.getAscent
    val textWidth = metrics.stringWidth(text)
    val baselineCenter = (area.height - textHeight) / 2 + ascent

    textAlign match {
      case TEXT_ALIGN_CENTER =>
        drawText(text, (area.width - textWidth) / 2, baselineCenter, font, color, area)
      case TEXT_ALIGN_LEFT =>
        drawText(text, 0, baselineCenter, font, color, area)
      case TEXT_ALIGN_RIGHT =>
        drawText(text, area.width - textWidth, baselineCenter, font, color, area)
      case _ => throw new IllegalArgumentException("Unsupported text alignment key: " + textAlign)
    }
  }


  /**
    * Draws an image as the size of the area given
    *
    * @param image   the image to draw
    * @param area    the area to draw the image onto
    * @param padding the padding of the area before the image
    */
  override def drawImage(image: BufferedImage, area: Rectangle, padding: Int = 0): Unit = {
    g.drawImage(image, area.x + padding, area.y + padding, area.width - padding * 2, area.height - padding * 2, null)
  }

  /**
    * Set the line width of this context
    *
    * Affects things like drawing rectangles etc.
    *
    * @param width the width of the line
    */
  override def strokeSize_=(width: Int): Unit = {
    g.setStroke(new BasicStroke(width))
  }

  /**
    * Fill a fitted oval inside given rectangle
    *
    * @param color the color of the oval
    * @param area  the area to fit the oval inside
    */
  override def fillOval(color: Color, area: Rectangle): Unit = {
    g.setColor(color)
    g.fillOval(area.x, area.y, area.width, area.height)
  }

  /**
    * Draw a fitted oval inside given rectangle
    *
    * @param color the color of the oval
    * @param area  the area to fit the oval inside
    */
  override def drawOval(color: Color, area: Rectangle): Unit = {
    g.setColor(color)
    g.drawOval(area.x, area.y, area.width, area.height)
  }

  /**
    * Clear a 2D area with transparency
    *
    * @param area the area
    */
  override def clearArea(area: Rectangle): Unit = {
    g.setBackground(new Color(255, 255, 255, 0))
    g.clearRect(area.x, area.y, area.width, area.height)
  }

  /**
    * Get the size of a line in a given font
    *
    * @param font the font used
    * @param line the line drawn
    * @return the size in format (width, height)
    */
  override def lineSize(font: Font, line: String): Rectangle2D = {
    g.getFontMetrics(font).getStringBounds(line, g)
  }

  /**
    * Do something with a stroke of given width
    *
    * @param width the width of the stroke
    * @param f     the function to be called once the stroke size is set
    */
  override def withStroke(width: Int)(f: Graphics2DContext => Unit): Unit = {
    val oldStroke = g.getStroke
    strokeSize_=(width)
    f(this)
    g.setStroke(oldStroke)
  }
}
