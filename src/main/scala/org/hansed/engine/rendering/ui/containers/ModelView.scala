package org.hansed.engine.rendering.ui.containers

import java.awt.image.BufferedImage
import java.util.UUID

import org.hansed.engine.Engine
import org.hansed.engine.core.eventSystem.Events
import org.hansed.engine.core.{GameObject, GameScene}
import org.hansed.engine.rendering.RenderingEvents.{EntityCameraUpdate, FocusEntityCamera}
import org.hansed.engine.rendering.camera.{Camera, EntityFollowingCamera}
import org.hansed.engine.rendering.models.animated.Animator
import org.hansed.engine.rendering.models.stationary.ModelComponent
import org.hansed.engine.rendering.ui.graphics.Context2D
import org.hansed.engine.rendering.ui.{Component, UI}
import org.hansed.engine.rendering.{FrameBuffer, RenderEngine}
import org.hansed.engine.saving.DefaultSaveProtocol.AtmosphereLoader
import org.lwjgl.BufferUtils
import org.lwjgl.opengl.GL11._
import org.lwjgl.opengl.GL30.GL_COLOR_ATTACHMENT0

import scala.concurrent.ExecutionContext.Implicits.global

/**
  * An UI component that displays a game object
  *
  * @param _x         x coordinate
  * @param _y         y coordinate
  * @param w          width, the pixel size of the fbo is unchanged by the actual drawn component can be scaled
  * @param h          height, the pixel size of the fbo is unchanged by the actual drawn component can be scaled
  * @param rootObject the root object of the scene to be rendered onto this component
  * @author Hannes Sederholm
  */
class ModelView(_x: Int, _y: Int, w: Int, h: Int, rootObject: GameObject) extends Component(_x, _y, w, h) {

  /**
    * The camera used for looking at the entity
    */
  val camera: EntityFollowingCamera = new EntityFollowingCamera(Camera.perspective(width, height), radius = 4.0f) {
    horizontalSpin = -Math.PI.toFloat / 4.0f
  }

  private var _renderer: GameObject = _

  renderer = rootObject

  /**
    * The scene that is rendered onto the fbo and eventually to the UI
    */
  var scene: GameScene = GameScene(camera, _renderer, AtmosphereLoader.config("modelView"))

  /**
    * The fbo that the model is drawn onto
    */
  val fbo: FrameBuffer = new FrameBuffer("ModelView:" + UUID.randomUUID(), w, h)

  /**
    * A buffer of pixels to be set onto the image and extracted from the fbo
    */
  private val pixels = BufferUtils.createByteBuffer(w * h * 4)

  /**
    * The image that the fbo will get drawn onto
    */
  private val image: BufferedImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB)

  /**
    * Tells if the model has been drawn onto the fbo yet
    */
  var modelDrawn: Boolean = false

  /**
    * Get the root object that is used to render the scene of this model view
    *
    * @return the game object that is focused to render when rendering this UI
    */
  def renderer: GameObject = _renderer

  def renderer_=(renderer: GameObject): Unit = {

    _renderer = renderer

    scene = GameScene(camera, renderer, AtmosphereLoader.config("modelView"))

    //update the image once resources are loaded
    renderer.collectComponents().flatMap({
      case r: ModelComponent => Some(r)
      case _ => None
    }).foreach(r => {
      if (!r.material.diffuse.resource.loadComplete) {
        r.material.diffuse.resource.futureImages.onComplete(_ => {
          modelDrawn = false
          repaint()
        })
      }
      if (!r.model.resource.loadCompleted) {
        r.model.resource.futureShape.onComplete(_ => {
          modelDrawn = false
          repaint()
        })
      }
    })
    modelDrawn = false
    repaint()
  }


  //because we may have transparency (and likely have) in the image we repaint the parent to clear background
  hasTransparency = true

  /**
    * Repaint this component with given graphics context
    *
    * @param g the context
    */
  override protected def paint(g: Context2D): Unit = {
    if (!modelDrawn) {
      drawModel()
      modelDrawn = true
    }
    g.drawImage(image, area)
  }

  /**
    * Draw the model onto the fbo and from there onto a buffered image
    */
  def drawModel(): Unit = {
    fbo.bind()
    val g = image.createGraphics()
    g.setBackground(UI.TRANSPARENT)
    g.clearRect(0, 0, image.getWidth, image.getHeight)

    val tempScene = Engine.scene
    Engine.scene = scene
    Events.signal(FocusEntityCamera(_renderer, autoUpdate = true), instantly = true)
    Events.signal(EntityCameraUpdate(), instantly = true)
    //make sure the animation bind poses have been applied
    scene.rootObject.onComponents {
      case animator: Animator => animator.playOnce()
      case _ =>
    }
    scene.rootObject.update()
    //clear the image
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    RenderEngine.render()
    Engine.scene = tempScene

    //read the pixels onto pixel buffer
    glReadBuffer(GL_COLOR_ATTACHMENT0)
    glReadPixels(0, 0, w, h, GL_RGBA, GL_UNSIGNED_BYTE, pixels)

    fbo.unbind()

    //set the pixels on the BufferedImage
    for {
      x <- 0 until w
      y <- 0 until h
    } {
      val i = (x + (w * y)) * 4
      val r = pixels.get(i) & 0xFF
      val g = pixels.get(i + 1) & 0xFF
      val b = pixels.get(i + 2) & 0xFF
      if (r != 0 || g != 0 || b != 0) {
        image.setRGB(x, h - (y + 1), (0xFF << 24) | (r << 16) | (g << 8) | b)
      }
    }
  }

}
