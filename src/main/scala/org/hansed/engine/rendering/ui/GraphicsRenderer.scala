package org.hansed.engine.rendering.ui

import org.hansed.engine.rendering.textures.GraphicsTexture
import org.lwjgl.opengl.GL13.GL_TEXTURE0

/**
  * An UI renderer that uses [[org.hansed.engine.rendering.textures.GraphicsTexture]] for rendering
  *
  * @param x      x coordinate
  * @param y      y coordinate
  * @param width  width
  * @param height height
  * @author Hannes Sederholm
  */
class GraphicsRenderer(x: Int, y: Int, width: Int, height: Int) extends GuiRenderer(x, y, width, height) {

  val texture = new GraphicsTexture(width.toInt, height.toInt)

  /**
    * Draw the component, this method is called after the shader has been informed about the component
    */
  override def render(): Unit = {
    texture.bind(GL_TEXTURE0)
    gui.draw()
    texture.unbind()
  }
}
