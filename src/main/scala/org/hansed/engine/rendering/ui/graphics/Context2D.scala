package org.hansed.engine.rendering.ui.graphics

import java.awt.geom.Rectangle2D
import java.awt.image.BufferedImage
import java.awt.{Color, Font, Rectangle}

import org.hansed.engine.rendering.ui.UI.{TEXT_ALIGN_CENTER, TextAlign}
import org.hansed.engine.rendering.ui.decorations.Border

/**
  * A 2D graphics context interface for drawing UI elements
  *
  * @author Hannes Sederholm
  */
trait Context2D {

  /**
    * Set the line width of this context
    *
    * Affects things like drawing rectangles etc.
    *
    * @param width the width of the line
    */
  def strokeSize_=(width: Int): Unit

  /**
    * Do something with a stroke of given width
    *
    * @param width the width of the stroke
    * @param f     the function to be called once the stroke size is set
    */
  def withStroke(width: Int)(f: Graphics2DContext => Unit): Unit

  /**
    * Fill a 2d rectangle with a given color inside a given reference area
    *
    * @param x      coordinate
    * @param y      coordinate
    * @param width  in pixels
    * @param height in pixels
    * @param color  to fill with
    * @param area   a 2d rectangle that the coordinates are relative to
    */
  def fillRect(x: Int, y: Int, width: Int, height: Int, color: Color, area: Rectangle): Unit

  /**
    * Fill a 2d rectangle with a given color inside a given reference area
    *
    * @param x      coordinate
    * @param y      coordinate
    * @param width  in pixels
    * @param height in pixels
    * @param radius in pixels
    * @param color  to fill with
    * @param area   a 2d rectangle that the coordinates are relative to
    */
  def fillRoundRect(x: Int, y: Int, width: Int, height: Int, radius: Int, color: Color, area: Rectangle): Unit

  /**
    * Draw a 2d rectangle with a given color inside a given reference area
    *
    * @param x      coordinate
    * @param y      coordinate
    * @param width  in pixels
    * @param height in pixels
    * @param color  to fill with
    * @param area   a 2d rectangle that the coordinates are relative to
    */
  def drawRect(x: Int, y: Int, width: Int, height: Int, color: Color, area: Rectangle): Unit

  /**
    * Draw a 2d rectangle with a given color inside a given reference area
    *
    * @param x      coordinate
    * @param y      coordinate
    * @param width  in pixels
    * @param height in pixels
    * @param radius in pixels
    * @param color  to fill with
    * @param area   a 2d rectangle that the coordinates are relative to
    */
  def drawRoundRect(x: Int, y: Int, width: Int, height: Int, radius: Int, color: Color, area: Rectangle): Unit

  /**
    * Fill a fitted oval inside given rectangle
    *
    * @param color the color of the oval
    * @param area  the area to fit the oval inside
    */
  def fillOval(color: Color, area: Rectangle): Unit

  /**
    * Draw a fitted oval inside given rectangle
    *
    * @param color the color of the oval
    * @param area  the area to fit the oval inside
    */
  def drawOval(color: Color, area: Rectangle): Unit

  /**
    * Draw a string of text at given position relative to a given reference area
    *
    * @param x    coordinate
    * @param y    coordinate
    * @param text the text
    * @param font font to be used
    * @param area a 2d rectangle that the coordinates are relative to
    */
  def drawText(text: String, x: Int, y: Int, font: Font, color: Color, area: Rectangle): Unit

  /**
    * Draw a text centered based on a given rectangular area
    *
    * @param text the text
    * @param area the area
    * @param font the font
    */
  def alignText(text: String, font: Font, color: Color, area: Rectangle, textAlign: TextAlign = TEXT_ALIGN_CENTER): Unit

  /**
    * Draws an image as the size of the area given
    *
    * @param image   the image to draw
    * @param area    the area to draw the image onto
    * @param padding the padding of the area before the image
    */
  def drawImage(image: BufferedImage, area: Rectangle, padding: Int = 0): Unit

  /**
    * Clear a 2D area with transparency
    *
    * @param area the area
    */
  def clearArea(area: Rectangle): Unit

  /**
    * Get the size of a line in a given font
    *
    * @param font the font used
    * @param line the line drawn
    * @return the size in format (width, height)
    */
  def lineSize(font: Font, line: String): Rectangle2D


  /**
    * Get the pixel height of a line
    *
    * @param font the font that would be used
    * @return the height of a line drawn using the font
    */
  def lineHeight(font: Font): Int = lineSize(font, "").getHeight.toInt

  /**
    * Fill a rectangle and draw a border around it
    *
    * @param x            relative x of the rectangle
    * @param y            relative y of the rectangle
    * @param width        width of the rectangle
    * @param height       height of the rectangle
    * @param color        the color of the inside of the rectangle
    * @param border       border to be draw around the rectangle
    * @param area         the area the coordinates are relative to
    * @param cornerRadius the radius of the corners of the rectangle <= 0 means no rounding
    */
  def fillWithBorder(x: Int, y: Int, width: Int, height: Int, color: Color, border: Border, cornerRadius: Int,
                     area: Rectangle): Unit = {
    if (cornerRadius > 0) {
      fillRoundRect(x, y, width, height, cornerRadius, color, area)
      withStroke(border.width) {
        _.drawRoundRect(x, y, width, height, cornerRadius, border.color, area)
      }
    } else {
      fillRect(x, y, width, height, color, area)
      withStroke(border.width) {
        _.drawRect(x, y, width, height, border.color, area)
      }
    }
  }
}
