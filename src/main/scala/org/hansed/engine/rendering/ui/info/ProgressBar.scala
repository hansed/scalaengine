package org.hansed.engine.rendering.ui.info

import java.awt.Color

import org.hansed.engine.rendering.ui.UI
import org.hansed.engine.rendering.ui.graphics.Context2D

/**
  * A progress bar ui element
  *
  * @param _x         coordinate
  * @param _y         coordinate
  * @param w          width
  * @param min        minimum value
  * @param max        maximum value
  * @param text       the text displayed on the progress bar
  * @param startValue the value set at creation of the bar
  * @author Hannes Sederholm
  */
class ProgressBar(_x: Int, _y: Int, w: Int, h: Int = 25, min: Float = 0.0f, max: Float = 1.0f, text: String = "",
                  startValue: Float = 0.0f) extends Label(text, _x, _y, w, h) {

  private var _value: Float = startValue

  private var _fillColor: Color = UI.highlight

  background = UI.secondaryBackground

  /**
    * Repaint this component with given graphics context
    *
    * @param g the context
    */
  override protected def paint(g: Context2D): Unit = {
    g.fillWithBorder(0, 0, width, height, background, border, cornerRadius, area)
    g.fillWithBorder(0, 0, (width * progress).toInt, height, fillColor, border, cornerRadius, area)
    g.alignText(text, font, foreground, area)
  }

  /**
    * The progress percentage of this bar
    *
    * @return progress from 0.0 to 1.0
    */
  def progress: Float = _value / max

  /**
    * Get the progress value of this bar between min and max
    *
    * @return value
    */
  def value: Float = _value

  /**
    * Set the value
    *
    * @param value new value
    */
  def value_=(value: Float): Unit = {
    _value = Math.min(Math.max(value, min), max)
    repaint()
  }

  /**
    * Get the color that progress on this bar is displayed in
    *
    * @return progress fill color
    */
  def fillColor: Color = _fillColor

  /**
    * Set the progress fill color of this bar
    *
    * @param color the color
    */
  def fillColor_=(color: Color): Unit = {
    _fillColor = color
    repaint()
  }
}
