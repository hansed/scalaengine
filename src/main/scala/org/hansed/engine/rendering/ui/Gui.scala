package org.hansed.engine.rendering.ui

import java.util.UUID

import org.hansed.engine.core.resources.ResourceUser
import org.hansed.engine.rendering.resources.VaoResource
import org.hansed.util.GLUtil.storeAttribute
import org.lwjgl.opengl.GL11.{GL_TRIANGLE_STRIP, glDrawArrays}
import org.lwjgl.opengl.GL15.{GL_ARRAY_BUFFER, glBindBuffer}
import org.lwjgl.opengl.GL20.{glDisableVertexAttribArray, glEnableVertexAttribArray}
import org.lwjgl.opengl.GL30.glBindVertexArray

/**
  * Represents the OpenGL side of an UI
  * [[org.hansed.engine.rendering.ui.GuiRenderer]] uses this for rendering
  *
  * @author Hannes Sederholm
  */
class Gui extends ResourceUser[VaoResource](UUID.randomUUID().toString) {

  //origin at top left
  private val positions = Array[Float](0.0f, 0.0f, 0.0f, -2.0f, 2.0f, -2.0f, 2.0f, 0.0f)

  loadData()

  /**
    * Load the corners onto vao
    */
  private def loadData(): Unit = {
    bind()
    storeAttribute(resource, 0, 2, positions)
    unbind()
  }

  /**
    * Set this resource as current, whatever that might mean depends on the type of the resource
    */
  override def bind(): Unit = {
    glBindVertexArray(resource.vao)
    glEnableVertexAttribArray(0)
  }

  /**
    * Return from what bind does to the default state
    * Note that if the system related to the resource wasn't at default state before bind the state will be different
    */
  override def unbind(): Unit = {
    glDisableVertexAttribArray(0)
    glBindBuffer(GL_ARRAY_BUFFER, 0)
    glBindVertexArray(0)
  }

  /**
    * Draw the quad
    */
  def draw(): Unit = {
    bind()
    glDrawArrays(GL_TRIANGLE_STRIP, 0, positions.length)
    unbind()
  }

  /**
    * Loads the resource
    * Called when no cached resource was found
    *
    * @param key resource key
    * @return resource wrapped in Option
    */
  override def loadResource(key: String): Option[VaoResource] = Some(new VaoResource(key))

}
