package org.hansed.engine.rendering.ui.info

import org.hansed.engine.rendering.ui.UI
import org.hansed.engine.rendering.ui.decorations.Text
import org.hansed.engine.rendering.ui.graphics.Context2D

/**
  * A simple text label element
  *
  * @param _text  text
  * @param x      coordinate
  * @param y      coordinate
  * @param width  width of element
  * @param height height of element
  * @author Hannes Sederholm
  */
class Label(_text: String, x: Int, y: Int, width: Int, height: Int)
  extends Text(_text, x, y, width, height) {

  //Disable the default transparency set by text
  hasTransparency = false
  background = UI.secondaryBackground

  /**
    * Repaint this component with given graphics context
    *
    * @param g the context
    */
  override protected def paint(g: Context2D): Unit = {
    g.fillWithBorder(0, 0, width, height, background, border, cornerRadius, area)
    super.paint(g)
  }

}
