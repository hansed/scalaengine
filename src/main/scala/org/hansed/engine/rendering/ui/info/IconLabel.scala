package org.hansed.engine.rendering.ui.info

import java.awt.{Color, Rectangle}
import java.awt.image.BufferedImage

import org.hansed.engine.core.resources.ResourceLoader
import org.hansed.engine.rendering.ui.graphics.Context2D

/**
  * A label component with an icon on the left side of the label
  *
  * @param icon        the icon
  * @param _text       text
  * @param _x          coordinate
  * @param _y          coordinate
  * @param w           width
  * @param h           height
  * @param iconPadding padding of the icon from top and bottom
  * @author Hannes Sederholm
  */
class IconLabel(icon: BufferedImage, _text: String, _x: Int, _y: Int, w: Int, h: Int, iconPadding: Int = 0) extends Label(_text, _x, _y, w, h) {

  private var ownIcon = false

  background = new Color(0, 0, 0, 0)

  def this(iconPath: String, _text: String, _x: Int, _y: Int, w: Int, h: Int) = {
    this(ResourceLoader.loadImage(iconPath), _text, _x, _y, w, h)
    ownIcon = true
  }

  def this(iconPath: String, _text: String, _x: Int, _y: Int, w: Int, h: Int, iconPadding: Int) = {
    this(ResourceLoader.loadImage(iconPath), _text, _x, _y, w, h, iconPadding)
    ownIcon = true
  }

  /**
    * Repaint this component with given graphics context
    *
    * @param g the context
    */
  override protected def paint(g: Context2D): Unit = {
    g.fillRect(0, 0, width, height, background, area)
    g.drawImage(icon, new Rectangle(x, y, height, height), iconPadding)
    g.alignText(text, font, foreground, new Rectangle(x + height, y, width - height, height))
  }

  override def finalize(): Unit = {
    if (ownIcon) {
      icon.flush()
    }
  }
}
