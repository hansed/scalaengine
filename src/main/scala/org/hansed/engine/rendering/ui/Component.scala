package org.hansed.engine.rendering.ui

import java.awt.geom.Rectangle2D
import java.awt.{Color, Rectangle}

import org.hansed.engine.rendering.ui.UI.{UIAlignment, UI_ALIGN_CENTER}
import org.hansed.engine.rendering.ui.decorations.Border
import org.hansed.engine.rendering.ui.graphics.Context2D
import org.hansed.engine.rendering.ui.info.Tooltip
import org.lwjgl.glfw.GLFW.{GLFW_MOUSE_BUTTON_LEFT, GLFW_MOUSE_BUTTON_MIDDLE, GLFW_MOUSE_BUTTON_RIGHT}

import scala.collection.mutable

/**
  * An UI component that is rendered as a 2D quad
  *
  * @author Hannes Sederholm
  */
abstract class Component(private var _x: Int, private var _y: Int, private var _width: Int, private var _height: Int) {

  /**
    * Whether or not the parent component should be repainted as well when this is repainted
    */
  protected var hasTransparency: Boolean = false

  private var tooltip: Option[Component] = None

  /**
    * Determines the rendering order on a given parent component or UI
    *
    * Defaults to 1000; the greater the number the later the component is rendered
    */
  var layer: Int = 1000

  protected val children: mutable.SortedSet[Component] = mutable.SortedSet[Component]()

  private var _visible: Boolean = true

  private var _parent: Option[UI] = None

  private var _area: Rectangle = new Rectangle(x, y, width, height)

  private var _parentComponent: Option[Component] = None

  private var _foreground: Color = UI.foreground

  private var _background: Color = UI.primaryBackground

  private var _border: Border = Border(UI.outline, 1)

  private var _cornerRadius: Int = UI.cornerRadius

  protected var lastCursorY: Int = 0
  protected var lastCursorX: Int = 0

  protected var cursorInside = false
  protected var focused = false

  protected var mouseButtonDown: Option[Int] = None


  /**
    * Whether or not on the next repaint this component should be repainted
    */
  private var repaintRequired = true

  updateArea()

  /**
    * Repaint this component and its children
    *
    * @param g the context to use for rendering
    */
  def paintAll(g: Context2D, repaintArea: Rectangle2D, forceRepaint: Boolean = false): Boolean = {
    var painted = false
    if (visible) {
      if (repaintRequired || forceRepaint || repaintArea.intersects(area)) {
        paint(g)
        painted = true
      }
      children.foreach(c => {
        if (c.visible) {
          painted = painted | c.paintAll(g, repaintArea, forceRepaint || repaintRequired || painted)
        }
      })
      repaintRequired = false
    }
    painted
  }

  /**
    * Repaint this component with given graphics context
    *
    * @param g the context
    */
  protected def paint(g: Context2D): Unit

  /**
    * Request a repaint of this component on the next render tick
    *
    */
  def repaint(area: Rectangle = area): Unit = {
    repaintRequired = true
    if (hasTransparency) {
      parentComponent.foreach(_.repaint())
    }
    parent.foreach(_.repaint(area))
  }

  /**
    * Called if this component is clicked and none of the children consume the event
    *
    * The event can be consumed by returning true with this method
    *
    * @param button the button clicked
    * @return
    */
  protected def onClick(button: Int): Boolean = false

  /**
    * Called when the cursor enters this component
    */
  protected def onMouseEntered(): Unit = {}

  /**
    * Called when the cursor exits this component
    */
  protected def onMouseExited(): Unit = {}

  /**
    * Called when mouse is moved over this component
    *
    * @param offsetX the offset in x direction
    * @param offsetY the offset in y direction
    */
  protected def onMouseMoved(offsetX: Int, offsetY: Int): Unit = {}

  /**
    * Called when the mouse is moved over the component and a mouse button is down
    *
    * @param button  id of the button down
    * @param offsetX x offset of the movement
    * @param offsetY y offset of the movement
    */
  protected def onMouseDragged(button: Int, offsetX: Int, offsetY: Int): Unit = {}

  /**
    * Called when the component is focused by clicking on it
    */
  protected def onFocusGained(): Unit = {}

  /**
    * Called when the component loses focus due to the user clicking another component
    */
  protected def onFocusLost(): Unit = {}

  /**
    * Called when a key is typed and this component is focused
    *
    * @param key the key typed
    */
  protected def onKeyTyped(key: Int): Unit = {}

  /**
    * Called when a key is pressed and this component is focused
    *
    * @param keyCode the glfw key code of the key pressed
    */
  protected def onKeyDown(keyCode: Int): Unit = {}

  /**
    * Called when a key is pressed and this component is focused
    *
    * @param keyCode the glfw key code of the key released
    */
  protected def onKeyUp(keyCode: Int): Unit = {}

  /**
    * Called when the components parent is clicked
    *
    * If the child's clicked returns true E.G. the event is consumed the onClick of this component wont get called
    *
    * @param clickX the x coordinate of the click
    * @param clickY the y coordinate of the click
    * @param button the button clicked
    * @return whether the click event was consumed
    */
  def clicked(clickX: Int, clickY: Int, button: Int): Boolean = {
    val wasFocused = focused
    focused = false
    //If click is inside parent component and parent component is visible
    if (area.contains(clickX, clickY) && visible) {
      //If no child consumes the click event
      if (!children.foldLeft(false)((consumed, component) => {
        consumed || component.clicked(clickX, clickY, button)
      })) {
        //If no child gained focus gain focus
        if (!children.exists(_.focused)) {
          if (!wasFocused) {
            onFocusGained()
          }
          focused = true
        }
        // Let this component have a chance at consuming the click event
        onClick(button)
      } else {
        // If a child consumed the event focus was lost
        if (wasFocused) {
          focusLost()
        }
        // Pass on the message that the click was consumed
        true
      }
    } else {
      // If click wasn't even inside this component focus was definitely lost
      if (wasFocused) {
        focusLost()
      }
      // Won't consume events outside the component
      false
    }
  }

  /**
    * Inform the component that focus was lost
    */
  def focusLost(): Unit = {
    mouseButtonDown = None
    onFocusLost()
  }


  /**
    * Called when the parent UI is focused and a char is typed
    *
    * @param key the character typed
    */
  def keyTyped(key: Int): Unit = {
    if (focused) {
      onKeyTyped(key)
    } else {
      children.foreach(_.keyTyped(key))
    }
  }

  /**
    * Called when the parent UI is focused and a key is pressed down
    *
    * @param keyCode the glfw keycode of the key released
    */
  def keyDown(keyCode: Int): Unit = {
    if (focused) {
      keyCode match {
        case GLFW_MOUSE_BUTTON_LEFT | GLFW_MOUSE_BUTTON_MIDDLE | GLFW_MOUSE_BUTTON_RIGHT =>
          mouseButtonDown = Some(keyCode)
        case _ =>
      }
      onKeyDown(keyCode)
    } else {
      children.foreach(_.keyDown(keyCode))
    }
  }

  /**
    * Called when the parent UI is focused and a key is released
    *
    * @param keyCode the glfw keycode of the key released
    */
  def keyUp(keyCode: Int): Unit = {
    keyCode match {
      case GLFW_MOUSE_BUTTON_LEFT | GLFW_MOUSE_BUTTON_MIDDLE | GLFW_MOUSE_BUTTON_RIGHT =>
        mouseButtonDown = None
      case _ =>
    }
    if (focused) {
      onKeyUp(keyCode)
    } else {
      children.foreach(_.keyUp(keyCode))
    }
  }

  /**
    * Called when mouse is moved
    *
    * @param mouseX the x coordinate the mouse moved to
    * @param mouseY the y coordinate the mouse moved to
    */
  def mouseMoved(mouseX: Int, mouseY: Int): Unit = {
    children.foreach(_.mouseMoved(mouseX, mouseY))

    val contains: Boolean = area.contains(mouseX, mouseY)
    val exited = cursorInside && !contains
    val entered = !cursorInside && contains
    val relativeCursorX = mouseX - area.x
    val relativeCursorY = mouseY - area.y

    cursorInside = contains

    if (exited) {
      onMouseExited()
      tooltip.foreach(_.visible = false)
    } else if (entered) {
      lastCursorX = relativeCursorX
      lastCursorY = relativeCursorY
      onMouseEntered()
      tooltip.foreach(_.visible = true)
    }

    if (contains) {
      val offsetX = relativeCursorX - lastCursorX
      val offsetY = relativeCursorY - lastCursorY
      onMouseMoved(offsetX, offsetY)
      mouseButtonDown.foreach(onMouseDragged(_, offsetX, offsetY))
      lastCursorX = relativeCursorX
      lastCursorY = relativeCursorY
    }

  }

  /**
    * Add a child component to this component
    *
    * @param component the child
    */
  def add(component: Component*): Unit = {
    component.foreach(component => {
      children += component
      component.parent = parent
      component.parentComponent = Some(this)
    })
  }

  /**
    * Set the tooltip text for this component
    *
    * @param text                the text displayed on the tooltip
    * @param verticalAlignment   alignment of the tooltip on vertical axis
    * @param horizontalAlignment alignment of the tooltip on horizontal axis
    */
  def setTooltip(text: String, verticalAlignment: UIAlignment = UI_ALIGN_CENTER,
                 horizontalAlignment: UIAlignment = UI_ALIGN_CENTER): Unit = {
    val tooltip = new Tooltip(text, verticalAlignment, horizontalAlignment)
    add(tooltip)
    this.tooltip = Some(tooltip)
  }

  /**
    * Remove a child component from this component
    *
    * @param component the child
    */
  def remove(component: Component): Unit = {
    children -= component
    repaint(component.area)
    component.parent = None
    component.parentComponent = None
  }

  /**
    * Get the UI that this component is added to None if not added to anything
    *
    * @return parent UI
    */
  def parent: Option[UI] = _parent

  /**
    * Set the parent UI of this component and its children
    *
    * @param parent the parent UI
    */
  def parent_=(parent: Option[UI]): Unit = {
    if (_parent.isDefined) {
      _parent.foreach(_.componentRemoved(this))
    }
    if (parent.isDefined) {
      parent.foreach(_.componentAdded(this))
    }
    _parent = parent
    children.foreach(_.parent = parent)
  }

  /**
    * Get the component this component is added to, None if this is a root component
    *
    * @return an UI component
    */
  def parentComponent: Option[Component] = _parentComponent

  /**
    * Set the parent component of this component
    *
    * Updates the absolute area after the parent has been set
    *
    * @param component new parent component
    */
  def parentComponent_=(component: Option[Component]): Unit = {
    _parentComponent = component
    updateArea()
  }

  /**
    * Update the absolute area of this component
    */
  def updateArea(): Unit = {
    val parentArea = parentComponent.map(_.area).getOrElse(new Rectangle(0, 0, 0, 0))
    _area = new Rectangle(parentArea.x + x, parentArea.y + y, width, height)
    children.foreach(_.updateArea())
  }

  /**
    * Get the absolute area of this component, mainly absolute x and y coordinate
    *
    * @return a rectangle specifying the bounds of this component
    */
  def area: Rectangle = {
    _area
  }

  /**
    * Create an UI wrapper around this component for displaying the component
    *
    * @return the UI component
    */
  def asUI: UI = {
    //Create an UI
    val ui = new UI(x, y, width, height)

    //set the coordinates to 0 as UI will handle that
    x = 0
    y = 0

    //add this component to the UI created
    ui.add(this)

    ui
  }

  def absX: Int = area.x

  def absY: Int = area.y

  def x: Int = _x

  def y: Int = _y

  def width: Int = _width

  def height: Int = _height

  def visible: Boolean = _visible

  def background: Color = _background

  def foreground: Color = _foreground

  def border: Border = _border

  def cornerRadius: Int = _cornerRadius

  def x_=(value: Int): Unit = {
    _x = value
    updateArea()
  }

  def y_=(value: Int): Unit = {
    _y = value
    updateArea()
  }

  def width_=(value: Int): Unit = {
    _width = value
    updateArea()
  }

  def height_=(value: Int): Unit = {
    _height = value
    updateArea()
  }

  def visible_=(value: Boolean): Unit = {
    _visible = value
    repaint()
  }

  def foreground_=(foreground: Color): Unit = {
    _foreground = foreground
    repaint()
  }

  def background_=(background: Color): Unit = {
    _background = background
    repaint()
  }

  def border_=(border: Border): Unit = {
    _border = border
    repaint()
  }

  def cornerRadius_=(radius: Int): Unit = {
    _cornerRadius = radius
    repaint()
  }

}

object Component {

  implicit val componentOrdering: Ordering[Component] = (x: Component, y: Component) => {
    if (x.layer != y.layer) {
      x.layer - y.layer
    } else {
      if (x.equals(y)) 0 else 1
    }
  }

}
