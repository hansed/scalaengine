package org.hansed.engine.rendering.ui.info

import org.hansed.engine.rendering.ui.UI
import org.hansed.engine.rendering.ui.containers.Panel
import org.hansed.engine.rendering.ui.decorations.Text
import org.hansed.engine.rendering.ui.input.Button

/**
  * A popup dialogue UI that displays a message with a single confirm button
  *
  * @param _x          coordinate
  * @param _y          coordinate
  * @param w           width
  * @param h           height
  * @param info        the message
  * @param confirmText the button text
  * @author Hannes Sederholm
  */
abstract class MessagePopup(_x: Int, _y: Int, w: Int, h: Int, info: String, confirmText: String) extends UI(_x, _y, w, h) {

  add(new Panel(0, 0, w, h) {

    background = UI.secondaryBackground

    {
      val buttonWidth = w / 2
      val buttonX = (w - buttonWidth) / 2
      val buttonY = h - 40

      add(new Button(confirmText, buttonWidth, 30, buttonX, buttonY) {
        override protected def onClick(button: Int): Boolean = {
          onClose()
          MessagePopup.this.remove()
          true
        }
      })
      add(new Text(info, 0, 0, w, h - 30) {
        textAlign = UI.TEXT_ALIGN_CENTER
      })
    }
  })

  def onClose(): Unit

}
