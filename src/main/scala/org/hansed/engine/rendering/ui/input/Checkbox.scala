package org.hansed.engine.rendering.ui.input

import org.hansed.engine.rendering.ui.UI
import org.hansed.engine.rendering.ui.decorations.Text
import org.hansed.engine.rendering.ui.graphics.Context2D

/**
  * An ui element that can be either checked or not
  *
  * @param x    coordinate
  * @param y    coordinate
  * @param size size of the box
  * @author Hannes Sederholm
  */
class Checkbox(x: Int, y: Int, size: Int = 25) extends Text("√", x, y, size, size) {

  private var _checked = false

  hasTransparency = false
  background = UI.secondaryBackground

  textAlign = UI.TEXT_ALIGN_CENTER

  font = font.deriveFont(size * 0.8f)

  /**
    * Repaint this component with given graphics context
    *
    * @param g the context
    */
  override protected def paint(g: Context2D): Unit = {
    g.fillWithBorder(0, 0, width, height, background, border, cornerRadius, area)
    if (checked)
      g.alignText(text, font, foreground, area, textAlign = textAlign)
  }

  /**
    * Called if this component is clicked and none of the children consume the event
    *
    * The event can be consumed by returning true with this method
    *
    * @param button the button clicked
    * @return
    */
  override protected def onClick(button: Int): Boolean = {
    checked = !checked
    true
  }

  /**
    * Can be overridden for a simple callback on value
    */
  def onValueChanged(): Unit = {}

  def checked: Boolean = _checked

  /**
    * Set the checked value of this checkbox
    *
    * Repaint is requested after the value has been set
    *
    * @param value new value
    */
  def checked_=(value: Boolean): Unit = {
    _checked = value
    onValueChanged()
    repaint()
  }
}
