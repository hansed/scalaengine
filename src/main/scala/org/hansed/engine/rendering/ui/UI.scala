package org.hansed.engine.rendering.ui

import java.awt.{Color, Font, Rectangle}

import org.hansed.engine.core.GameObject
import org.hansed.engine.core.eventSystem.EventListener
import org.hansed.engine.core.eventSystem.events.Event
import org.hansed.engine.core.eventSystem.events.HierarchyEvents.ObjectAdded
import org.hansed.engine.core.eventSystem.events.InputEvents.{CursorPosition, KeyDown, KeyUp, TextTyped}
import org.hansed.engine.core.input.InputHandler
import org.hansed.engine.core.resources.ResourceLoader
import org.hansed.engine.rendering.textures.GraphicsTexture
import org.hansed.engine.rendering.ui.graphics.Graphics2DContext
import org.hansed.engine.saving.DefaultSaveProtocol.ThemeLoader
import org.hansed.engine.saving.Theme
import org.joml.{Quaternionf, Vector3f}
import org.lwjgl.glfw.GLFW.{GLFW_MOUSE_BUTTON_LEFT, GLFW_MOUSE_BUTTON_MIDDLE, GLFW_MOUSE_BUTTON_RIGHT}

import scala.collection.mutable

/**
  * A GameObject that represents an UI of given size at a given position
  *
  * @param x      coordinate in pixels from the left
  * @param y      coordinate in pixels from top
  * @param width  width of ui used for focusing the ui with click
  * @param height height of ui used for focusing the ui with click
  * @author Hannes Sederholm
  */
class UI(x: Int, y: Int, width: Int, height: Int) extends GameObject with EventListener {

  position = new Vector3f(x, 0, y)

  /**
    * The UI components added to this UI
    */
  val uiComponents: mutable.SortedSet[Component] = mutable.SortedSet[Component]()

  /**
    * Contains all UI components added to this UI that have event listeners
    */
  val eventListeners: mutable.Set[EventListener] = mutable.Set[EventListener]()

  private var repaintArea: Option[Rectangle] = Some(new Rectangle(0, 0, width, height))

  /**
    * Used for tracking the currently focused UI, used for passing key events to the correct components
    */
  private var focused = false

  /**
    * The render used for drawing the 2D texture of this UI
    */
  val component: GraphicsRenderer = new GraphicsRenderer(x, y, width, height) {

    /**
      * Draw the component, this method is called after the shader has been informed about the component
      */
    override def render(): Unit = {
      repaintArea.foreach(area => {
        paint(area)
      })
      repaintArea = None
      super.render()
    }
  }

  addComponent(component)

  /**
    * Called when component has this set as parent
    *
    * @param component the component
    */
  def componentAdded(component: Component): Unit = {
    component match {
      case listener: EventListener => eventListeners += listener
      case _ =>
    }
  }

  /**
    * Called when component had this as parent but parent was changed
    *
    * @param component the component
    */
  def componentRemoved(component: Component): Unit = {
    component match {
      case listener: EventListener => eventListeners -= listener
      case _ =>
    }
  }

  /**
    * Paint the components onto the texture
    */
  def paint(area: Rectangle): Unit = {
    component.texture.update(g => {
      val context = new Graphics2DContext(g)
      context.clearArea(area)
      var forceLayer = Integer.MAX_VALUE
      uiComponents.iterator.foreach(c => {
        repaintArea.foreach(area => {
          if (c.paintAll(context, area, c.layer > forceLayer)) {
            forceLayer = c.layer
          }
        })
      })
    })
  }

  /**
    * Request this UI to repaint on next render
    */
  def repaint(area: Rectangle = new Rectangle(0, 0, width, height)): Unit = {
    repaintArea = Some(repaintArea.map(_.union(area)).getOrElse(area))
  }

  /**
    * Add a component to this UI
    *
    * @param component the component
    */
  def add(component: Component): Unit = {
    component.parent = Some(this)
    uiComponents += component
  }

  /**
    * Remove a component from this UI
    *
    * @param component the component
    */
  def remove(component: Component): Unit = {
    component.parent = None
    uiComponents.remove(component)
    repaint()
  }

  /**
    * Called when the component is clicked
    *
    * @param buttonId the id of the button clicked
    */
  def clicked(buttonId: Int): Unit = {
    val cursorX = InputHandler.lastCursorX.toInt
    val cursorY = InputHandler.lastCursorY.toInt
    if (contains(cursorX, cursorY)) {
      focused = true
      uiComponents.foreach(_.clicked(cursorX - x, cursorY - y, buttonId))
    } else {
      focused = false
    }
  }

  /**
    * Called when the cursor is moved
    *
    * @param mouseX new cursor X coordinate
    * @param mouseY new cursor Y coordinate
    */
  def mouseMoved(mouseX: Int, mouseY: Int): Unit = {
    uiComponents.foreach(_.mouseMoved(mouseX - x, mouseY - y))
  }

  /**
    * Does this component contain the coordinates specified
    *
    * @param x x coordinate
    * @param y y coordinate
    * @return if point is inside
    */
  def contains(x: Float, y: Float): Boolean = {
    val pos = position
    x >= pos.x && x <= pos.x + width && y >= pos.z && y <= pos.z + height
  }

  /**
    * Shorthand for accessing the texture of the renderer component
    *
    * @return a graphics texture for rendering
    */
  def texture: GraphicsTexture = component.texture

  /**
    * Overrides the recursive rotation as we don't want interfaces rotating with parents
    *
    * The result of this method may not be altered as it may reference the actual rotation object of this transform.
    * This is to reduce the number of new objects created and therefore improve performance.
    *
    * @return the absolute rotation of this transform
    */
  override def rotation: Quaternionf = localRotation

  /**
    * Deal with an event
    *
    * @param event an event sent by an EventDispatcher
    */
  override def processEvent(event: Event): Unit = {
    event match {
      case event: CursorPosition =>
        mouseMoved(event.x.toInt, event.y.toInt)
      case event: ObjectAdded =>
        if (event.childObject.equals(this)) {
          repaint()
        }
      case event: KeyUp =>
        event.keyCode match {
          case GLFW_MOUSE_BUTTON_MIDDLE | GLFW_MOUSE_BUTTON_LEFT | GLFW_MOUSE_BUTTON_RIGHT =>
            clicked(event.keyCode)
          case _ =>
        }
        if (focused) {
          uiComponents.foreach(c => c.keyUp(event.keyCode))
        }
      case event: KeyDown =>
        if (focused) {
          uiComponents.foreach(c => c.keyDown(event.keyCode))
        }
      case event: TextTyped =>
        if (focused) {
          uiComponents.foreach(c => c.keyTyped(event.charCode))
        }
      case _ =>
    }
    eventListeners.foreach(listener => if (listener.condition) listener.processEvent(event))
  }
}

/**
  * Helps with building the UI
  *
  * @author Hannes Sederholm
  */
object UI {

  final val TRANSPARENT: Color = new Color(1, 1, 1, 0)

  private var _theme: Theme = _

  theme = ThemeLoader.config("default")

  var primaryBackground: Color = _

  var secondaryBackground: Color = _

  var outline: Color = _

  var accent: Color = _

  var highlight: Color = _

  var foreground: Color = _

  var font: Font = _

  var cornerRadius: Int = _

  def update(): Unit = {
    primaryBackground = Color.decode(theme.primaryBackground)
    secondaryBackground = Color.decode(theme.secondaryBackground)
    accent = Color.decode(theme.accent)
    highlight = Color.decode(theme.highlight)
    foreground = Color.decode(theme.foreground)
    outline = Color.decode(theme.outline)

    cornerRadius = theme.cornerRadius

    if (theme.externalFont) {
      font = ResourceLoader.loadFont(theme.font).deriveFont(Font.PLAIN, theme.fontSize)
    } else {
      font = new Font(theme.font, Font.PLAIN, theme.fontSize)
    }

  }

  def theme: Theme = _theme

  def theme_=(theme: Theme): Unit = {
    _theme = theme
    update()
  }

  type TextAlign = Int

  val TEXT_ALIGN_LEFT = 0
  val TEXT_ALIGN_CENTER = 1
  val TEXT_ALIGN_RIGHT = 2

  type UIAlignment = Int

  val UI_ALIGN_TOP = 0
  val UI_ALIGN_LEFT = 0
  val UI_ALIGN_BOTTOM = 1
  val UI_ALIGN_RIGHT = 1
  val UI_ALIGN_CENTER = 2
}
