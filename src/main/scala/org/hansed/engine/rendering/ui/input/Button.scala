package org.hansed.engine.rendering.ui.input

import java.awt.Color

import org.hansed.engine.rendering.ui.UI
import org.hansed.engine.rendering.ui.graphics.Context2D
import org.hansed.engine.rendering.ui.info.Label

/**
  * An button element that gets highlighted on hover
  *
  * @param text text of the button
  * @param w    width
  * @param h    height
  * @param _x   coordinate
  * @param _y   coordinate
  * @author Hannes Sederholm
  */
class Button(text: String, w: Int, h: Int, _x: Int, _y: Int) extends Label(text, _x, _y, w, h) {

  background = UI.secondaryBackground

  private var _hoverColor = UI.accent

  textAlign = UI.TEXT_ALIGN_CENTER

  /**
    * Repaint this component with given graphics context
    *
    * @param g the context
    */
  override protected def paint(g: Context2D): Unit = {
    val prev = background
    background = if (cursorInside) hoverColor else background
    super.paint(g)
    background = prev
  }

  /**
    * Called when the cursor enters this component
    */
  override protected def onMouseEntered(): Unit = {
    repaint()
  }

  /**
    * Called when the cursor exits this component
    */
  override protected def onMouseExited(): Unit = {
    repaint()
  }

  def hoverColor: Color = _hoverColor

  def hoverColor_=(color: Color): Unit = {
    _hoverColor = color
    repaint()
  }
}
