package org.hansed.engine.rendering.ui

import org.hansed.engine.Engine
import org.hansed.engine.core.{GameComponent, GameObject}
import org.hansed.engine.rendering.shaders.GuiShader
import org.joml.Vector3f

/**
  * Represents an UI element that the user may interact with
  *
  * @param x      x coordinate
  * @param y      y coordinate
  * @param width  width
  * @param height height
  * @author Hannes Sederholm
  */
abstract class GuiRenderer(val x: Float, val y: Float, val width: Float, val height: Float) extends GameComponent {

  val gui = new Gui()

  /**
    * Called when the component is created. Should be used for initiating the component
    *
    * @param parent the parent object of this component
    */
  override def prepare(parent: GameObject): Unit = {
    parent.shaders += GuiShader.key
  }

  /**
    * Called once every update tick for all components
    */
  override def update(): Unit = {}

  /**
    * The result of this method may not be altered as it may reference the actual position object of this transform.
    * This is to reduce the number of new objects created and therefore improve performance.
    *
    * @return the absolute position of this transform
    */
  override def position: Vector3f = {
    val windowSize = Engine.windowSize
    val pos = super.position
    new Vector3f(-1.0f + (pos.x / windowSize._1) * 2.0f, 1.0f - (pos.z / windowSize._2) * 2.0f, pos.y)
  }

  /**
    * The result of this method may not be altered as it may reference the actual scale object of this transform.
    * This is to reduce the number of new objects created and therefore improve performance.
    *
    * @return the absolute scale of this transform
    */
  override def scale: Vector3f = {
    val windowSize = Engine.windowSize
    new Vector3f(width / windowSize._1, height / windowSize._2, 0)
  }

  /**
    * Render this component
    */
  def render(): Unit
}
