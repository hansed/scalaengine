package org.hansed.engine.rendering.ui.info

import java.awt.Color

import org.hansed.engine.rendering.ui.graphics.Context2D

/**
  * An UI component that works like a label except that it supports multi line text using newline char
  *
  * @author Hannes Sederholm
  */
class TextView(_text: String, _x: Int, _y: Int, w: Int, h: Int) extends Label(_text, _x, _y, w, h) {

  background = new Color(0, 0, 0, 0)

  /**
    * Repaint this component with given graphics context
    *
    * @param g the context
    */
  override protected def paint(g: Context2D): Unit = {
    //
    //    g.fillRect(0, 0, width, height, background, area)
    //
    //    val lineHeight = g.lineHeight(font)
    //
    //    val lineArea = new Rectangle(x, y, width, lineHeight)
    //
    //    text.split("\n").foreach(line => {
    //      g.alignText(line, font, foreground, lineArea)
    //      lineArea.y += lineHeight
    //    })
  }
}
