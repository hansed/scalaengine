package org.hansed.engine.rendering.ui.info

import org.hansed.engine.rendering.ui.UI._
import org.hansed.engine.rendering.ui.graphics.Context2D
import org.hansed.engine.rendering.ui.{Component, UI}

/**
  * Represents a tooltip that may be added to any component to be shown at hover
  *
  * The labels initial size is 0x0 at 0,0 and will be set when the tooltip is added onto a component
  *
  * @param text the text of the tooltip
  */
class Tooltip(text: String, val verticalAlignment: UIAlignment, horizontalAlignment: UIAlignment)
  extends Component(0, 0, 0, 0) {

  visible = false

  private var sizeSet = false

  private val padding = 8

  /**
    * Repaint this component with given graphics context
    *
    * @param g the context
    */
  override protected def paint(g: Context2D): Unit = {
    if (!sizeSet) {
      background = UI.primaryBackground
      parentComponent.foreach(c => {
        val textBounds = g.lineSize(UI.font, text)
        width = textBounds.getWidth.toInt + padding * 2
        height = textBounds.getHeight.toInt + padding
        verticalAlignment match {
          case UI_ALIGN_LEFT =>
            x = -width - 2
          case UI_ALIGN_CENTER =>
            x = (c.width - width) / 2
          case UI_ALIGN_RIGHT =>
            x = c.width + 2
          case _ => throw new IllegalArgumentException("Unknown vertical alignment type: " + verticalAlignment)
        }
        horizontalAlignment match {
          case UI_ALIGN_TOP =>
            y = -height - 2
          case UI_ALIGN_CENTER =>
            y = (c.height - height) / 2
          case UI_ALIGN_BOTTOM =>
            y = c.height + 2
          case _ => throw new IllegalArgumentException("Unknown horizontal alignment type: " + verticalAlignment)
        }
        sizeSet = true
        updateArea()
      })
    }
    g.fillRect(0, 0, width, height, background, area)
    g.alignText(text, UI.font, UI.foreground, area)
  }

  /**
    * Set the parent component of this component
    *
    * Updates the absolute area after the parent has been set
    *
    * @param component new parent component
    */
  override def parentComponent_=(component: Option[Component]): Unit = {
    super.parentComponent_=(component)

    sizeSet = false
  }
}
