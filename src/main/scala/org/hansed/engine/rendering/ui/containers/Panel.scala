package org.hansed.engine.rendering.ui.containers

import org.hansed.engine.rendering.ui.Component
import org.hansed.engine.rendering.ui.graphics.Context2D

/**
  * A panel ui element
  *
  * @param x      coordinate
  * @param y      coordinate
  * @param width  width of panel
  * @param height height of panel
  * @author Hannes Sederholm
  */
class Panel(x: Int, y: Int, width: Int, height: Int) extends Component(x, y, width, height) {

  /**
    * Repaint this component with given graphics context
    *
    * @param g the context
    */
  override def paint(g: Context2D): Unit = {
    g.fillWithBorder(0, 0, width - 1, height - 1, background, border, cornerRadius, area)
    g.withStroke(border.width) {
      _.drawRect(0, 0, width - 1, height - 1, border.color, area)
    }
  }
}
