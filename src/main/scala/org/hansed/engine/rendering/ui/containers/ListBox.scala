package org.hansed.engine.rendering.ui.containers

import org.hansed.engine.rendering.ui.Component
import org.hansed.engine.rendering.ui.graphics.Context2D

/**
  * A container that lists its contents in a vertical list
  *
  * @param _x     the x coordinate
  * @param _y     the y coordinate
  * @param w      width
  * @param h      height
  * @param margin the margin between two items
  * @param items  the initial items in the box
  * @author Hannes Sederholm
  */
class ListBox(_x: Int, _y: Int, w: Int, h: Int, preferredRowHeight: Int = Int.MaxValue, margin: Int = 0,
              items: Vector[Component] = Vector[Component](), alternateBackgrounds: Boolean = false)
  extends Component(_x, _y, w, h) {

  items.foreach(add(_))

  hasTransparency = true

  def updateChildren(): Unit = {

    val rowHeight = Math.min((height - (children.size - 1) * margin) / children.size, preferredRowHeight)
    var index = 0

    children.foreach(child => {
      child.x = 0
      child.y = index * (rowHeight + margin)
      child.width = width
      child.height = rowHeight
      index += 1
    })

    repaint()
  }

  /**
    * Repaint this component with given graphics context
    *
    * @param g the context
    */
  override def paint(g: Context2D): Unit = {
    //by default transparent
  }

  /**
    * Add a child component to this component
    *
    * @param component the child
    */
  override def add(component: Component*): Unit = {
    component.foreach(super.add(_))
    updateChildren()
  }
}
