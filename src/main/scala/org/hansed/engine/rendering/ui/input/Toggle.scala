package org.hansed.engine.rendering.ui.input

import java.awt.{Color, Rectangle}

import org.hansed.engine.rendering.ui.graphics.Context2D
import org.hansed.engine.rendering.ui.{Component, UI}

/**
  * An ui element that can be either on or off toggled by clicking
  *
  * @param x    coordinate
  * @param y    coordinate
  * @param size the diameter of the toggle ball, other sizes are proportional to that
  * @author Hannes Sederholm
  */
class Toggle(x: Int, y: Int, size: Int = 20) extends Component(x, y, size * 2, size) {

  // The radius of the bar the ball slides on
  private val barRadius = Math.ceil(4.0 * size / 5).toInt

  private var _enabled = false

  //because we don't draw the whole area we need to repaint the parent as well
  hasTransparency = true

  //Color used when toggled on
  foreground = UI.highlight

  //Color used when toggled off
  background = UI.accent


  /**
    * Repaint this component with given graphics context
    *
    * @param g the context
    */
  override protected def paint(g: Context2D): Unit = {

    val color = if (enabled) foreground else background
    val fadeColor = new Color(color.getRed, color.getGreen, color.getBlue, 50)

    val ovalArea =
      if (enabled) {
        new Rectangle(absX + width - size, absY, size, size)
      } else {
        new Rectangle(absX, absY, size, size)
      }

    g.fillWithBorder(0, height - barRadius - border.width, width, barRadius, fadeColor, border, barRadius, area)

    g.fillOval(color, ovalArea)
  }


  /**
    * Allows for a simple callback on value change
    */
  def onToggle(): Unit = {}

  def enabled: Boolean = _enabled

  def enabled_=(value: Boolean): Unit = {
    _enabled = value
    onToggle()
    repaint()
  }


  /**
    * Called if this component is clicked and none of the children consume the event
    *
    * The event can be consumed by returning true with this method
    *
    * @param button the button clicked
    * @return
    */
  override protected def onClick(button: Int): Boolean = {
    enabled = !enabled
    true
  }
}
