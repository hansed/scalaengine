package org.hansed.engine.rendering.ui.decorations

import java.awt.Color

/**
  * A border decoration for an ui element
  *
  * @param color the color of the border
  * @param width the width of the border
  * @author Hannes Sederholm
  */
case class Border(color: Color, width: Int)
