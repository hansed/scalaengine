package org.hansed.engine.rendering.ui.input

import org.hansed.engine.rendering.ui.UI
import org.hansed.engine.rendering.ui.decorations.Text
import org.hansed.engine.rendering.ui.info.Label
import org.hansed.engine.util.timers.{Timer, Timers}
import org.lwjgl.glfw.GLFW._

/**
  * A Text field UI component
  *
  * @param _x    coordinate
  * @param _y    coordinate
  * @param w     width
  * @param h     height
  * @param _text text
  * @param pad   horizontal padding (left and right) between the text and the start of the text
  * @author Hannes Sederholm
  */
class TextField(_x: Int, _y: Int, w: Int, h: Int, _text: String = "", pad: Int = 5)
  extends Label(_text, _x, _y, w, h) {

  private val cursor = new Text("|", 0, 0, 0, height)

  private var _cursorPosition = text.length

  add(cursor)

  padding = pad

  private val switchTick = 5
  private var tickCount = 0

  private val blinkerTimer = new Timer(() => {
    tickCount += 1
    if (focused) {
      if (tickCount >= switchTick) {
        tickCount = 0
        cursor.visible = !cursor.visible
      }
    } else if (cursor.visible) {
      cursor.visible = false
    }
  }, 100)

  Timers.add(blinkerTimer)

  /**
    * Set the parent UI of this component and its children
    *
    * @param parent the parent UI
    */
  override def parent_=(parent: Option[UI]): Unit = {
    super.parent_=(parent)
    cursorPosition = text.length
  }

  def cursorPosition: Int = _cursorPosition

  def cursorPosition_=(position: Int): Unit = {
    _cursorPosition = position
    parent.foreach(ui => {
      val g = ui.component.texture.image.createGraphics()
      val metrics = g.getFontMetrics(font)
      val textWidth = metrics.stringWidth(text)
      val startWidth = metrics.stringWidth(text.take(cursorPosition))
      //Offset due to whitespace between characters
      val cursorOffset = (metrics.charWidth(' ') + 1) / 2
      textAlign match {
        case UI.TEXT_ALIGN_LEFT =>
          cursor.x = padding + startWidth - cursorOffset
        case UI.TEXT_ALIGN_CENTER =>
          cursor.x = padding + (textArea.width - textWidth) / 2 + startWidth - cursorOffset
        case UI.TEXT_ALIGN_RIGHT =>
          cursor.x = padding + textArea.width - textWidth + startWidth - cursorOffset
        case _ => throw new IllegalArgumentException("Unsupported text alignment key: " + textAlign)
      }
      g.dispose()
    })
    tickCount = 0
    cursor.visible = true
  }

  /**
    * Called when a key is typed and the parent UI is focused
    *
    * @param key the key typed
    */
  override protected def onKeyTyped(key: Int): Unit = {
    text = text.take(cursorPosition) + key.toChar + text.takeRight(text.length - cursorPosition)
    cursorPosition += 1
  }


  /**
    * Called when a key is pressed and this component is focused
    *
    * @param keyCode the glfw key code of the key pressed
    */
  override protected def onKeyDown(keyCode: Int): Unit = {
    keyCode match {
      case GLFW_KEY_BACKSPACE =>
        if (cursorPosition > 0) {
          text = text.take(cursorPosition - 1) + text.takeRight(text.length - cursorPosition)
          cursorPosition -= 1
        }
      case GLFW_KEY_LEFT =>
        if (cursorPosition > 0) {
          cursorPosition -= 1
        }
      case GLFW_KEY_RIGHT =>
        if (cursorPosition < text.length) {
          cursorPosition += 1
        }
      case GLFW_KEY_DELETE =>
        if (cursorPosition < text.length) {
          text = text.take(text.length - 1)
        }
      case GLFW_KEY_END =>
        cursorPosition = text.length
      case GLFW_KEY_HOME =>
        cursorPosition = 0
      case _ =>
    }
  }

  override def finalize(): Unit = {
    //remove the timer for the blinker
    Timers.remove(blinkerTimer)
  }
}
