package org.hansed.engine.rendering.ui.containers

import org.hansed.engine.core.eventSystem.EventListener
import org.hansed.engine.core.eventSystem.events.Event
import org.hansed.engine.core.eventSystem.events.InputEvents.MouseScroll
import org.hansed.engine.rendering.ui.decorations.Border
import org.hansed.engine.rendering.ui.graphics.Context2D
import org.hansed.engine.rendering.ui.{Component, UI}
import org.lwjgl.glfw.GLFW.{GLFW_MOUSE_BUTTON_LEFT, GLFW_MOUSE_BUTTON_MIDDLE, GLFW_MOUSE_BUTTON_RIGHT}

/**
  * The background panel for [[org.hansed.engine.rendering.ui.containers.ScrollPane]] that contains the scrolls
  *
  * @param width  width of panel
  * @param height height of panel
  * @param target the component that gets scrolled with the bars
  * @author Hannes Sederholm
  */
class ScrollBarPane(width: Int, height: Int, target: Component, scrollSize: Int) extends Panel(0, 0, width, height)
  with EventListener {

  private val sliderMargin = 3

  //Slider height is the percentage of the full height that is visible of the target
  private val sliderHeight = math.max(25, math.max((height.toFloat / target.height) * height, 0).toInt)

  /**
    * Position of the slider in pixels
    */
  private var _sliderPosition: Int = 0

  /**
    * Whether the slider is currently being dragged
    */
  var drag: Boolean = false

  def sliderPosition: Int = _sliderPosition

  def sliderPosition_=(pos: Int): Unit = {
    _sliderPosition = math.min(math.max(pos, 0), height - sliderHeight)
    updateTarget()
  }

  /**
    * Update the slider position relative to a given cursor position
    *
    * @param cursorX the x position
    * @param cursorY the y position
    */
  def updateSliderPosition(cursorX: Int, cursorY: Int): Unit = {
    if (drag) {
      sliderPosition = cursorY - (sliderHeight + 1) / 2
    }
  }

  /**
    * Update the target position based on the slider position
    */
  def updateTarget(): Unit = {
    target.y = ((height - target.height) * (sliderPosition.toFloat / (height - sliderHeight))).toInt
    target.repaint()
    repaint()
  }


  /**
    * Called when a key is pressed and this component is focused
    *
    * @param keyCode the glfw key code of the key pressed
    */
  override protected def onKeyDown(keyCode: Int): Unit = {
    keyCode match {
      case GLFW_MOUSE_BUTTON_MIDDLE | GLFW_MOUSE_BUTTON_LEFT | GLFW_MOUSE_BUTTON_RIGHT =>
        if (cursorInside && lastCursorX >= width - scrollSize) {
          drag = true
          updateSliderPosition(lastCursorX, lastCursorY)
        }
      case _ =>
    }
  }


  /**
    * Called when a key is pressed and this component is focused
    *
    * @param keyCode the glfw key code of the key released
    */
  override protected def onKeyUp(keyCode: Int): Unit = {
    keyCode match {
      case GLFW_MOUSE_BUTTON_MIDDLE | GLFW_MOUSE_BUTTON_LEFT | GLFW_MOUSE_BUTTON_RIGHT =>
        drag = false
      case _ =>
    }
  }

  /**
    * Called when the mouse is moved over the component and a mouse button is down
    *
    * @param button  id of the button down
    * @param offsetX x offset of the movement
    * @param offsetY y offset of the movement
    */
  override protected def onMouseDragged(button: Int, offsetX: Int, offsetY: Int): Unit = {
    updateSliderPosition(lastCursorX, lastCursorY)
  }


  /**
    * Called when the cursor exits this component
    */
  override protected def onMouseExited(): Unit = {
    drag = false
  }

  /**
    * Repaint this component with given graphics context
    *
    * @param g the context
    */
  override def paint(g: Context2D): Unit = {
    super.paint(g)
    //Draw the vertical scroll bar
    g.fillRect(width - scrollSize, 0, scrollSize, height, UI.outline, area)
    //Draw vertical bar slider
    g.fillWithBorder(width - scrollSize + sliderMargin, sliderPosition, scrollSize - 2 * sliderMargin,
      sliderHeight, UI.accent, Border(UI.primaryBackground, 1), 0, area)
  }

  /**
    * Deal with an event
    *
    * @param event an event sent by an EventDispatcher
    */
  override def processEvent(event: Event): Unit = {
    event match {
      case event: MouseScroll =>
        if (cursorInside) {
          sliderPosition -= (event.amount * 2).toInt
        }
      case _ =>
    }
  }
}
