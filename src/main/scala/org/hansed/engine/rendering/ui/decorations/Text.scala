package org.hansed.engine.rendering.ui.decorations

import java.awt.{Font, Rectangle}

import org.hansed.engine.rendering.ui.UI.TextAlign
import org.hansed.engine.rendering.ui.graphics.Context2D
import org.hansed.engine.rendering.ui.{Component, UI}

/**
  * A decoration component that draws text on the UI
  *
  * By default transparent because no background is drawn
  *
  * @param _text  initial text to be drawn
  * @param x      coordinate
  * @param y      coordinate
  * @param width  width of the component
  * @param height height of the component
  * @author Hannes Sederholm
  */
class Text(private var _text: String, x: Int, y: Int, width: Int, height: Int) extends Component(x, y, width, height) {

  private var _font: Font = UI.font

  private var _textAlign: TextAlign = UI.TEXT_ALIGN_LEFT

  private var _padding: Int = 0

  private var _textArea: Rectangle = area

  // Text doesn't fill the whole area so it has to be transparent
  hasTransparency = true

  /**
    * Repaint this component with given graphics context
    *
    * @param g the context
    */
  override protected def paint(g: Context2D): Unit = {
    g.alignText(text, font, foreground, textArea, textAlign = textAlign)
  }

  def textArea: Rectangle = _textArea

  def text: String = _text

  def font: Font = _font

  def padding: Int = _padding

  def textAlign: TextAlign = _textAlign

  def textArea_=(area: Rectangle): Unit = {
    _textArea = area
    repaint()
  }

  def padding_=(padding: Int): Unit = {
    _padding = padding
    updateArea()
  }

  def text_=(text: String): Unit = {
    _text = text
    repaint()
  }

  def font_=(font: Font): Unit = {
    _font = font
    repaint()
  }

  def textAlign_=(align: TextAlign): Unit = {
    _textAlign = align
    repaint()
  }

  /**
    * Update the absolute area of this component
    */
  override def updateArea(): Unit = {
    super.updateArea()
    textArea = new Rectangle(area.x + padding, area.y, area.width - padding * 2, area.height)
  }
}
