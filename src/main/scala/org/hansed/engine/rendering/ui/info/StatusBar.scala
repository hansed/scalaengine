package org.hansed.engine.rendering.ui.info

import org.hansed.engine.Engine
import org.hansed.engine.core.GameComponent
import org.hansed.engine.rendering.ui.UI
import org.joml.{Matrix4f, Vector3f, Vector4f}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
  * An UI that shows a given value around the parent component via a progress bar
  *
  * @param currentValue a function that returns the value displayed
  * @param maxValue     the max value
  * @param barParentPos a callback that returns the position of the target object
  * @author Hannes Sederholm
  */
class StatusBar(currentValue: () => Float, maxValue: Float, barParentPos: () => Vector3f) extends UI(0, 0, 60, 10) {

  var bar: Option[ProgressBar] = None

  Future(new ProgressBar(0, 0, 60, max = maxValue) {
    value = currentValue()
  }).onComplete(f => {
    f.foreach(result => {
      bar = Some(result)
      add(result)
    })
  })


  /**
    * Sets the absolute position by adding the inverse of absolute position to the given position
    *
    * @param newPos new absolute position
    */
  def setPosition(newPos: Vector3f): Unit =
    position = newPos.sub(parent.map(p => p.position).getOrElse(new Vector3f()))

  //keep the health bar above the enemy on the screen
  addComponent(new GameComponent {

    var lastValue: Float = currentValue()

    override def update(): Unit = {
      val newValue = currentValue()
      if (lastValue != newValue) {
        bar.foreach(_.value = newValue)
      }
      var pos = Engine.camera.projection.mul(Engine.camera.viewMatrix, new Matrix4f()).transform(new Vector4f
      (barParentPos(), 1.0f), new Vector4f())
      val w = pos.w
      pos = pos.div(w)
      val windowSize = Engine.windowSize
      setPosition(new Vector3f((pos.x + 1.0f) / 2.0f * windowSize._1 - 30, 0, (pos.y - 1.0f) / -2.0f * windowSize
        ._2 - 40 / w))
      //keep the size fixed
      scale = new Vector3f(w, w, w)
    }
  })
}
