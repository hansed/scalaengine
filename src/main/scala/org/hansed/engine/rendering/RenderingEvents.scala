package org.hansed.engine.rendering

import org.hansed.engine.core.GameObject
import org.hansed.engine.core.eventSystem.events.Event

/**
  * Contains events related to controlling what the player sees
  *
  * @author Hannes Sederholm
  */
object RenderingEvents {

  /**
    * Sent when an [[org.hansed.engine.rendering.camera.EntityFollowingCamera]] needs to change the thing its looking at
    *
    * @param target the target transform to look at
    */
  case class FocusEntityCamera(target: GameObject, autoUpdate: Boolean) extends Event

  /**
    * Sent when we want to update the camera position
    */
  case class EntityCameraUpdate() extends Event

}
