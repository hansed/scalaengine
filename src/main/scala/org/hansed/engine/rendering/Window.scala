package org.hansed.engine.rendering

import org.hansed.engine.core.input.InputHandler
import org.hansed.util.GLUtil._
import org.lwjgl.glfw.Callbacks.glfwFreeCallbacks
import org.lwjgl.glfw.GLFW._
import org.lwjgl.glfw.GLFWErrorCallback
import org.lwjgl.opengl.GL.createCapabilities
import org.lwjgl.opengl.GL11.glClearColor
import org.lwjgl.system.MemoryUtil.NULL

/**
 * Acts as an interface for creating and using a GLFW window
 *
 * @author Hannes Sederholm
 */
@throws[IllegalStateException]
class Window(val width: Int, val height: Int, title: String) {

  GLFWErrorCallback.createPrint(System.err).set()
  if (!glfwInit())
    throw new IllegalStateException("Unable to create the window")

  /*
  OpenGL properties like require version 4.5
   */
  glfwDefaultWindowHints()
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4)
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5)
  glfwWindowHint(GLFW_SAMPLES, 4)
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE)
  glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE)

  /*
    An unique identifier for this window
   */
  private val windowHandle = glfwCreateWindow(width, height, title, NULL, NULL)

  setInputCallbacks()

  glfwMakeContextCurrent(windowHandle)
  glfwSwapInterval(1)
  glfwShowWindow(windowHandle)
  createCapabilities()
  culling(true)
  depthTest(true)
  multisampling(true)
  glClearColor(0, 0, 0, 1)

  /**
   * GLFW lets you set callbacks for different window events
   */
  private def setInputCallbacks(): Unit = {
    glfwSetKeyCallback(windowHandle, InputHandler.KEY_CALLBACK)
    glfwSetCharCallback(windowHandle, InputHandler.TEXT_CALLBACK)
    glfwSetMouseButtonCallback(windowHandle, InputHandler.MOUSE_BUTTON_CALLBACK)
    glfwSetCursorPosCallback(windowHandle, InputHandler.MOUSE_CURSOR_CALLBACK)
    glfwSetScrollCallback(windowHandle, InputHandler.MOUSE_SCROLL_CALLBACK)
  }

  /**
   * Set the window to capture the cursor
   *
   * @param captured whether or not the cursor should be captured
   */
  def captureCursor(captured: Boolean): Unit = {
    if (captured) {
      glfwSetInputMode(windowHandle, GLFW_CURSOR, GLFW_CURSOR_DISABLED)
    } else {
      glfwSetInputMode(windowHandle, GLFW_CURSOR, GLFW_CURSOR_NORMAL)
    }
  }

  /**
   * Release the resources allocated for this window
   */
  def cleanUp(): Unit = {
    glfwFreeCallbacks(windowHandle)
    glfwDestroyWindow(windowHandle)

    glfwTerminate()
    glfwSetErrorCallback(null).free()
  }

  /**
   * Asks GLFW if this window has been asked to close
   *
   * @return if close button has been pressed
   */
  def shouldClose: Boolean = glfwWindowShouldClose(windowHandle)

  /**
   * Updates the OpenGl context
   */
  def update(): Unit = {
    glfwSwapBuffers(windowHandle)
  }

}
