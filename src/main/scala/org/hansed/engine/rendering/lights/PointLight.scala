package org.hansed.engine.rendering.lights

import org.hansed.engine.core.GameObject
import org.joml.Vector3f

/**
  * Represents a point light in the world
  *
  * @param color  color of the light
  * @param offset the offset from parent object
  * @author Hannes Sederholm
  */
case class PointLight(color: Vector3f, power: Float = 0.3f, offset: Vector3f = new Vector3f()) extends GameObject {
  position = offset
}
