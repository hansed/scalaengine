package org.hansed.engine.rendering

import org.hansed.engine.core.{GameObject, GameScene}
import org.hansed.engine.rendering.lights.PointLight
import org.hansed.engine.rendering.shaders._

import scala.collection.mutable

object RenderEngine {

  /**
    * Maximum number of lights / object
    */
  val MAX_LIGHTS = 4

  private val shaders: Seq[Shader[_]] = Seq(SkyboxShader,
    StaticShader, AnimatedShader, GuiShader)

  private val shaderNames: Map[String, Shader[_]] = shaders.map(shader => (shader.key, shader)).toMap

  val lights: mutable.Set[PointLight] = mutable.Set[PointLight]()

  def updateObject(o: GameObject): Unit = {
    if (o.clickId != -1) {
      HitBoxShader.includeObject(o)
    }
    o match {
      case light: PointLight =>
        if (!lights.contains(light)) {
          lights += light
          shaders.foreach({
            case shader: LightShader[_] =>
              shader.updateAllLights()
            case _ =>
          })
        }
      case _ =>
    }

    o.shaders.foreach(key => {
      shaderNames.get(key).foreach(_.includeObject(o))
    })

  }

  def removeObject(o: GameObject): Unit = {
    if (o.clickId != -1) {
      HitBoxShader.removeObject(o)
    }
    o match {
      case light: PointLight =>
        if (lights.contains(light)) {
          lights -= light
          shaders.foreach({
            case shader: LightShader[_] =>
              shader.updateAllLights()
            case _ =>
          })
        }
      case _ =>
    }

    o.shaders.foreach(key => {
      shaderNames.get(key).foreach(_.removeObject(o))
    })

  }

  def loadScene(scene: GameScene): Unit = {
    HitBoxShader.clearObjects()
    shaders.foreach(_.clearObjects())
    scene.rootObject.collectTransforms[GameObject]({
      case o: GameObject => Some(o)
      case _ => None
    }).foreach(o => {
      updateObject(o)
    })
  }

  def render(): Unit = {
    shaders.foreach(shader => {
      shader.bind()
      shader.renderAll()
      shader.unbind()
    })
  }

}
