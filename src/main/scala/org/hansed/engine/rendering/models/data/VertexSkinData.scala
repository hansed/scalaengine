package org.hansed.engine.rendering.models.data

import scala.collection.mutable

import VertexSkinData.MAX_WEIGHTS

/**
  * Contains data about a specific vertex skinning data. Used when animating the vertex
  *
  * @author Hannes Sederholm
  */
class VertexSkinData {

  val joints: mutable.Buffer[Int] = mutable.Buffer[Int]()
  var weights: mutable.Buffer[Float] = mutable.Buffer[Float]()

  /**
    * Adds a weight and a joint id to arrays and keeps them ordered
    *
    * @param joint  joint id
    * @param weight weight of corresponding joint
    */
  def addWeight(joint: Int, weight: Float): Unit = {
    if (weights.isEmpty || weight < weights.last) {
      weights += weight
      joints += joint
    } else {
      for (i <- weights.indices) {
        if (weights(i) <= weight) {
          weights.insert(i, weight)
          joints.insert(i, joint)
        }
      }
    }
  }

  /**
    * Only allow [[VertexSkinData.MAX_WEIGHTS]] weights / vertex
    * Removes extra weights and normalises what is left
    */
  def limitWeights(): Unit = {
    //remove extra
    while (weights.size > MAX_WEIGHTS) {
      weights.remove(weights.size - 1)
      joints.remove(joints.size - 1)
    }
    //fill missing
    while (weights.size < MAX_WEIGHTS) {
      weights += 0.0f
      joints += 0
    }

    //normalise the weights (makes the sum add up to 1)
    val sum: Float = weights.sum
    for (i <- weights.indices) {
      weights(i) = weights(i) / sum
    }
  }

}

object VertexSkinData {
  /**
    * Maximum number of weights affecting one joint
    */
  final val MAX_WEIGHTS = 3
}

