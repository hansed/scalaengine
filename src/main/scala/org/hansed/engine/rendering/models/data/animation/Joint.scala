package org.hansed.engine.rendering.models.data.animation

import org.joml.Matrix4f

import scala.collection.mutable

/**
  * A recursive joint structure for describing animation skeletons
  *
  * @param id             id of joint in skeleton
  * @param localTransform the transform of the bones head relative to its origin
  * @author Hannes Sederholm
  */
class Joint(val id: Int, val localTransform: Matrix4f) {

  /**
    * What it takes to go back to origin (this is needed for applying the pose)
    */
  val inverseBindTransform = new Matrix4f()
  /**
    * The transform that should currently be applied to this bone if rendered
    */
  var currentTransform: Matrix4f = new Matrix4f()

  /**
    * The joints that are attached to the end of this joint
    */
  val children: mutable.Buffer[Joint] = mutable.Buffer[Joint]()

  /**
    * Add a joint to the structure
    *
    * @param child child joint
    */
  def addChild(child: Joint): Unit = children += child

  /**
    * Calculate inverse bind transforms for a skeleton
    *
    * @param parentBindTransform bind transform of parent joint
    */
  def calculateInverseBindTransforms(parentBindTransform: Matrix4f): Unit = {
    val bindTransform = parentBindTransform.mul(localTransform, new Matrix4f())
    bindTransform.invert(inverseBindTransform)
    for (child <- children) {
      child.calculateInverseBindTransforms(bindTransform)
    }
  }

  /**
    * Collect a list of all the transforms related to this joint
    *
    * @param transforms list of transforms
    */
  def collectTransforms(transforms: mutable.Buffer[Joint]): Unit = {
    children.foreach(_.collectTransforms(transforms))
    transforms += this
  }

}
