package org.hansed.engine.rendering.models.data

import org.joml.Vector3f

/**
  * Holds data about a shape
  *
  * @param vertices   vertex coordinates
  * @param textures   texture coordinates
  * @param normals    normal vectors
  * @param indices    indices
  * @param dimensions the size of this shape (the farthest point on each axis)
  * @author Hannes Sederholm
  */
case class Shape(vertices: Array[Float], textures: Array[Float], normals: Array[Float], indices: Array[Int],
                 tangents: Array[Float], bitangents: Array[Float], dimensions: Vector3f) {

  /**
    * Counts the vertices in this shape
    *
    * @return number of vertices
    */
  def vertexCount: Int = indices.length
}
