package org.hansed.engine.rendering.models.stationary

import org.hansed.engine.core.GameComponent
import org.hansed.engine.rendering.textures.Material

/**
  * Represents a model with a material assigned to it
  *
  * @author Hannes Sederholm
  */
class ModelComponent(var model: StaticModel, var material: Material) extends GameComponent {

}
