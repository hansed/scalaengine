package org.hansed.engine.rendering.models.animated

import org.hansed.engine.Engine
import org.hansed.engine.core.GameComponent
import org.hansed.engine.rendering.models.animated.Animator._
import org.hansed.engine.rendering.models.data.animation.{Animation, Joint, JointTransform, KeyFrame}
import org.joml.{Matrix4f, Quaternionf, Vector3f}

import scala.collection.mutable

/**
  * Animates through the poses of an animation
  *
  * @param animation animation to animate
  * @author Hannes Sederholm
  */
class Animator(val animation: Animation) extends GameComponent {

  /**
    * Is the animation progressing right now?, by default starts automatically playing
    */
  var playing = true

  /**
    * Should the animation start from the beginning once its done
    */
  var autoRepeat = true

  /**
    * How far forward has the animation proceeded
    */
  private var progress: Float = 0

  /**
    * Resets the animation and sets auto-repeat off to play the animation through once
    */
  def playOnce(): Unit = {
    autoRepeat = false
    progress = 0
    playing = true
  }

  /**
    * Deal with playing the animation
    */
  override def update(): Unit = {
    if (playing) {
      progressAnimation()
    }
  }

  /**
    * Calculate the current poses in the animation
    *
    * @return interpolated poses between the nearest keyframes in the animation
    */
  def poses(): mutable.HashMap[Int, JointTransform] = {
    var currentFrame = animation.keyFrames(0)
    var nextFrame = animation.keyFrames(0)
    var flag = false
    for (frame <- animation.keyFrames) {
      if (!flag) {
        if (frame.timeStamp > progress) {
          nextFrame = frame
          flag = true
        }
        if (!flag)
          currentFrame = frame
      }
    }

    val frameLength = nextFrame.timeStamp - currentFrame.timeStamp
    val frameProgress = if (frameLength == 0) 0 else (progress - currentFrame.timeStamp) / frameLength
    interpolate(currentFrame, nextFrame, frameProgress)
  }

  /**
    * Move forward in the animation
    */
  private def progressAnimation(): Unit = {
    progress += Engine.updateDelta.toFloat / Engine.UNIT
    if (progress > animation.length) {
      if (autoRepeat) {
        progress %= animation.length
      } else {
        progress = 0
        playing = false
      }
    }
  }
}

object Animator {

  /**
    * Interpolate between two keyframes
    *
    * @param current  the first frame
    * @param next     the second frame
    * @param progress the progress percentage of the animation between the two frames
    * @return an interpolated frame
    */
  def interpolate(current: KeyFrame, next: KeyFrame, progress: Float): mutable.HashMap[Int, JointTransform] = {
    val firstPose = current.pose
    val nextPose = next.pose
    val result = mutable.HashMap[Int, JointTransform]()

    for (key <- firstPose.keySet) {
      val currentTransform = firstPose(key)
      val nextTransform = nextPose(key)
      result.put(key, JointTransform(currentTransform.position.lerp(nextTransform.position, progress, new
          Vector3f()), currentTransform.rotation.slerp(nextTransform.rotation, progress, new Quaternionf())))
    }
    result
  }

  /**
    * Animate the skeleton according to given pose
    *
    * @param joint           the root joint
    * @param parentTransform the transform of the joints parent
    * @param pose            the pose
    */
  def applyPoseToJoints(joint: Joint, parentTransform: Matrix4f, pose: mutable.HashMap[Int, JointTransform]): Unit = {
    val localTransform = pose(joint.id).transform
    val currentTransform = parentTransform.mul(localTransform, new Matrix4f())
    for (child <- joint.children) {
      applyPoseToJoints(child, currentTransform, pose)
    }
    currentTransform.mul(joint.inverseBindTransform)
    joint.currentTransform = currentTransform
  }
}
