package org.hansed.engine.rendering.models.loaders

import java.io.IOException

import org.hansed.engine.core.resources.ResourceLoader
import org.hansed.engine.rendering.models.data.Vertex
import org.hansed.util.xml.ElementWrapper
import org.jdom2.input.SAXBuilder
import org.joml.{Matrix4f, Vector2f, Vector3f, Vector4f}

import scala.collection.mutable

/**
  * Load COLLADA models that don't move (aren't animated)
  * Much like the WavefrontLoader, only follows the format as far as is required for the purposes of this program
  *
  * @see [[https://www.khronos.org/collada/]]
  * @author Hannes Sederholm
  */
object StaticColladaLoader extends ModelLoader {

  /**
    * A correction for the fact that blender uses z axis as up where as we use y axis (because OpenGL does)
    */
  private final val UP_AXIS_CORRECTION = new Matrix4f().rotate(Math.toRadians(-90).toFloat, new Vector3f(1, 0, 0))

  var meshName: String = _

  /**
    * Fill the data lists with data from specified file
    *
    * @param path path to file
    */
  override protected def readData(path: String): Unit = {
    val saxBuilder = new SAXBuilder()
    val document = saxBuilder.build(ResourceLoader.getInputStream(path))

    val classElement = document.getRootElement
    val rootElement = ElementWrapper(classElement)

    //Load geometry
    val geometry = rootElement.getChild("library_geometries").getChild("geometry")
    meshName = geometry.id
    loadGeometryData(geometry)
  }

  /**
    * Collect the semantics and sources of input elements inside a wrapper
    *
    * @param data the wrapper that contains the input element
    * @return HashMap of the data
    */
  private def collectInputs(data: ElementWrapper): mutable.HashMap[String, Array[String]] = {
    val sources = mutable.HashMap[String, Array[String]]()
    val inputs = data.getChildren("input")
    //store the offsets and sources of data
    inputs.foreach(element => sources.put(element.attribute("semantic"), Array(element.attribute("source"),
      element.attribute("offset"))))
    sources
  }

  private def loadGeometryData(wrapper: ElementWrapper): Unit = {
    val geometry = wrapper.getChild("mesh")
    val data = geometry.getChildOption("triangles").getOrElse(geometry.getChildOption("polylist").orNull)
    if (data == null)
      throw new IOException("Invalid COLLADA file, no indices found!")

    val indexStrings = data.getChild("p").textArray

    val sources = collectInputs(data)

    if (!sources.contains("VERTEX") || !sources.contains("NORMAL") || !sources.contains("TEXCOORD")) {
      throw new IOException("Missing vertex, normals or textures data!")
    }

    val stride = sources.size
    val vertexOffset = sources("VERTEX")(1).toInt
    val normalOffset = sources("NORMAL")(1).toInt
    val textureOffset = sources("TEXCOORD")(1).toInt

    //collect the indices data
    for (i <- indexStrings.indices by stride) {
      inIndices += indexStrings(i + vertexOffset).toInt
      inIndices += indexStrings(i + textureOffset).toInt
      inIndices += indexStrings(i + normalOffset).toInt
    }

    //use collected sources to gather positions, normals and texture coordinates
    val verticesElement = geometry.getChild("source", meshName + "-positions").getChild("float_array")
    val normalsElement = geometry.getChild("source", sources("NORMAL")(0)).getChild("float_array")
    val texturesElement = geometry.getChild("source", sources("TEXCOORD")(0)).getChild("float_array")

    var values = verticesElement.textArray.map(_.toFloat)
    for (i <- values.indices by 3) {
      val vector = new Vector4f(values(i), values(i + 1), values(i + 2), 1)
      UP_AXIS_CORRECTION.transform(vector, vector)
      vertices += Vertex(vertices.size, new Vector3f(vector.x, vector.y, vector.z))
    }

    //normals
    values = normalsElement.textArray.map(_.toFloat)
    for (i <- values.indices by 3) {
      val vector = new Vector4f(values(i), values(i + 1), values(i + 2), 1)
      UP_AXIS_CORRECTION.transform(vector, vector)
      normals += new Vector3f(vector.x, vector.y, vector.z)
    }

    //texture coordinates
    values = texturesElement.textArray.map(_.toFloat)
    for (i <- values.indices by 2) {
      textures += new Vector2f(values(i), values(i + 1))
    }
  }


}
