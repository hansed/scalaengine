package org.hansed.engine.rendering.models.loaders

import org.hansed.engine.core.resources.ResourceLoader
import org.hansed.engine.rendering.models.data
import org.joml.{Vector2f, Vector3f}

/**
  * A [[org.hansed.engine.rendering.models.loaders.ModelLoader]] for loading Wavefront (.obj) files
  *
  * Only extracts the data needed by the game and therefore doesn't fully follow the format
  *
  * @see [[https://en.wikipedia.org/wiki/Wavefront_.obj_file]]
  * @author Hannes Sederholm
  */
object WavefrontLoader extends ModelLoader {
  /**
    * Fill the data lists with data from specified file
    *
    * @param path path to file
    */
  override protected def readData(path: String): Unit = {
    ResourceLoader.readLines(path, _.foreach(line => {
      val split = line.split(" ")
      if (line.startsWith("v ")) {
        val vertex = new Vector3f(split(1).toFloat, split(2).toFloat, split(3).toFloat)
        vertices += data.Vertex(vertices.size, vertex)
      } else if (line.startsWith("vt ")) {
        textures += new Vector2f(split(1).toFloat, split(2).toFloat)
      } else if (line.startsWith("vn ")) {
        normals += new Vector3f(split(1).toFloat, split(2).toFloat, split(3).toFloat)
      } else if (line.startsWith("f ")) {
        split.tail.map(_.split("/")).foreach(_.foreach(v => {
          inIndices += v.toInt - 1
        }))
      }
    }))
  }
}
