package org.hansed.engine.rendering.models.stationary.terrain

import java.util.UUID

import org.hansed.engine.content.map.Terrain._
import org.hansed.engine.rendering.models.data.Shape
import org.hansed.engine.rendering.models.stationary.StaticModel
import org.hansed.engine.rendering.resources.ModelResource
import org.hansed.util.Logger
import org.joml.{Vector2f, Vector3f}

/**
  * Represents a terrain in a 3d world
  *
  * @param height        the height provider for the terrain
  * @param textureTiling a factor that is used to map the texture coordinates, greater factor makes texture image go
  *                      past faster
  * @author Hannes Sederholm
  */
class TerrainModel(height: HeightProvider, textureTiling: Float = 1.0f) extends StaticModel(UUID.randomUUID()
  .toString) {

  /**
    * We don't load terrains from files so we must override the default procedure
    */
  override def loadResource(path: String): Option[ModelResource] = {
    Some(new ModelResource(path) {
      /**
        * A method for loading the shape to allow overriding for alternate loading methods
        *
        * @return the shape stored in this resource
        */
      override protected def loadShape: Shape = {
        def calculateNormal(x: Int, z: Int): Vector3f = {
          val heightL = height(x - 1, z)
          val heightR = height(x + 1, z)
          val heightD = height(x, z - 1)
          val heightU = height(x, z + 1)
          new Vector3f(heightL - heightR, 1.0f, heightD - heightU).normalize()
        }

        val vertexCount = TERRAIN_VERTEX_COUNT
        val count = vertexCount * vertexCount

        val vertices = new Array[Float](count * 3)
        val normals = new Array[Float](count * 3)
        val textures = new Array[Float](count * 2)
        val tangents = new Array[Float](count * 3)
        val bitangents = new Array[Float](count * 3)

        def position(pointer: Int): Vector3f = {
          new Vector3f(vertices(pointer * 3), vertices(pointer * 3 + 1), vertices(pointer * 3 + 2))
        }

        def setTangents(v1: Int, v2: Int, v3: Int): Unit = {
          val posDelta1 = position(v2).sub(position(v1), new Vector3f())
          val posDelta2 = position(v3).sub(position(v1), new Vector3f())

          val uv1 = new Vector2f(textures(v1 * 2), textures(v1 * 2 + 1))
          val uv2 = new Vector2f(textures(v2 * 2), textures(v2 * 2 + 1))
          val uv3 = new Vector2f(textures(v3 * 2), textures(v3 * 2 + 1))

          val uv1Delta = uv2.sub(uv1, new Vector2f())
          val uv2Delta = uv3.sub(uv1, new Vector2f())

          val r: Float = 1.0f / (uv1Delta.x * uv2Delta.y - uv1Delta.y * uv2Delta.x)

          def validLength(value: Float): Boolean = {
            Math.abs(value) > 0.0001f
          }

          if (validLength(uv2Delta.y))
            posDelta1.normalize(uv2Delta.y)
          if (validLength(uv1Delta.y))
            posDelta2.normalize(uv1Delta.y)

          if (r.isInfinity) {
            return
          }

          val tangent = posDelta1.sub(posDelta2, new Vector3f()).normalize(r)

          if (validLength(uv1Delta.x))
            posDelta2.normalize(uv1Delta.x)
          if (validLength(uv2Delta.x))
            posDelta1.normalize(uv2Delta.x)
          val bitangent = posDelta2.sub(posDelta1, new Vector3f()).normalize(r)

          def setVector(pointer: Int, vector: Vector3f, array: Array[Float]): Unit = {
            array(pointer * 3) = vector.x
            array(pointer * 3 + 1) = vector.y
            array(pointer * 3 + 2) = vector.z
          }

          setVector(v1, tangent, tangents)
          setVector(v2, tangent, tangents)
          setVector(v3, tangent, tangents)

          setVector(v1, bitangent, bitangents)
          setVector(v2, bitangent, bitangents)
          setVector(v3, bitangent, bitangents)
        }

        val indices = new Array[Int](6 * (vertexCount - 1) * (vertexCount - 1))
        Logger.logAction("Constructing terrain", this) {

          //build the vertices
          var vertexPointer = 0

          Logger.logAction("Building vertices", this) {
            for (gridZ <- 0 until vertexCount) {
              for (gridX <- 0 until vertexCount) {
                val x: Int = (TERRAIN_SQUARE_SIZE * gridX).toInt
                val z: Int = (TERRAIN_SQUARE_SIZE * gridZ).toInt
                val pointHeight = height(x, z)

                vertices(vertexPointer * 3) = x
                vertices(vertexPointer * 3 + 1) = pointHeight
                vertices(vertexPointer * 3 + 2) = z

                val normal = calculateNormal(x, z)
                normals(vertexPointer * 3) = normal.x
                normals(vertexPointer * 3 + 1) = normal.y
                normals(vertexPointer * 3 + 2) = normal.z

                textures(vertexPointer * 2) = gridX.toFloat / (vertexCount - 1).toFloat * textureTiling
                textures(vertexPointer * 2 + 1) = gridZ.toFloat / (vertexCount - 1).toFloat * textureTiling
                vertexPointer += 1
              }
            }
          }

          //build the indices
          var pointer = 0

          def insertIndex(index: Int)(): Unit = {
            indices(pointer) = index
            pointer += 1
          }

          Logger.logAction("Building indices", this) {
            for (gridZ <- 0 until (vertexCount - 1)) {
              for (gridX <- 0 until (vertexCount - 1)) {
                val topLeft = (gridZ * vertexCount) + gridX
                val topRight = topLeft + 1
                val bottomLeft = ((gridZ + 1) * vertexCount) + gridX
                val bottomRight = bottomLeft + 1
                insertIndex(topLeft)
                insertIndex(bottomLeft)
                insertIndex(topRight)
                setTangents(topLeft, bottomLeft, topRight)
                insertIndex(topRight)
                insertIndex(bottomLeft)
                insertIndex(bottomRight)
                setTangents(topLeft, bottomLeft, bottomRight)
              }
            }
          }
        }
        Shape(vertices, textures, normals, indices, tangents, bitangents, new Vector3f(vertexCount, 1.0f,
          TERRAIN_SQUARE_SIZE))
      }
    }
    )
  }

}
