package org.hansed.engine.rendering.models.loaders

import java.io.IOException

import org.hansed.engine.rendering.models.data.animation.{Animation, Joint}
import org.hansed.engine.rendering.models.data.{SkinnedShape, Vertex, VertexSkinData}
import org.hansed.engine.rendering.models.loaders.ModelLoader._
import org.hansed.util.Logger
import org.joml.{Vector2f, Vector3f}

import scala.collection.mutable

/**
  * Abstract class AnimatedModelLoader provides the methods for processing a models vertex data
  * Implementations provide the methods to read the data from a file format.
  *
  * This is pretty much a copy of [[org.hansed.engine.rendering.models.loaders.ModelLoader]] but I feel it would take
  * longer than is worth to structure the code in a way that it could be reused
  *
  * @author Hannes Sederholm
  */
abstract class AnimatedModelLoader {

  protected val vertices: mutable.Buffer[Vertex] = mutable.Buffer[Vertex]()
  protected val textures: mutable.Buffer[Vector2f] = mutable.Buffer[Vector2f]()
  protected val normals: mutable.Buffer[Vector3f] = mutable.Buffer[Vector3f]()
  protected val inIndices: mutable.Buffer[Int] = mutable.Buffer[Int]()
  protected val vertexSkinData: mutable.Buffer[VertexSkinData] = mutable.Buffer[VertexSkinData]()

  private val outIndices: mutable.Buffer[Int] = mutable.Buffer[Int]()

  private var verticesArray: Array[Float] = _
  private var texturesArray: Array[Float] = _
  private var normalsArray: Array[Float] = _
  private var tangentsArray: Array[Float] = _
  private var bitangentsArray: Array[Float] = _
  private var weightsArray: Array[Float] = _
  private var jointIdsArray: Array[Int] = _

  private var dimensions: Vector3f = _

  protected var skeleton: Joint = _
  protected var animation: Animation = _

  /**
    * Load a model from a file
    *
    * @param path file path
    * @return a shape containing the necessary data about the model
    */
  def loadModel(path: String): SkinnedShape = {
    resetData()
    try {
      readData(path)
    } catch {
      case e: IOException =>
        Logger.err("Error parsing model file: " + path, this)
        e.printStackTrace()
      case e: Throwable => {
        Logger.err("Unexpected error occurred loading file: " + path, this)
      }
        e.printStackTrace()
    }
    //construct the out indices list
    processData()
    //Construct data arrays to store data in
    verticesArray = new Array[Float](vertices.size * 3)
    texturesArray = new Array[Float](vertices.size * 2)
    normalsArray = new Array[Float](vertices.size * 3)
    tangentsArray = new Array[Float](vertices.size * 3)
    bitangentsArray = new Array[Float](vertices.size * 3)

    weightsArray = new Array[Float](vertices.size * VertexSkinData.MAX_WEIGHTS)
    jointIdsArray = new Array[Int](vertices.size * VertexSkinData.MAX_WEIGHTS)
    dimensions = new Vector3f()

    //Fill those arrays
    fillDataArrays()

    //store the data arrays in a shape object and return that
    new SkinnedShape(verticesArray, texturesArray, normalsArray, outIndices.toArray, tangentsArray, bitangentsArray,
      weightsArray, jointIdsArray,
      skeleton, animation, dimensions)
  }

  /**
    * Just fills the arrays based on what the indices are defining
    */
  private def fillDataArrays(): Unit = {
    for (i <- vertices.indices) {
      val currentVertex = vertices(i)
      val position = currentVertex.position
      val textureCoordinate = textures(currentVertex.textureIndex.getOrElse(0))
      val normalVector = normals(currentVertex.normalIndex.getOrElse(0))
      val tangent = currentVertex.averageTangent
      val bitangent = currentVertex.averageBitangent

      def f(a: Float, b: Float): Float = {
        if (Math.abs(a) > Math.abs(b))
          a
        else b
      }

      dimensions.set(f(dimensions.x, position.x), f(dimensions.y, position.y), f(dimensions.z, position.z))

      verticesArray(i * 3) = position.x
      verticesArray(i * 3 + 1) = position.y
      verticesArray(i * 3 + 2) = position.z
      texturesArray(i * 2) = textureCoordinate.x
      texturesArray(i * 2 + 1) = 1 - textureCoordinate.y
      normalsArray(i * 3) = normalVector.x
      normalsArray(i * 3 + 1) = normalVector.y
      normalsArray(i * 3 + 2) = normalVector.z
      tangentsArray(i * 3) = tangent.x
      tangentsArray(i * 3 + 1) = tangent.y
      tangentsArray(i * 3 + 2) = tangent.z
      bitangentsArray(i * 3) = bitangent.x
      bitangentsArray(i * 3 + 1) = bitangent.y
      bitangentsArray(i * 3 + 2) = bitangent.z
      currentVertex.skinData.foreach(skinData => {
        (0 until VertexSkinData.MAX_WEIGHTS).foreach((index: Int) => {
          jointIdsArray(i * VertexSkinData.MAX_WEIGHTS + index) = skinData.joints(index)
          weightsArray(i * VertexSkinData.MAX_WEIGHTS + index) = skinData.weights(index)
        })
      })

    }
  }

  /**
    * Construct the data arrays out of loaded data that is stored in the buffers
    */
  private def processData(): Unit = {
    for (index <- inIndices.indices by 9) {
      val v1 = processVertex(index)
      val v2 = processVertex(index + 3)
      val v3 = processVertex(index + 6)
      calculateTangent(v1, v2, v3, textures)
    }
  }

  /**
    * Extract the data for a vertex starting in indices list at given index
    * index + 0 contains positionIndex
    * index + 1 contains textureIndex
    * index + 2 contains normalIndex (surface normal)
    *
    * @param index starting index
    */
  private def processVertex(index: Int): Vertex = {
    val vertexIndex = inIndices(index)
    val textureIndex = inIndices(index + 1)
    val normalIndex = inIndices(index + 2)

    val currentVertex = vertices(vertexIndex)

    if (currentVertex.isSet) {
      processDuplicateVertex(currentVertex, textureIndex, normalIndex, outIndices, vertices)
    } else {
      currentVertex.textureIndex = Some(textureIndex)
      currentVertex.normalIndex = Some(normalIndex)
      outIndices += vertexIndex
    }
    currentVertex
  }

  /**
    * Called between every loaded model
    */
  private def resetData(): Unit = {
    vertices.clear()
    textures.clear()
    normals.clear()
    vertexSkinData.clear()
    inIndices.clear()
    outIndices.clear()
  }

  /**
    * Fill the data lists with data from specified file
    *
    * @param path path to file
    */
  protected def readData(path: String): Unit

}

object AnimatedModelLoader {
  /**
    * Load a model with the appropriate loader
    *
    * @param path to model file
    * @return a shape
    */
  def loadModel(path: String): SkinnedShape = {
    if (path.endsWith(".dae")) {
      AnimatedColladaLoader.loadModel(path)
    } else {
      throw new IllegalArgumentException("Invalid animated model file: " + path)
    }
  }
}

