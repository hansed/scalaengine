package org.hansed.engine.rendering.models.data.animation

/**
  * Represents an animation that may be played on a skinned model
  *
  * @param length    the length of the animation
  * @param keyFrames the keyframes of the animation
  * @author Hannes Sederholm
  */
case class Animation(length: Float, keyFrames: Array[KeyFrame])
