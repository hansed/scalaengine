package org.hansed.engine.rendering.models

import org.hansed.engine.core.GameObject
import org.hansed.engine.rendering.models.animated.{AnimatedModel, AnimatedRenderer}
import org.hansed.engine.rendering.models.stationary.{ModelComponent, StaticModel}
import org.hansed.engine.rendering.shaders.StaticShader
import org.hansed.engine.saving.DefaultSaveProtocol.ModelDefinitionLoader
import org.hansed.engine.saving.ModelDefinition

/**
  * A [[org.hansed.engine.core.GameObject]] that represents a 3D model in the world
  * Deals with loading animated or static models, so that you can pass any (of the two) type of model config as
  * parameter and it will load correctly
  *
  * @param config the ModelDefinition that defines this Model
  * @author Hannes Sederholm
  */
class Model(val config: ModelDefinition) extends GameObject {

  def this(configName: String) = {
    this(ModelDefinitionLoader.config(configName))
  }

  //if config is animated we need to create an animated renderer otherwise static
  if (config.animated) {
    addComponent(new AnimatedRenderer(new AnimatedModel(config.model), config.material))
  } else {
    addComponent(new ModelComponent(new StaticModel(config.model), config.material))
    shaders += StaticShader.key
  }

}
