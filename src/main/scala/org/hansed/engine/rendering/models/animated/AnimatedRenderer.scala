package org.hansed.engine.rendering.models.animated

import org.hansed.engine.core.{GameComponent, GameObject}
import org.hansed.engine.rendering.shaders.AnimatedShader
import org.hansed.engine.rendering.textures.Material


/**
  * Animated renderer component
  * Renders an animated model at the location of the parent component
  *
  * @param model    model to render
  * @param material material
  * @author Hannes Sederholm
  */
class AnimatedRenderer(val model: AnimatedModel, val material: Material) extends GameComponent {

  val animator = new Animator(model.resource.shape.animation)

  /**
    * Called when the component is created. Should be used for initiating the component
    *
    * @param parent the parent object of this component
    */
  override def prepare(parent: GameObject): Unit = {
    parent.shaders += AnimatedShader.key
  }

  /**
    * Called once every update tick for all components
    */
  override def update(): Unit = {
    animator.update()
  }

}

