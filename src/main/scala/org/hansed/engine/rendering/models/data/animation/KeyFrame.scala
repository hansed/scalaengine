package org.hansed.engine.rendering.models.data.animation

import scala.collection.mutable

/**
  * Single keyframe in an animation
  *
  * @param timeStamp time of the frame
  * @param pose      pose at the time
  * @author Hannes Sederholm
  */
class KeyFrame(val timeStamp: Float, val pose: mutable.HashMap[Int, JointTransform]) {

  /**
    * Add a transform to this keyframe
    *
    * @param id        the id of the joint to transform
    * @param transform the transform
    */
  def addTransform(id: Int, transform: JointTransform): Unit = {
    pose.put(id, transform)
  }
}
