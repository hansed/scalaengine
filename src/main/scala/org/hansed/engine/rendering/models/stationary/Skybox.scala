package org.hansed.engine.rendering.models.stationary

import org.hansed.engine.core.resources.ResourceUser
import org.hansed.engine.rendering.models.stationary.Skybox._
import org.hansed.engine.rendering.resources.SkyboxResource
import org.hansed.util.GLUtil._
import org.lwjgl.opengl.GL11.{GL_FLOAT, GL_TRIANGLES, glDrawArrays}
import org.lwjgl.opengl.GL15.{GL_ARRAY_BUFFER, GL_STATIC_DRAW, glBindBuffer, glBufferData}
import org.lwjgl.opengl.GL20.{glDisableVertexAttribArray, glEnableVertexAttribArray, glVertexAttribPointer}
import org.lwjgl.opengl.GL30.glBindVertexArray

/**
  * Represents a skybox cube
  *
  * @param texturePath path to the folder that contains the textures
  * @author Hannes Sederholm
  */
class Skybox(texturePath: String) extends ResourceUser[SkyboxResource](texturePath) {

  /**
    * Set this resource as current, whatever that might mean depends on the type of the resource
    */
  override def bind(): Unit = {
    glBindVertexArray(resource.vao)
    glEnableVertexAttribArray(0)
    resource.texture.bind()
  }

  /**
    * Return from what bind does to the default state
    * Note that if the system related to the resource wasn't at default state before bind the state will be different
    */
  override def unbind(): Unit = {
    glDisableVertexAttribArray(0)
    glBindVertexArray(0)
    resource.texture.unbind()
  }

  /**
    * Loads the resource
    * Called when no cached resource was found
    *
    * @param key resource key
    * @return resource wrapped in Option
    */
  override def loadResource(key: String): Option[SkyboxResource] = {
    val res = new SkyboxResource(key)
    glBindVertexArray(res.vao)
    glEnableVertexAttribArray(0)
    glBindBuffer(GL_ARRAY_BUFFER, res.vbo(0))
    glBufferData(GL_ARRAY_BUFFER, BOX_VERTICES, GL_STATIC_DRAW)
    glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0)
    Some(res)
  }

  /**
    * Draw the skybox
    */
  def draw(): Unit = {
    bind()
    depthTest(false)
    glDrawArrays(GL_TRIANGLES, 0, BOX_VERTICES.length / 3)
    depthTest(true)
    unbind()
  }
}

object Skybox {

  val BOX_SIZE: Float = 50.0f

  val BOX_VERTICES: Array[Float] = Array[Float](-BOX_SIZE, BOX_SIZE, -BOX_SIZE, -BOX_SIZE, -BOX_SIZE, -BOX_SIZE,
    BOX_SIZE, -BOX_SIZE, -BOX_SIZE, BOX_SIZE,
    -BOX_SIZE, -BOX_SIZE, BOX_SIZE, BOX_SIZE, -BOX_SIZE, -BOX_SIZE,
    BOX_SIZE, -BOX_SIZE,

    -BOX_SIZE, -BOX_SIZE, BOX_SIZE, -BOX_SIZE, -BOX_SIZE, -BOX_SIZE, -BOX_SIZE, BOX_SIZE, -BOX_SIZE, -BOX_SIZE,
    BOX_SIZE, -BOX_SIZE, -BOX_SIZE, BOX_SIZE,
    BOX_SIZE,
    -BOX_SIZE, -BOX_SIZE, BOX_SIZE,

    BOX_SIZE, -BOX_SIZE, -BOX_SIZE, BOX_SIZE, -BOX_SIZE, BOX_SIZE, BOX_SIZE, BOX_SIZE, BOX_SIZE, BOX_SIZE, BOX_SIZE,
    BOX_SIZE, BOX_SIZE, BOX_SIZE, -BOX_SIZE, BOX_SIZE,
    -BOX_SIZE, -BOX_SIZE,

    -BOX_SIZE, -BOX_SIZE, BOX_SIZE, -BOX_SIZE, BOX_SIZE, BOX_SIZE, BOX_SIZE, BOX_SIZE, BOX_SIZE, BOX_SIZE, BOX_SIZE,
    BOX_SIZE, BOX_SIZE, -BOX_SIZE, BOX_SIZE, -BOX_SIZE,
    -BOX_SIZE, BOX_SIZE,

    -BOX_SIZE, BOX_SIZE, -BOX_SIZE, BOX_SIZE, BOX_SIZE, -BOX_SIZE, BOX_SIZE, BOX_SIZE, BOX_SIZE, BOX_SIZE, BOX_SIZE,
    BOX_SIZE, -BOX_SIZE, BOX_SIZE, BOX_SIZE, -BOX_SIZE,
    BOX_SIZE, -BOX_SIZE,

    -BOX_SIZE, -BOX_SIZE, -BOX_SIZE, -BOX_SIZE, -BOX_SIZE, BOX_SIZE, BOX_SIZE, -BOX_SIZE, -BOX_SIZE, BOX_SIZE,
    -BOX_SIZE, -BOX_SIZE, -BOX_SIZE, -BOX_SIZE, BOX_SIZE,
    BOX_SIZE, -BOX_SIZE, BOX_SIZE)
}