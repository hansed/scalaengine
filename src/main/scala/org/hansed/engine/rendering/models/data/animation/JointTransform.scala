package org.hansed.engine.rendering.models.data.animation

import org.joml.{Matrix4f, Quaternionf, Vector3f}

/**
  * Represents a tranformation of a joint during an animation
  *
  * @param position the position of the head of the joint
  * @param rotation the rotation of the joint
  * @author Hannes Sederholm
  */
case class JointTransform(position: Vector3f, rotation: Quaternionf) {

  /**
    * The transformation matrix describing this joint
    */
  val transform: Matrix4f = {
    new Matrix4f().translate(position).rotate(rotation)
  }

}
