package org.hansed.engine.rendering.models.loaders

import java.io.IOException

import org.hansed.engine.core.resources.ResourceLoader
import org.hansed.engine.rendering.models.data.animation.{Animation, Joint, JointTransform, KeyFrame}
import org.hansed.engine.rendering.models.data.{Vertex, VertexSkinData}
import org.hansed.util.Logger
import org.hansed.util.xml.ElementWrapper
import org.jdom2.input.SAXBuilder
import org.joml.{Matrix4f, Quaternionf, Vector2f, Vector3f, Vector4f}

import scala.collection.mutable

/**
  * Load animated collada models
  *
  * @author Hannes Sederholm
  */
object AnimatedColladaLoader extends AnimatedModelLoader {

  private final val UP_AXIS_CORRECTION = new Matrix4f().rotate(Math.toRadians(-90).toFloat, new Vector3f(1, 0, 0))

  var meshName: String = _

  private var jointNames: mutable.Buffer[String] = _

  /**
    * Fill the data lists with data from specified file
    *
    * @param path path to file
    */
  override protected def readData(path: String): Unit = {
    val saxBuilder = new SAXBuilder()
    val document = saxBuilder.build(ResourceLoader.getInputStream(path))

    val classElement = document.getRootElement
    val rootElement = ElementWrapper(classElement)

    //Load geometry
    val geometry = rootElement.getChild("library_geometries").getChild("geometry")
    meshName = geometry.id
    loadGeometryData(geometry)

    //Load skin
    val skin = rootElement.getChild("library_controllers").getChild("controller").getChild("skin")
    loadSkinData(skin)

    //Load skeleton
    val skeleton = rootElement.getChild("library_visual_scenes").getChild("visual_scene")
      .getChildWithIdContaining("node", "Armature").getChild("node")
    loadSkeletonData(skeleton)

    //Load animations
    val animations = rootElement.getChild("library_animations")
    loadAnimationData(animations)

  }

  /**
    * Collect the semantics and sources of input elements inside a wrapper
    *
    * @param data the wrapper that contains the input element
    * @return HashMap of the data
    */
  private def collectInputs(data: ElementWrapper): mutable.HashMap[String, Array[String]] = {
    val sources = mutable.HashMap[String, Array[String]]()
    val inputs = data.getChildren("input")
    //store the offsets and sources of data
    inputs.foreach(element => sources.put(element.attribute("semantic"), Array(element.attribute("source"),
      element.attribute("offset"))))
    sources
  }

  /**
    * Load geometry data from a COLLADA file
    *
    * @param wrapper geometry node
    */
  private def loadGeometryData(wrapper: ElementWrapper): Unit = {
    val geometry = wrapper.getChild("mesh")
    val data = geometry.getChildOption("triangles").getOrElse(geometry.getChildOption("polylist").orNull)
    if (data == null)
      throw new IOException("Invalid COLLADA file, no indices found!")

    val indexStrings = data.getChild("p").textArray

    val sources = collectInputs(data)

    if (!sources.contains("VERTEX") || !sources.contains("NORMAL") || !sources.contains("TEXCOORD")) throw new
        IOException("Missing vertex, normals or textures data!")

    val stride = sources.size
    val vertexOffset = sources("VERTEX")(1).toInt
    val normalOffset = sources("NORMAL")(1).toInt
    val textureOffset = sources("TEXCOORD")(1).toInt

    //collect the indices data
    for (i <- indexStrings.indices by stride) {
      inIndices += indexStrings(i + vertexOffset).toInt
      inIndices += indexStrings(i + textureOffset).toInt
      inIndices += indexStrings(i + normalOffset).toInt
    }

    //use collected sources to gather positions, normals and texture coordinates
    val verticesElement = geometry.getChild("source", meshName + "-positions").getChild("float_array")
    val normalsElement = geometry.getChild("source", sources("NORMAL")(0)).getChild("float_array")
    val texturesElement = geometry.getChild("source", sources("TEXCOORD")(0)).getChild("float_array")

    var values = verticesElement.textArray.map(_.toFloat)
    for (i <- values.indices by 3) {
      val vector = new Vector4f(values(i), values(i + 1), values(i + 2), 1)
      UP_AXIS_CORRECTION.transform(vector, vector)
      vertices += Vertex(vertices.size, new Vector3f(vector.x, vector.y, vector.z))
    }

    //normals
    values = normalsElement.textArray.map(_.toFloat)
    for (i <- values.indices by 3) {
      val vector = new Vector4f(values(i), values(i + 1), values(i + 2), 1)
      UP_AXIS_CORRECTION.transform(vector, vector)
      normals += new Vector3f(vector.x, vector.y, vector.z)
    }

    //texture coordinates
    values = texturesElement.textArray.map(_.toFloat)
    for (i <- values.indices by 2) {
      textures += new Vector2f(values(i), values(i + 1))
    }
  }

  /**
    * Load skin data from a COLLADA file
    *
    * @param skin skin node
    */
  private def loadSkinData(skin: ElementWrapper): Unit = {
    val weightsData: ElementWrapper = skin.getChild("vertex_weights")
    val sources = collectInputs(weightsData)

    jointNames = skin.getChild("source", sources("JOINT")(0)).getChild("Name_array").textArray.toBuffer
    val weights: Array[Float] = skin.getChild("source", sources("WEIGHT")(0)).getChild("float_array").textArray.map(_
      .toFloat)
    val vCounts = weightsData.getChild("vcount").textArray.map(_.toInt)
    val values = weightsData.getChild("v").textArray.map(_.toInt)

    var dataIndex = 0
    var vertexCount = 0
    for (count <- vCounts) {
      val vertexSkinData = new VertexSkinData()
      for (_ <- 0 until count) {
        vertexSkinData.addWeight(values(dataIndex), weights(values(dataIndex + 1)))
        dataIndex += 2
      }
      vertexSkinData.limitWeights()
      vertices(vertexCount).skinData = Some(vertexSkinData)
      vertexCount += 1
    }
  }

  /**
    * Load animation keyframes from COLLADA file
    *
    * @param animationsNode root node of the animation
    */
  private def loadAnimationData(animationsNode: ElementWrapper): Unit = {
    val poses = animationsNode.getChildren("animation")

    var keyframes: Array[KeyFrame] = null

    var length = 0.0f
    for (i <- jointNames.indices) {
      val times = poses(i).getChildren("source").head.getChild("float_array").textArray.map(_.toFloat)
      val floats = poses(i).getChildren("source")(1).getChild("float_array").textArray.map(_.toFloat)
      if (keyframes == null) {
        keyframes = new Array[KeyFrame](times.length)
      }
      length = Math.max(times(times.length - 1), length)
      val matrix = new Array[Float](16)
      for (keyframe <- times.indices) {
        if (keyframes(keyframe) == null) {
          keyframes(keyframe) = new KeyFrame(times(keyframe), mutable.HashMap())
        }
        for (index <- 0 until 16) {
          matrix(index) = floats(keyframe * 16 + index)
        }
        val mat = new Matrix4f().set(matrix).transpose()
        if (i == 0) {
          UP_AXIS_CORRECTION.mul(mat, mat)
        }

        val position = mat.getTranslation(new Vector3f())
        val rotation = mat.getNormalizedRotation(new Quaternionf())

        val transform = JointTransform(position, rotation)
        keyframes(keyframe).addTransform(i, transform)
      }
    }
    animation = Animation(length, keyframes)
  }

  /**
    * Load the skeleton data
    *
    * @param rootNode node element in COLLADA file
    */
  private def loadSkeletonData(rootNode: ElementWrapper): Unit = {
    skeleton = extractJoint(rootNode)
    addChildren(skeleton, rootNode)
    skeleton.calculateInverseBindTransforms(new Matrix4f())
  }

  /**
    * Collect all the children of a joint
    *
    * @param parent  joint whose children need to be extracted
    * @param wrapper from which parent was extracted
    */
  private def addChildren(parent: Joint, wrapper: ElementWrapper): Unit = {
    wrapper.getChildren("node").foreach(e => {
      val child = extractJoint(e)
      if (child != null) {
        parent.addChild(child)
        addChildren(child, e)
      }
    })
  }

  /**
    * Collect the joint data from a node element in the COLLADA file
    *
    * @param wrapper node element
    * @return Joint
    */
  private def extractJoint(wrapper: ElementWrapper): Joint = {
    val id = jointNames.indexOf(wrapper.attribute("name").replaceAll("[. ]", "_"))
    if (id == -1) {
      Logger.log("Found a transform without a bone!", this)
      return null
    }
    val matrixData = wrapper.getChild("matrix").textArray.map(_.toFloat)
    val matrix = new Matrix4f().set(matrixData).transpose()
    if (id == 0) UP_AXIS_CORRECTION.mul(matrix, matrix)
    new Joint(id, matrix)
  }
}

