package org.hansed.engine.rendering.models.stationary

import org.hansed.engine.core.resources.ResourceUser
import org.hansed.engine.rendering.resources.ModelResource
import org.lwjgl.opengl.GL11._
import org.lwjgl.opengl.GL15._
import org.lwjgl.opengl.GL20._
import org.lwjgl.opengl.GL30.glBindVertexArray

/**
  * Represents a model that can't move
  *
  * @param key model key
  * @author Hannes Sederholm
  */
class StaticModel(key: String) extends ResourceUser[ModelResource](key) {

  /**
    * Set this resource as current, whatever that might mean depends on the type of the resource
    */
  override def bind(): Unit = {
    glBindVertexArray(resource.vao)
    resource.enabledAttributes.foreach(glEnableVertexAttribArray)
  }

  /**
    * Return from what bind does to the default state
    * Note that if the system related to the resource wasn't at default state before bind the state will be different
    */
  override def unbind(): Unit = {
    resource.enabledAttributes.foreach(glDisableVertexAttribArray)
    glBindBuffer(GL_ARRAY_BUFFER, 0)
    glBindVertexArray(0)
  }

  /**
    * Binds the model and then draws it
    */
  def draw(): Unit = {
    bind()
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, resource.ibo)
    glDrawElements(GL_TRIANGLES, resource.shape.vertexCount, GL_UNSIGNED_INT, 0)
    unbind()
  }

  /**
    * Create the model resource
    */
  override def loadResource(path: String): Option[ModelResource] = {
    Some(new ModelResource(path))
  }
}
