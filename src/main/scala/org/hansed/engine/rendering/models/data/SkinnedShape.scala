package org.hansed.engine.rendering.models.data

import org.hansed.engine.rendering.models.data.animation.{Animation, Joint}
import org.joml.Vector3f

/**
  * A shape that has some skinning data attached to it for animating
  *
  * @param weights   weights of different joints (bones)
  * @param jointIds  ids of joints
  * @param skeleton  structure of joints
  * @param animation data about the keyframes
  * @author Hannes Sederholm
  */
class SkinnedShape(vertices: Array[Float], textures: Array[Float], normals: Array[Float], indices: Array[Int],
                   tangents: Array[Float], bitangents: Array[Float], val weights: Array[Float],
                   val jointIds: Array[Int], val skeleton: Joint, val animation: Animation,
                   dimensions: Vector3f)
  extends Shape(vertices, textures, normals, indices, tangents, bitangents, dimensions) {

}

