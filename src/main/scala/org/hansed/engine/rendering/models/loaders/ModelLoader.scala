package org.hansed.engine.rendering.models.loaders

import java.io.IOException

import org.hansed.engine.rendering.models.data.{Shape, Vertex}
import org.hansed.engine.rendering.models.loaders.ModelLoader._
import org.hansed.util.Logger
import org.joml.{Vector2f, Vector3f}

import scala.collection.mutable

/**
  * Abstract class ModelLoader provides the methods for processing a models vertex data
  * Implementations provide the methods to read the data from a file format.
  *
  * @author Hannes Sederholm
  */
abstract class ModelLoader {

  protected val vertices: mutable.Buffer[Vertex] = mutable.Buffer[Vertex]()
  protected val textures: mutable.Buffer[Vector2f] = mutable.Buffer[Vector2f]()
  protected val normals: mutable.Buffer[Vector3f] = mutable.Buffer[Vector3f]()
  protected val inIndices: mutable.Buffer[Int] = mutable.Buffer[Int]()

  private val outIndices: mutable.Buffer[Int] = mutable.Buffer[Int]()

  private var verticesArray: Array[Float] = _
  private var texturesArray: Array[Float] = _
  private var normalsArray: Array[Float] = _
  private var tangentsArray: Array[Float] = _
  private var bitangentsArray: Array[Float] = _

  private var dimensions: Vector3f = _

  /**
    * Load a model from a file
    *
    * @param path file path
    * @return a shape containing the necessary data about the model
    */
  def loadModel(path: String): Shape = synchronized {
    resetData()
    try {
      readData(path)
    } catch {
      case e: IOException =>
        Logger.err("Error parsing model file: " + path + "\n", this)
        e.printStackTrace()
      case e: Throwable =>
        Logger.err("Unexpected error occurred loading file: " + path, this)
        e.printStackTrace()
    }
    //construct the out indices list
    processData()
    //Construct data arrays to store data in
    verticesArray = new Array[Float](vertices.size * 3)
    texturesArray = new Array[Float](vertices.size * 2)
    normalsArray = new Array[Float](vertices.size * 3)
    tangentsArray = new Array[Float](vertices.size * 3)
    bitangentsArray = new Array[Float](vertices.size * 3)
    dimensions = new Vector3f()
    //Fill those arrays
    fillDataArrays()

    //store the data arrays in a shape object and return that
    Shape(verticesArray, texturesArray, normalsArray, outIndices.toArray, tangentsArray, bitangentsArray, dimensions)
  }

  /**
    * Just fills the arrays based on what the indices are defining
    */
  private def fillDataArrays(): Unit = {
    for (i <- vertices.indices) {
      val currentVertex = vertices(i)
      val position = currentVertex.position
      val textureCoordinate = textures(currentVertex.textureIndex.getOrElse(0))
      val normalVector = normals(currentVertex.normalIndex.getOrElse(0))
      val tangent = currentVertex.averageTangent
      val bitangent = currentVertex.averageBitangent

      def f(a: Float, b: Float): Float = {
        if (Math.abs(a) > Math.abs(b))
          a
        else b
      }

      dimensions.set(f(dimensions.x, position.x), f(dimensions.y, position.y), f(dimensions.z, position.z))

      verticesArray(i * 3) = position.x
      verticesArray(i * 3 + 1) = position.y
      verticesArray(i * 3 + 2) = position.z
      texturesArray(i * 2) = textureCoordinate.x
      texturesArray(i * 2 + 1) = 1 - textureCoordinate.y
      normalsArray(i * 3) = normalVector.x
      normalsArray(i * 3 + 1) = normalVector.y
      normalsArray(i * 3 + 2) = normalVector.z
      tangentsArray(i * 3) = tangent.x
      tangentsArray(i * 3 + 1) = tangent.y
      tangentsArray(i * 3 + 2) = tangent.z
      bitangentsArray(i * 3) = bitangent.x
      bitangentsArray(i * 3 + 1) = bitangent.y
      bitangentsArray(i * 3 + 2) = bitangent.z
    }
  }

  /**
    * Construct the data arrays out of loaded data that is stored in the buffers
    */
  private def processData(): Unit = {
    for (index <- inIndices.indices by 9) {
      val v1 = processVertex(index)
      val v2 = processVertex(index + 3)
      val v3 = processVertex(index + 6)

      calculateTangent(v1, v2, v3, textures)

    }
  }

  /**
    * Extract the data for a vertex starting in indices list at given index
    * index + 0 contains positionIndex
    * index + 1 contains textureIndex
    * index + 2 contains normalIndex (surface normal)
    *
    * @param index starting index
    */
  private def processVertex(index: Int): Vertex = {
    val vertexIndex = inIndices(index)
    val textureIndex = inIndices(index + 1)
    val normalIndex = inIndices(index + 2)

    val currentVertex = vertices(vertexIndex)

    if (currentVertex.isSet) {
      processDuplicateVertex(currentVertex, textureIndex, normalIndex, outIndices, vertices)
    } else {
      currentVertex.textureIndex = Some(textureIndex)
      currentVertex.normalIndex = Some(normalIndex)
      outIndices += vertexIndex
    }
    currentVertex
  }

  /**
    * Called between every loaded model
    */
  private def resetData(): Unit = {
    vertices.clear()
    textures.clear()
    normals.clear()
    inIndices.clear()
    outIndices.clear()
  }

  /**
    * Fill the data lists with data from specified file
    *
    * @param path path to file
    */
  protected def readData(path: String): Unit
}

object ModelLoader {

  /**
    * Calculate the tangent of a triangle specified by 3 vertices and add it as a tangent of the vertices
    *
    * @param v1 vertex 1
    * @param v2 vertex 2
    * @param v3 vertex 3
    */
  def calculateTangent(v1: Vertex, v2: Vertex, v3: Vertex, textures: mutable.Buffer[Vector2f]): Unit = {
    val posDelta1 = v2.position.sub(v1.position, new Vector3f())
    val posDelta2 = v3.position.sub(v1.position, new Vector3f())

    val uv1 = textures(v1.textureIndex.getOrElse(0))
    val uv2 = textures(v2.textureIndex.getOrElse(0))
    val uv3 = textures(v3.textureIndex.getOrElse(0))

    val uv1Delta = uv2.sub(uv1, new Vector2f())
    val uv2Delta = uv3.sub(uv1, new Vector2f())

    val r: Float = 1.0f / (uv1Delta.x * uv2Delta.y - uv1Delta.y * uv2Delta.x)

    def validLength(value: Float): Boolean = {
      Math.abs(value) > 0.0001f
    }

    if (validLength(uv2Delta.y))
      posDelta1.normalize(uv2Delta.y)
    if (validLength(uv1Delta.y))
      posDelta2.normalize(uv1Delta.y)

    if (r.isInfinity) {
      return
    }

    val tangent = posDelta1.sub(posDelta2, new Vector3f()).normalize(r)

    if (validLength(uv1Delta.x))
      posDelta2.normalize(uv1Delta.x)
    if (validLength(uv2Delta.x))
      posDelta1.normalize(uv2Delta.x)
    val bitangent = posDelta2.sub(posDelta1, new Vector3f()).normalize(r)

    v1.addTangent(tangent, bitangent)
    v2.addTangent(tangent, bitangent)
    v3.addTangent(tangent, bitangent)


  }

  /**
    * Attempt to reuse vertices if possible. If vertex has the same data for normals, textures and position it is
    * considered to be the same
    *
    * @param previousVertex a vertex that is a candidate for being the same vertex
    * @param textureIndex   the new texture index
    * @param normalIndex    the new normal index
    * @param indices        current indices list
    * @param vertices       current vertex list
    * @return a new vertex
    */
  @scala.annotation.tailrec
  def processDuplicateVertex(previousVertex: Vertex, textureIndex: Int, normalIndex: Int, indices: mutable
  .Buffer[Int], vertices: mutable.Buffer[Vertex]): Vertex = {
    if (previousVertex.equalsByTextureAndNormal(textureIndex, normalIndex)) {
      indices += previousVertex.index
      previousVertex
    } else {
      if (previousVertex.duplicate.isDefined) {
        processDuplicateVertex(previousVertex.duplicate.get, textureIndex, normalIndex, indices, vertices)
      } else {
        val duplicate = Vertex(vertices.size, previousVertex.position, previousVertex.tangents, previousVertex
          .bitangents, previousVertex.skinData)
        duplicate.normalIndex = Some(normalIndex)
        duplicate.textureIndex = Some(textureIndex)
        previousVertex.duplicate = Some(duplicate)
        vertices += duplicate
        indices += duplicate.index
        duplicate
      }
    }
  }

  /**
    * Load a model with the appropriate loader
    *
    * @param path to model file
    * @return a shape
    */
  def loadModel(path: String): Shape = {
    Logger.logTime(s"loaded model $path", this) {
      if (path.endsWith(".obj")) {
        WavefrontLoader.loadModel(path)
      } else if (path.endsWith(".dae")) {
        StaticColladaLoader.loadModel(path)
      } else {
        throw new IllegalArgumentException("Invalid model file: " + path)
      }
    }
  }
}