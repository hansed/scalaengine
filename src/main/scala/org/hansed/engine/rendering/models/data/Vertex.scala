package org.hansed.engine.rendering.models.data

import org.joml.Vector3f

import scala.collection.mutable

/**
  * Contains data for a single vertex
  *
  * @param index      index of the vertex
  * @param position   position of the vertex
  * @param tangents   the tangents associated with this vertex
  * @param bitangents the bitangents associtaed with this vertex
  * @param skinData   the skinning data associated with the vertex
  * @author Hannes Sederholm
  */
case class Vertex(index: Int, position: Vector3f, tangents: mutable.Buffer[Vector3f] = mutable.Buffer[Vector3f](),
                  bitangents: mutable.Buffer[Vector3f] = mutable.Buffer[Vector3f](), var
                  skinData: Option[VertexSkinData] = None) {

  var textureIndex: Option[Int] = None
  var normalIndex: Option[Int] = None

  var duplicate: Option[Vertex] = None

  /**
    * Calculate the average tangent for this vertex
    * That is a combination of all the tangents of the triangles this vertex is a part of
    *
    * @return the tangent vector for this vertex
    */
  def averageTangent: Vector3f = average(tangents)

  /**
    * Calculate the average bitangent for this vertex
    * That is a combination of all the tangents of the triangles this vertex is a part of
    *
    * @return the tangent vector for this vertex
    */
  def averageBitangent: Vector3f = average(bitangents)

  /**
    * Calculate the average of a list of vectors
    *
    * @param vectors the vectors
    */
  private def average(vectors: mutable.Buffer[Vector3f]) = {
    val average = new Vector3f()

    vectors.foreach(average.add(_))

    if (average.length() != 0) {
      average.normalize()
    }
    average
  }

  /**
    * Add a tangent to the list of tangents that contribute to the average tangent of this vertex
    *
    * @param tangent new tangent
    */
  def addTangent(tangent: Vector3f, bitangent: Vector3f): Unit = {
    tangents += tangent
    bitangents += tangent
  }

  /**
    * Is this vertex fully defined, that is has a texture and normal specified
    *
    * @return if vertex is fully defined
    */
  def isSet: Boolean = textureIndex.isDefined && normalIndex.isDefined

  /**
    * Do the normal and texture index of this vertex match the given
    *
    * @param tx a texture index
    * @param nm a normal index
    * @return if matches
    */
  def equalsByTextureAndNormal(tx: Int, nm: Int): Boolean = textureIndex.forall(_ == tx) && normalIndex.forall(_ == nm)

}
