package org.hansed.engine.rendering.models.animated

import org.hansed.engine.core.resources.ResourceUser
import org.hansed.engine.rendering.resources.AnimatedModelResource
import org.hansed.util.GLUtil._
import org.lwjgl.opengl.GL11.{GL_TRIANGLES, GL_UNSIGNED_INT, glDrawElements}
import org.lwjgl.opengl.GL15._
import org.lwjgl.opengl.GL20.{glDisableVertexAttribArray, glEnableVertexAttribArray}
import org.lwjgl.opengl.GL30.glBindVertexArray

/**
  * Represents a model that has skinning data and an animation attached to it.
  *
  * @param key model key
  * @author Hannes Sederholm
  */

class AnimatedModel(key: String) extends ResourceUser[AnimatedModelResource](key) {


  loadData()

  /**
    * Load the data to the resource
    */
  def loadData(): Unit = {
    bind()
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, resource.ibo)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, resource.shape.indices, GL_STATIC_DRAW)

    storeAttribute(resource, 0, 3, resource.shape.vertices)
    storeAttribute(resource, 1, 2, resource.shape.textures)
    storeAttribute(resource, 2, 3, resource.shape.normals)
    storeAttribute(resource, 3, 3, resource.shape.tangents)
    storeAttribute(resource, 4, 3, resource.shape.bitangents)
    storeAttribute(resource, 5, 3, resource.shape.jointIds)
    storeAttribute(resource, 6, 3, resource.shape.weights)

    unbind()
  }


  /**
    * Set this resource as current, whatever that might mean depends on the type of the resource
    */
  override def bind(): Unit = {
    glBindVertexArray(resource.vao)
    resource.enabledAttributes.foreach(glEnableVertexAttribArray)
  }

  /**
    * Return from what bind does to the default state
    * Note that if the system related to the resource wasn't at default state before bind the state will be different
    */
  override def unbind(): Unit = {
    resource.enabledAttributes.foreach(glDisableVertexAttribArray)
    glBindBuffer(GL_ARRAY_BUFFER, 0)
    glBindVertexArray(0)
  }

  def draw(): Unit = {
    bind()
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, resource.ibo)
    glDrawElements(GL_TRIANGLES, resource.shape.vertexCount, GL_UNSIGNED_INT, 0)
    unbind()
  }

  override def loadResource(path: String): Option[AnimatedModelResource] = {
    Some(new AnimatedModelResource(path))
  }
}

