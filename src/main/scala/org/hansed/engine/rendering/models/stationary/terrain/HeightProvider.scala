package org.hansed.engine.rendering.models.stationary.terrain

/**
  * Used for giving heights to a terrain etc
  *
  * @author Hannes Sederholm
  */
trait HeightProvider {

  /**
    * Get heights at a given coordinate
    *
    * @param x coordinate
    * @param z coordinate
    * @return height at that point
    */
  def apply(x: Float, z: Float): Float = height(x, z)

  /**
    * Get heights at a given coordinate
    *
    * @param x coordinate
    * @param z coordinate
    * @return height at that point
    */
  def height(x: Float, z: Float): Float

}
