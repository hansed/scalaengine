package org.hansed.engine.content.map

import org.hansed.engine.content.map.Terrain._
import org.hansed.engine.rendering.textures.Material

import scala.util.Random

class GeneratedTerrain(seed: Long, material: Material, textureTiling: Float = 1.0f) extends Terrain(material, textureTiling) {
  private val rng = new Random(seed)

  private val amplitude = 10.0f + rng.nextInt(25)

  private val octaves = 2 + rng.nextInt(4)

  private val roughness = 0.01f + rng.nextFloat() * 0.1f

  private def interpolatedNoise(x: Float, z: Float): Float = {
    val floorX = x.toInt
    val floorZ = z.toInt

    val fracX = x - floorX
    val fracz = z - floorZ

    val v1 = smoothNoise(floorX, floorZ)
    val v2 = smoothNoise(floorX + 1, floorZ)
    val v3 = smoothNoise(floorX, floorZ + 1)
    val v4 = smoothNoise(floorX + 1, floorZ + 1)

    val i1 = interpolate(v1, v2, fracX)
    val i2 = interpolate(v3, v4, fracX)
    interpolate(i1, i2, fracz)
  }

  private def interpolate(a: Float, b: Float, blend: Float): Float = {
    val theta = blend * Math.PI
    val f = (1.0f - Math.cos(theta)).toFloat * 0.5f
    a * (1 - 0f - f) + b * f
  }

  /**
    * Use basic convolution to get a smooth height noise
    *
    * @param x x of desired point
    * @param z z of desired point
    */
  private def smoothNoise(x: Int, z: Int): Float = {
    var sum: Float = 0
    for {
      xOffset <- -1 to 1
      zOffset <- -1 to 1
    } {
      if (xOffset == 0 && zOffset == 0) {
        sum += noise(x, z) / 4.0f
      } else if (xOffset == 0 || zOffset == 0 || xOffset == 1 || zOffset == 1) {
        sum += noise(x + xOffset, z + zOffset) / 16.0f
      } else {
        sum += noise(x + xOffset, z + zOffset) / 8.0f
      }
    }
    sum
  }

  private def noise(x: Int, z: Int) = {
    rng.setSeed(x * 49632 + z * 325176 + seed)
    rng.nextFloat() * 2.0f - 1.0f
  }

  /**
    * Get the height of the terrain at a given point
    *
    * Defaults to 0
    *
    * @param x the x coordinate
    * @param z the z coordinate
    * @return
    */
  override def height(x: Float, z: Float): Float = {
    val xFloat = x / TERRAIN_SQUARE_SIZE
    val zFloat = z / TERRAIN_SQUARE_SIZE
    var total = 0.0f
    val d = Math.pow(2, octaves - 1).toFloat
    for (i <- 0 until octaves) {
      val freq = Math.pow(2, i).toFloat / d
      val amp = Math.pow(roughness, i).toFloat * amplitude
      total += interpolatedNoise(xFloat * freq, zFloat * freq) * amp
    }
    total
  }
}
