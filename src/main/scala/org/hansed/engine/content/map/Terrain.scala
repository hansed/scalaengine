package org.hansed.engine.content.map

import org.hansed.engine.physics.PhysicsObject
import org.hansed.engine.physics.PhysicsObject.StaticObject
import org.hansed.engine.rendering.models.stationary.ModelComponent
import org.hansed.engine.rendering.models.stationary.terrain.{HeightProvider, TerrainModel}
import org.hansed.engine.rendering.shaders.StaticShader
import org.hansed.engine.rendering.textures.Material

abstract class Terrain(material: Material, textureTiling: Float = 1.0f) extends PhysicsObject() with HeightProvider with StaticObject {

  addComponent(new ModelComponent(new TerrainModel(this, textureTiling), material))

  // Enable static shader
  shaders += StaticShader.key

}

object Terrain {
  val TERRAIN_SQUARE_SIZE = 10.0f
  val TERRAIN_SIZE: Int = 1024
  val TERRAIN_VERTEX_COUNT: Int = (TERRAIN_SIZE / TERRAIN_SQUARE_SIZE).toInt
}
