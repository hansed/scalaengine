package org.hansed.engine.content.actors

import org.hansed.engine.core.GameObject
import org.hansed.engine.physics.movement.FreeKeyboardMovement


/**
  * An object that can be used as a 1st person player that uses ray tracing to interact with other objects
  *
  * @author Hannes Sederholm
  */
class RayPlayer extends GameObject {

  addComponent(new FreeKeyboardMovement)

}
