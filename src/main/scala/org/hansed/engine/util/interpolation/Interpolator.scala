package org.hansed.engine.util.interpolation

import org.joml.Vector3f

trait Interpolator[T] {

  /**
    * Interpolate between two elements of type T
    *
    * @param a        first element (start)
    * @param b        second element (end)
    * @param progress the progress
    */
  def interpolate(a: T, b: T, progress: Float): T

}

object Interpolator {

  implicit object FloatInterpolator extends Interpolator[Float] {
    override def interpolate(a: Float, b: Float, progress: Float): Float = {
      a * (1.0f - progress) + b * progress
    }
  }

  implicit object IntegerInterpolator extends Interpolator[Int] {
    override def interpolate(a: Int, b: Int, progress: Float): Int = {
      (a.toFloat * (1.0f - progress) + b.toFloat * progress).toInt
    }
  }

  implicit class VectorInterpolator(targetVector: Vector3f) extends Interpolator[Vector3f] {
    override def interpolate(a: Vector3f, b: Vector3f, progress: Float): Vector3f = a.lerp(b, progress, targetVector)
  }

  /**
    * Animate an interpolation between two values
    *
    * @param a            first value (start)
    * @param b            second value (end)
    * @param executor     gets called at most once every tickLength with interpolated value
    * @param length       length of animation (ms)
    * @param tickLength   length of tick (defaults to 100ms)
    * @param interpolator the interpolator for T
    * @tparam T type of a and b
    */
  def animate[T](a: T, b: T, executor: T => Unit, length: Long, tickLength: Long = 100, onFinish: () => Unit = () => ())(implicit interpolator: Interpolator[T]): Unit = {
    new Thread(() => {
      val start = System.currentTimeMillis()
      var progress = 0.0f
      var lastExecution = 0L
      do {
        val time = System.currentTimeMillis()
        progress = (time - start).toFloat / length
        if (time - lastExecution > tickLength) {
          executor(interpolator.interpolate(a, b, progress))
          lastExecution = time
        }
        Thread.sleep(1)
      } while (progress < 1)
      executor(interpolator.interpolate(a, b, 1.0f))
      onFinish()
    }).start()
  }

}
