package org.hansed.engine.util.grid

import org.hansed.engine.core.GameObject
import org.hansed.engine.core.eventSystem.events.Event
import org.hansed.engine.core.eventSystem.{EventListener, Events}
import org.hansed.engine.rendering.models.Model
import org.hansed.engine.util.grid.GridEvents.OutOfGrid
import org.joml.Vector3f

import scala.collection.mutable

/**
  * Represents a grid like structure and provides methods for controlling things in it
  *
  * @param columns number of columns in the grid
  * @param rows    number of rows in the grid
  * @param x       position in the world
  * @param z       position in the world
  * @param floors  height of the world
  * @param y       position in the world defaults to 0
  * @author Hannes Sederholm
  */
class Grid(val columns: Int, val rows: Int, x: Float, z: Float, val floors: Int = 1, gridSize: Float = 1.0f, y: Float
= 0.0f, displayFloor: Boolean = false) extends GameObject with EventListener {

  /**
    * Provides convenient access to the children by coordinate
    */
  private val _children = Array.fill(columns, rows, floors)(mutable.HashSet[GameObject]())

  /**
    * Create a slab floor for the grid on request
    */
  if (displayFloor) {
    for {
      x <- 0 until columns
      z <- 0 until rows
    } {
      val child = new Model("slab")
      child.position = new Vector3f(this.x + x * gridSize, y, this.z + z * gridSize)
      addChild(child)
    }
  }

  position = new Vector3f(x, z, y)

  /**
    * Return true for valid coordinates
    *
    * @param x coordinate
    * @param z coordinate
    * @param y coordinate
    * @return whether coordinate is in grid
    */
  def validCoordinate(x: Int, z: Int, y: Int = 0): Boolean = {
    x >= 0 && x < columns && z >= 0 && z < rows && y >= 0 && y < floors
  }

  /**
    * Add a child to the grid at the given position
    *
    * @param child the child to add
    * @param x     the x coordinate on the grid
    * @param z     the y coordinate on the grid
    */
  def addChild(child: GameObject, x: Int, z: Int, y: Int = 0): Unit = {
    super.addChild(child)
    setPosition(child, x, z, y)
  }

  /**
    * Set the position of a child to a position in the grid
    *
    * @param child to be relocated
    * @param x     coordinate
    * @param z     coordinate
    * @param y     coordinate
    */
  def setPosition(child: GameObject, x: Int, z: Int, y: Int = 0): Unit = {
    if (!validCoordinate(x, z, y)) {
      //if the child goes out of grid let everyone know and then remove it
      Events.signal(OutOfGrid(child))
      removeChild(child)
    } else {
      _children(x)(z)(y).add(child)
      child.position = new Vector3f(x * gridSize, y * gridSize, z * gridSize)
    }
  }

  /**
    * Find the grid coordinate of a given game object, if the object isn't a direct child of the grid no coordinate
    * will be returned
    *
    * @param child the child
    * @return 3d coordinate in the grid
    */
  def gridCoordinate(child: GameObject): Option[(Int, Int, Int)] = {
    if (children.contains(child)) {
      val gridX = (child.localPosition.x / gridSize).toInt
      val gridZ = (child.localPosition.z / gridSize).toInt
      val floor = (child.localPosition.y / gridSize).toInt
      Some(gridX, gridZ, floor)
    } else {
      None
    }
  }

  /**
    * Remove a child from this grid
    *
    * @param child the child to remove
    * @param x     coordinate
    * @param z     coordinate
    * @param y     coordinate defaults to 0
    */
  def removeChild(child: GameObject, x: Int, z: Int, y: Int = 0): Unit = {
    removeChild(child)
    _children(x)(z)(y).remove(child)
  }

  /**
    * Attempt to locate the child in the grid and remove it
    *
    * @param child the child to remove
    */
  def remove(child: GameObject): Unit = {
    gridCoordinate(child).foreach({ case (x: Int, z: Int, y: Int) => removeChild(child, x, z, y) })
  }

  /**
    * Clear the grid of all its contents (doesn't include the floor)
    */
  def clear(): Unit = {
    val childSets = _children.flatten.flatten
    childSets.flatten.foreach(removeChild)
    childSets.foreach(_.clear())
  }

  /**
    * Move the child on the grid to a given square
    *
    * @param child the child to move
    * @param toX   the new x
    * @param toZ   the new z
    * @param toY   the new y defaults to 0
    */
  def moveChild(child: GameObject, toX: Int, toZ: Int, toY: Int = 0): Unit = {
    gridCoordinate(child).foreach({ case (x: Int, z: Int, y: Int) =>
      _children(x)(z)(y).remove(child)
      setPosition(child, toX, toZ, toY)
    })
  }

  private val emptySet = mutable.HashSet[GameObject]()

  /**
    * Return the children at a given position
    *
    * @param x coordinate
    * @param z coordinate
    * @param y coordinate
    */
  def allChildren(x: Int, z: Int, y: Int = 0): mutable.HashSet[GameObject] = {
    if (validCoordinate(x, z, y)) {
      _children(x)(z)(y)
    } else {
      emptySet
    }
  }

  /**
    * Deal with an event
    *
    * @param event an event sent by an EventDispatcher
    */
  override def processEvent(event: Event): Unit = {
    event match {
      case _ =>
    }
  }
}
