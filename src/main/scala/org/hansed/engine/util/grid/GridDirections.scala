package org.hansed.engine.util.grid

/**
  * Movement directions in a grid
  *
  * @author Hannes Sederholm
  */
object GridDirections {

  abstract case class Direction(xOffset: Int, zOffset: Int, yOffset: Int) {
    def left: Direction

    def right: Direction
  }

  object NORTH extends Direction(0, -1, 0) {
    override def left: Direction = WEST

    override def right: Direction = EAST
  }

  object SOUTH extends Direction(0, 1, 0) {
    override def left: Direction = EAST

    override def right: Direction = WEST
  }

  object EAST extends Direction(1, 0, 0) {
    override def left: Direction = NORTH

    override def right: Direction = SOUTH
  }

  object WEST extends Direction(-1, 0, 0) {
    override def left: Direction = SOUTH

    override def right: Direction = WEST
  }

}
