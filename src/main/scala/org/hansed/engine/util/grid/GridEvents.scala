package org.hansed.engine.util.grid

import org.hansed.engine.core.GameObject
import org.hansed.engine.core.eventSystem.events.Event

/**
  * Contains events that the Grid object might send or receive
  *
  * @author Hannes Sederholm
  */
object GridEvents {

  /**
    * Sent when a game object is requested to move or is placed outside of the grid
    *
    * @param gameObject the object leaving the grid
    */
  case class OutOfGrid(gameObject: GameObject) extends Event

}
