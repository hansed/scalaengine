package org.hansed.engine.util.timers

import org.hansed.engine.Flags
import org.hansed.engine.Flags.SHUTDOWN

import scala.collection.mutable

/**
  * Allows for registering concurrent executor timers
  *
  * @author Hannes Sederholm
  */
object Timers extends Thread {

  private val timers = mutable.Buffer[Timer]()

  private val queuedAdds = mutable.Buffer[Timer]()
  private val queuedRemoves = mutable.Buffer[Timer]()

  start()

  override def run(): Unit = {
    while (!Flags(SHUTDOWN)) {
      timers ++= queuedAdds
      queuedAdds.clear()
      timers --= queuedRemoves
      queuedRemoves.clear()
      val currentTime = System.currentTimeMillis()
      timers.foreach(_.tick(currentTime))
      Thread.sleep(1)
    }
  }

  def add(timer: Timer): Unit = {
    queuedAdds += timer
  }

  def remove(timer: Timer): Unit = {
    queuedAdds += timer
  }
}
