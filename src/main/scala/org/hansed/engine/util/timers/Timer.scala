package org.hansed.engine.util.timers

/**
  * Timer that may be added to the [[org.hansed.engine.util.timers.Timers]] Thread
  *
  * @param executor   called once every tick
  * @param tickLength minimum delay between two ticks
  * @author Hannes Sederholm
  */
class Timer(executor: () => Unit, tickLength: Long = 1000) {

  var lastTick = 0L

  def tick(currentTime: Long): Unit = {
    if (currentTime - lastTick > tickLength) {
      lastTick = currentTime
      executor()
    }
  }

}
