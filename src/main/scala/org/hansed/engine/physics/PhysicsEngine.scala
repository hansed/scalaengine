package org.hansed.engine.physics

import org.hansed.engine.core.components.EntityTracker
import org.hansed.engine.core.eventSystem.Events
import org.hansed.engine.core.{GameComponent, GameObject, Transform}
import org.hansed.engine.physics.PhysicsEvents.ApplyForce
import org.hansed.engine.physics.PhysicsObject.StaticObject
import org.hansed.engine.physics.collisions.Collider
import org.joml.Vector3f

import scala.collection.mutable

/**
  * Deals with moving physics entities and applying collisions to them
  *
  * @author Hannes Sederholm
  */
class PhysicsEngine() extends GameComponent {

  /**
    * Keep track of all the physics objects with collision shapes
    */
  private val objectTracker = new EntityTracker[PhysicsObject with Collider] {
    override def extract(transform: Transform[_]): Option[PhysicsObject with Collider] = {
      transform match {
        case o: PhysicsObject with Collider => Some(o)
        case _ => None
      }
    }
  }

  /**
    * Keep track of all the non static physics objects with collision shapes
    */
  private val colliderTracker = new EntityTracker[PhysicsObject with Collider] {
    override def extract(transform: Transform[_]): Option[PhysicsObject with Collider] = {
      transform match {
        case _: PhysicsObject with StaticObject => None
        case o: PhysicsObject with Collider => Some(o)
        case _ => None
      }
    }
  }

  private def objects: mutable.Set[PhysicsObject with Collider] = objectTracker.targets

  private def colliders: mutable.Set[PhysicsObject with Collider] = colliderTracker.targets


  /**
    * Called when the component is created. Should be used for initiating the component
    *
    * @param parent the parent object of this component
    */
  override def prepare(parent: GameObject): Unit = {
    parent.addComponent(colliderTracker)
    parent.addComponent(objectTracker)
  }

  /**
    * Called once every update tick for all components
    */
  override def update(): Unit = {
    colliders.foreach(physicsObject => {
      objects.foreach(otherObject => {
        val collisionData = physicsObject.intersect(otherObject)
        if (collisionData.collides) {
          collisionData.collisionNormal.foreach(normal => {
            Events.signal(ApplyForce(physicsObject, normal.negate(new Vector3f()), 1), instantly = true)
          })
        }
      })
    })
  }

}