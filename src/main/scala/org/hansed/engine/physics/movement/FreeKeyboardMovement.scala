package org.hansed.engine.physics.movement

import org.hansed.engine.core.eventSystem.events.Event
import org.hansed.engine.core.eventSystem.events.InputEvents.{KeyDown, KeyHold, KeyUp, MouseMove}
import org.hansed.engine.core.eventSystem.{EventListener, Events}
import org.hansed.engine.core.{GameComponent, GameObject, Transform}
import org.hansed.engine.physics.PhysicsEvents.{ApplyAngularForce, ApplyForce}
import org.hansed.engine.physics.force.{AngularImpact, ForceProvider}
import org.joml.Vector3f
import org.lwjgl.glfw.GLFW._

/**
 * A movement model where the player may freely move a game object using the keyboard
 *
 * @param movementForce             the magnitude of the force applied when moving
 * @param angularForce              the magnitude of the angular torque applied when rotating
 * @param mouseVerticalMultiplier   a multiplier applied when rotation done by mouse on vertical axis
 * @param mouseHorizontalMultiplier a multiplier applied when rotation done by mouse on horizontal axis
 * @author Hannes Sederholm
 */
class FreeKeyboardMovement(movementForce: Float = 0.01f, var angularForce: Float = 0.25f,
                           var mouseVerticalMultiplier: Float = 0.3f, var mouseHorizontalMultiplier: Float = 0.75f)
  extends GameComponent with EventListener {

  var middleMouseDown = false

  val forces = new ForceProvider(Map(
    "left" -> new Vector3f(-movementForce, 0, 0),
    "right" -> new Vector3f(movementForce, 0, 0),
    "up" -> new Vector3f(0, movementForce, 0),
    "down" -> new Vector3f(0, -movementForce, 0),
    "backward" -> new Vector3f(0, 0, movementForce),
    "forward" -> new Vector3f(0, 0, -movementForce)
  ))

  /**
   * Called when the component is created. Should be used for initiating the component
   *
   * @param parent the parent object of this component
   */
  override def prepare(parent: GameObject): Unit = {
    parent.addComponent(forces)
  }

  /**
   * Deal with an event
   *
   * @param event an event sent by an EventDispatcher
   */
  override def processEvent(event: Event): Unit = {
    event match {
      case event: MouseMove =>
        if (middleMouseDown) {
          parent.foreach(p => {
            Events.signal(
              ApplyAngularForce(p,
                AngularImpact(-angularForce * event.deltaX * mouseHorizontalMultiplier, Transform.Y_AXIS, global =
                  true)))
            Events.signal(
              ApplyAngularForce(p,
                AngularImpact(-angularForce * event.deltaY * mouseVerticalMultiplier, Transform.X_AXIS)))
          })
        }
      case event: KeyDown =>
        if (event.keyCode == GLFW_MOUSE_BUTTON_MIDDLE)
          middleMouseDown = true
      case event: KeyUp =>
        if (event.keyCode == GLFW_MOUSE_BUTTON_MIDDLE)
          middleMouseDown = false
      case event: KeyHold =>
        event.keyCode match {
          case GLFW_KEY_A =>
            parent.foreach(p => Events.signal(ApplyForce(p, forces("left"), 1), instantly = true))
          case GLFW_KEY_D =>
            parent.foreach(p => Events.signal(ApplyForce(p, forces("right"), 1), instantly = true))
          case GLFW_KEY_W =>
            parent.foreach(p => Events.signal(ApplyForce(p, forces("forward"), 1), instantly = true))
          case GLFW_KEY_S =>
            parent.foreach(p => Events.signal(ApplyForce(p, forces("backward"), 1), instantly = true))
          case GLFW_KEY_SPACE =>
            parent.foreach(p => Events.signal(ApplyForce(p, forces("up"), 1), instantly = true))
          case GLFW_KEY_LEFT_SHIFT =>
            parent.foreach(p => Events.signal(ApplyForce(p, forces("down"), 1), instantly = true))
          case GLFW_KEY_RIGHT =>
            parent.foreach(p =>
              Events.signal(ApplyAngularForce(p, AngularImpact(-angularForce, Transform.Y_AXIS, global = true))))
          case GLFW_KEY_LEFT =>
            parent.foreach(p =>
              Events.signal(ApplyAngularForce(p, AngularImpact(angularForce, Transform.Y_AXIS, global = true))))
          case GLFW_KEY_UP =>
            parent.foreach(p => {
              Events.signal(ApplyAngularForce(p, AngularImpact(angularForce, Transform.X_AXIS)))
            })
          case GLFW_KEY_DOWN =>
            parent.foreach(p => {
              Events.signal(ApplyAngularForce(p, AngularImpact(-angularForce, Transform.X_AXIS)))
            })
          case _ =>
        }
      case _ =>
    }
  }
}
