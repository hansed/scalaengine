package org.hansed.engine.physics.movement

import org.hansed.engine.core.{GameComponent, GameObject}

/**
  * Movement super component, extended by components that move their parent
  *
  * @author Hannes Sederholm
  */
trait Movement extends GameComponent {
  /**
    * Called when the component is created. Should be used for initiating the component
    *
    * @param parent the parent object of this component
    */
  override def prepare(parent: GameObject): Unit = {

  }
}


