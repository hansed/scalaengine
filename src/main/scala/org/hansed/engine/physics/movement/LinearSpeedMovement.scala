package org.hansed.engine.physics.movement

import org.hansed.engine.Engine
import org.joml.Vector3f

/**
  * A game component that moves its parent towards a target with a fixed speed in a straight line
  *
  * @param speed          speed of movement in units/second
  * @param threshold      how near the target do we need to get to finish
  * @param removeAtTarget = whether this component should be removed when it reaches the target
  * @param onTarget       called when the parent reached the target (only if removeAtTarget is true)
  * @author Hannes Sederholm
  */
class LinearSpeedMovement(val speed: Float, target: () => Vector3f, val threshold: Float = 0.01f,
                          onTarget: () => Unit = () => (), removeAtTarget: Boolean = true) extends Movement {

  /**
    * Move the parent object
    */
  override def update(): Unit = {
    parent.foreach(p => {
      val path = p.position.negate(new Vector3f()).add(target())
      if (path.length() <= threshold) {
        if (removeAtTarget) {
          onTarget()
          remove()
        }
      } else if (path.length() - speed * Engine.GAME_TICK / Engine.UNIT <= 0) {
        p.position = target()
        if (removeAtTarget) {
          onTarget()
          remove()
        }
      } else {
        p.moveLocal(path.normalize(speed * Engine.GAME_TICK / Engine.UNIT))
        p.lookAlong(path)
      }
    })
  }
}
