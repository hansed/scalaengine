package org.hansed.engine.physics.movement

import org.hansed.engine.core.eventSystem.events.Event
import org.hansed.engine.core.eventSystem.events.InputEvents.{KeyDown, KeyHold}
import org.hansed.engine.core.eventSystem.{EventListener, Events}
import org.hansed.engine.core.{GameComponent, GameObject, Transform}
import org.hansed.engine.physics.PhysicsEvents.{ApplyAngularForce, ApplyForce}
import org.hansed.engine.physics.force.{AngularImpact, ForceProvider}
import org.joml.Vector3f
import org.lwjgl.glfw.GLFW._

/**
  * A movement component that represents your typical 3d movement where you would walk around a solid terrain
  *
  * @param force        the magnitude of the force when moving forward/backward/left/right
  * @param jumpStrength the magnitude of the force applied when jumping, will be applied for a duration of 5 game ticks
  * @param turnForce    the magnitude of the force applied when turning left or right
  * @author Hannes Sederholm
  */
class BasicGameMovement(val force: Float = 0.01f, val jumpStrength: Float = 0.2f, turnForce: Float = 0.5f) extends
  GameComponent with EventListener {

  val forces = new ForceProvider(Map(
    "left" -> new Vector3f(-force, 0, 0),
    "right" -> new Vector3f(force, 0, 0),
    "backward" -> new Vector3f(0, 0, force),
    "forward" -> new Vector3f(0, 0, -force))
  )

  val jumpForce = new Vector3f(0, jumpStrength, 0)

  /**
    * Called when the component is created. Should be used for initiating the component
    *
    * @param parent the parent object of this component
    */
  override def prepare(parent: GameObject): Unit = {
    parent.addComponent(forces)
  }

  /**
    * Deal with an event
    *
    * @param event an event sent by an EventDispatcher
    */
  override def processEvent(event: Event): Unit = {
    event match {
      case event: KeyDown =>
        event.keyCode match {
          case GLFW_KEY_SPACE =>
            parent.foreach(p => Events.signal(ApplyForce(p, jumpForce, 5), instantly = true))
          case _ =>
        }
      case event: KeyHold =>
        event.keyCode match {
          case GLFW_KEY_W =>
            parent.foreach(p => Events.signal(ApplyForce(p, forces("forward"), 1), instantly = true))
          case GLFW_KEY_S =>
            parent.foreach(p => Events.signal(ApplyForce(p, forces("backward"), 1), instantly = true))
          case GLFW_KEY_A =>
            parent.foreach(p =>
              Events.signal(ApplyAngularForce(p, AngularImpact(turnForce, Transform.Y_AXIS, global = true))))
          case GLFW_KEY_D =>
            parent.foreach(p =>
              Events.signal(ApplyAngularForce(p, AngularImpact(-turnForce, Transform.Y_AXIS, global = true))))
          case _ =>
        }
      case _ =>
    }
  }
}
