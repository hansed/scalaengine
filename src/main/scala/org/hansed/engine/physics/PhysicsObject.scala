package org.hansed.engine.physics

import org.hansed.engine.core.eventSystem.EventListener
import org.hansed.engine.core.eventSystem.events.Event
import org.hansed.engine.core.{GameComponent, GameObject}
import org.hansed.engine.physics.PhysicsEvents.{ApplyAngularForce, ApplyForce, ApplyGlobalForce}
import org.hansed.engine.physics.PhysicsObject.StaticObject
import org.hansed.engine.physics.force.{AngularImpact, Impact}
import org.joml.Vector3f

import scala.collection.mutable

/**
  * Represents an object that interacts physically with other objects
  *
  * @param mass                the mass of the object
  * @param angularMass         the angular mass of the object
  * @param speedAttenuation    the attenuation of the speed of the object
  * @param rotationAttenuation the attenuation of the rotation of the object
  * @author Hannes Sederholm
  */
class PhysicsObject(var mass: Float = 1.0f, var angularMass: Float = 1.0f, var speedAttenuation: Float = 0.1f,
                    var rotationAttenuation: Float = 0.1f) extends GameObject with EventListener {

  if (mass == 0.0f) {
    throw new IllegalArgumentException("Can't have zero mass")
  }

  /**
    * The speed of movement by axis
    */
  val speed: Vector3f = new Vector3f()

  /**
    * The forces acting upon this object
    */
  val forces: mutable.HashSet[Impact] = mutable.HashSet()

  /**
    * The angular speed around localAxis
    */
  val localAngularSpeed: Vector3f = new Vector3f()

  /**
    * The angular speed around global axis
    */
  val globalAngularSpeed: Vector3f = new Vector3f()

  /**
    * The angular forces acting upon this object
    */
  val angularForces: mutable.HashSet[AngularImpact] = mutable.HashSet()


  addComponent(new GameComponent {
    override def update(): Unit = {

      //remove all the forces that are no longer active
      forces.filterInPlace(_.active)
      angularForces.filterInPlace(_.active)

      //apply attenuation
      speed.mul(1.0f - speedAttenuation)
      globalAngularSpeed.mul(1.0f - rotationAttenuation)
      localAngularSpeed.mul(1.0f - rotationAttenuation)

      //add the acceleration caused by active forces
      speed.add(Impact.sum(forces).div(mass))

      angularForces.foreach(f => {
        val normalizedForce = f.axis.normalize(f.magnitude / angularMass, new Vector3f())
        if (f.global) {
          globalAngularSpeed.add(normalizedForce)
        } else {
          localAngularSpeed.add(normalizedForce)
        }
      })

      //apply the speeds
      PhysicsObject.this.moveLocal(speed)

      //apply local rotations
      PhysicsObject.this.rotateX(localAngularSpeed.x)
      PhysicsObject.this.rotateY(localAngularSpeed.y)
      PhysicsObject.this.rotateZ(localAngularSpeed.z)

      //apply global locations
      PhysicsObject.this.rotateGlobalX(globalAngularSpeed.x)
      PhysicsObject.this.rotateGlobalY(globalAngularSpeed.y)
      PhysicsObject.this.rotateGlobalZ(globalAngularSpeed.z)

    }
  })

  /**
    * Deal with an event
    *
    * @param event an event sent by an EventDispatcher
    */
  override def processEvent(event: Event): Unit = {
    event match {
      case ApplyForce(target, force, duration) =>
        if (target.equals(this)) {
          forces += Impact(force, duration)
        }
      case ApplyAngularForce(target, force) =>
        if (target.equals(this)) {
          angularForces += force
        }
      case ApplyGlobalForce(force, duration) =>
        if (!isInstanceOf[StaticObject]) {
          forces += Impact(force, duration)
        }
      case _ =>
    }
  }
}

object PhysicsObject {

  /**
    * Specifies that an object is static and thus will not move. Which means collisions need not be checked
    */
  trait StaticObject

}
