package org.hansed.engine.physics

import org.hansed.engine.core.GameObject
import org.hansed.engine.core.eventSystem.events.Event
import org.hansed.engine.physics.force.AngularImpact
import org.joml.Vector3f

/**
  * Contains different events related to physics interactions
  *
  * @author Hannes Sederholm
  */
object PhysicsEvents {

  /**
    * Apply a force to a given object for a given number of game ticks
    *
    * @param target   the target to feel the force
    * @param force    the force to be applied
    * @param duration the number of game ticks the force should be active
    */
  case class ApplyForce(target: GameObject, force: Vector3f, duration: Long) extends Event

  /**
    * Apply a force to all on static physics objects
    *
    * @param force    the force
    * @param duration the duration of the force
    */
  case class ApplyGlobalForce(force: Vector3f, duration: Long) extends Event

  /**
    * Apply a force to a given object for a given number of game ticks to make it spin
    *
    * @param target the target to feel the force
    * @param force  the force to be applied
    */
  case class ApplyAngularForce(target: GameObject, force: AngularImpact) extends Event

}
