package org.hansed.engine.physics.force

import org.hansed.engine.Engine
import org.joml.Vector3f

/**
  * Represents an angular force around an axis for a given amount of time
  *
  * @param magnitude the magnitude of the force
  * @param axis      the axis around which the force is applied
  * @param global    whether or not the rotation should be applied on global axis or local
  * @param duration  the time the force will be active
  * @author Hannes Sederholm
  */
case class AngularImpact(magnitude: Float, axis: Vector3f, global: Boolean = false, duration: Long = 1) {

  /**
    * The ending time of this impact
    */
  val end: Long = Engine.time + duration

  /**
    * Whether this AngularImpact is still active
    *
    * @return false if the time for this impact has passed
    */
  def active: Boolean = end >= Engine.time
}
