package org.hansed.engine.physics.force

import org.hansed.engine.Engine
import org.joml.Vector3f

/**
  * Represents a single force that may be applied on an object
  *
  * @param force    the vector representing this force
  * @param duration number of engine ticks that this impact is active
  * @author Hannes Sederholm
  */
case class Impact(force: Vector3f, duration: Long) {

  val end: Long = Engine.time + duration

  /**
    * Whether this Impact is still active
    *
    * @return false if the time for this impact has passed
    */
  def active: Boolean = end >= Engine.time

  /**
    * Create a force to a given direction of a given magnitude
    *
    * @param direction the direction of the force
    * @param magnitude the size of the force
    * @return new force
    */
  def this(direction: Vector3f, magnitude: Float, duration: Long) = {
    this(direction.normalize(magnitude), duration)
  }

  /**
    * Create a force object
    *
    * @param xForce force along x axis
    * @param yForce force along y axis
    * @param zForce force along z axis
    * @return new force
    */
  def this(xForce: Float, yForce: Float, zForce: Float, duration: Long) = {
    this(new Vector3f(xForce, yForce, zForce), duration)
  }

  /**
    * Add two forces together
    *
    * @param other the other force
    * @return new force
    */
  def +(other: Impact): Impact = {
    Impact(force.add(other.force, new Vector3f()), duration)
  }

  /**
    * Create a new force that is exactly the opposite of this force
    *
    * @return negated force
    */
  def negated: Impact = {
    Impact(force.negate(new Vector3f()), duration)
  }

}

object Impact {

  /**
    * Return the sum of multiple forces
    *
    * @param impacts the forces to be added up
    * @return the sum force
    */
  def sum(impacts: Iterable[Impact]): Vector3f = {
    val result = new Vector3f()
    impacts.foreach(impact => result.add(impact.force))
    result
  }

}
