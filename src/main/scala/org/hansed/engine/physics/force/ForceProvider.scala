package org.hansed.engine.physics.force

import org.hansed.engine.core.GameComponent
import org.joml.Vector3f

/**
  * A component that may be used to easily access forces in relation to the parent object
  *
  * @author Hannes Sederholm
  */
class ForceProvider(forces: Map[String, Vector3f]) extends GameComponent {

  /**
    * Fetch a given force
    *
    * @param flag the name of  the force in the forces map
    * @return transformed force
    */
  def apply(flag: String): Vector3f = {
    new Vector3f(forces.getOrElse(flag, new Vector3f())).rotate(rotation)
  }

}
