package org.hansed.engine.physics.collisions

import org.hansed.engine.physics.PhysicsObject
import org.hansed.engine.physics.collisions.Collider.CollisionData
import org.hansed.util.Logger
import org.joml.{Quaternionf, Vector3f}

/**
  * Represents a collider that may be added to a [[org.hansed.engine.physics.PhysicsObject]]
  * to enabled collision detection
  *
  * @author Hannes Sederholm
  */
trait Collider {

  this: PhysicsObject =>

  /**
    * Check collision between two objects
    *
    * @param other other collider that collides with this
    */
  def intersect(other: Collider): CollisionData = {
    if (this != other) {
      (this, other) match {
        //Sphere with sphere
        case (sphere: PhysicsObject with SphereCollider, other: PhysicsObject with SphereCollider) =>
          //the distance between the outer edges of the spheres
          val distance = sphere.position.distance(other.position) - sphere.radius - other.radius
          if (distance <= 0) {
            val goal = other.position.negate(new Vector3f()).add(sphere.position)
            goal.normalize(distance)
            CollisionData(collides = true, collisionNormal = Some(goal))
          } else {
            CollisionData(collides = false)
          }
        //Sphere with plane
        case (sphere: PhysicsObject with SphereCollider, plane: PhysicsObject with PlaneCollider) =>
          intersectSphereWithPlane(sphere, plane)
        //Plane with sphere
        case (plane: PhysicsObject with PlaneCollider, sphere: PhysicsObject with SphereCollider) =>
          val collisionData = intersectSphereWithPlane(sphere, plane)
          collisionData.collisionNormal.foreach(_.negate)
          collisionData
        //Unimplemented
        case _ => Logger.err("Collision not implemented: " + this.getClass.getCanonicalName + " with "
          + other.getClass.getCanonicalName, this)
          CollisionData(collides = false)
      }
    } else {
      CollisionData(collides = false)
    }
  }

  private def intersectSphereWithPlane(sphere: PhysicsObject with SphereCollider,
                                       plane: PhysicsObject with PlaneCollider): CollisionData = {
    val result = plane.position.negate(new Vector3f())
    result.add(sphere.position)
    val distance = result.dot(plane.normal) - sphere.radius
    if (distance < 0 && distance > -sphere.radius) {
      result.set(plane.normal).normalize(distance)
      if (plane.isInfinite) {
        CollisionData(collides = true, collisionNormal = Some(result))
      } else {
        val positionInPlane = new Vector3f(plane.position).negate().add(result).add(sphere.position)
        positionInPlane.rotate(plane.rotation.invert(new Quaternionf()))
        if (Math.abs(positionInPlane.x) <= plane.width / 2 + sphere.radius
          && Math.abs(positionInPlane.y) <= plane.height / 2 + sphere.radius) {
          CollisionData(collides = true, collisionNormal = Some(result))
        } else {
          CollisionData(collides = false)
        }
      }
    } else {
      CollisionData(collides = false)
    }
  }

}

object Collider {

  case class CollisionData(collides: Boolean, collisionNormal: Option[Vector3f] = None)

}
