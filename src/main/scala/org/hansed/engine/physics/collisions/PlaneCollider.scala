package org.hansed.engine.physics.collisions

import org.hansed.engine.physics.PhysicsObject
import org.joml.Vector3f

/**
  * Represents a collider for a plane
  *
  * The origin of the plane is considered to be in the middle
  *
  * @author Hannes Sederholm
  */
trait PlaneCollider extends Collider {
  this: PhysicsObject =>

  /**
    * The normal vector of the plane
    */
  def normal: Vector3f = new Vector3f(0, 0, 1).rotate(rotation)

  /**
    * Check if the plane is infinite
    *
    * @return true if either width or height is infinite
    */
  def isInfinite: Boolean = width.isInfinity || height.isInfinity

  /**
    * The width of the plane
    */
  val width: Float

  /**
    * The height of the plane
    */
  val height: Float

}
