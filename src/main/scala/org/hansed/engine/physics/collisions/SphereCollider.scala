package org.hansed.engine.physics.collisions

import org.hansed.engine.physics.PhysicsObject

/**
  * Represents a collider of a sphere shape
  *
  * @author Hannes Sederholm
  */
trait SphereCollider extends Collider {

  this: PhysicsObject =>

  /**
    * The radius of the sphere
    *
    * @return the radius
    */
  def radius: Float
}

