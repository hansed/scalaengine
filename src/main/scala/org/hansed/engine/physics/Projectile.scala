package org.hansed.engine.physics

import org.hansed.engine.core.{GameComponent, GameObject}
import org.hansed.engine.physics.movement.Movement
import org.hansed.engine.rendering.models.Model

/**
  * A projectile
  *
  * @param movement    defines the path of the projectile
  * @param removeOnHit remove this from the parent on hit. We assume that the movement component is removed at target
  *                    so that we can detect that
  * @author Hannes Sederholm
  */
class Projectile(model: String, movement: Movement, removeOnHit: Boolean = true) extends GameObject {

  addChild(new Model(model))
  addComponent(movement)

  /**
    * Remove a component from this object
    *
    * If the removed component is the movement and the projectile is set to remove on movement we remove
    *
    * @param component to remove
    */
  override def removeComponent(component: GameComponent): Unit = {
    if (removeOnHit) {
      component match {
        case _: Movement =>
          remove()
        case _ =>
      }
    }
    super.removeComponent(component)
  }
}
