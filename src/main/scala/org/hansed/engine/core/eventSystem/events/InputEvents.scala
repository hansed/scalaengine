package org.hansed.engine.core.eventSystem.events

import org.hansed.engine.core.GameObject

/**
  * Contains all the input related events
  *
  * @author Hannes Sederholm
  */
object InputEvents {

  type KeyCode = Int

  /**
    * KeyDown event is sent when the user presses any key (this includes mouse buttons) while the GLFW window is active
    *
    * @param keyCode a keyCode matching one located in org.lwjgl.glfw.GLFW
    */
  case class KeyDown(keyCode: KeyCode) extends Event

  /**
    * KeyDown event is sent when the user releases any key (this includes mouse buttons) while the GLFW window is active
    *
    * @param keyCode a keyCode matching one located in org.lwjgl.glfw.GLFW
    */
  case class KeyUp(keyCode: KeyCode) extends Event

  /**
    * KeyHold event is sent on all input cycles after the user has pressed a key but hasn't yet released it
    * Includes mouse buttons
    *
    * @param keyCode a keyCode matching one located in org.lwjgl.glfw.GLFW
    */
  case class KeyHold(keyCode: KeyCode) extends Event

  /**
    * Sent when a text key is pressed and the window is active
    *
    * @param charCode the UTF-8 code of the character the key pressed represents
    */
  case class TextTyped(charCode: Int) extends Event

  /**
    * Sent on mouse move
    *
    * @param deltaX the distance covered on the x axis since last cycle
    * @param deltaY the distance covered on the y axis since last cycle
    */
  case class MouseMove(deltaX: Float, deltaY: Float) extends Event

  /**
    * Sent on mouse move
    *
    * @param x position on the x axis
    * @param y position on the y axis
    */
  case class CursorPosition(x: Float, y: Float) extends Event

  /**
    * Sent when the user scrolls the scroll wheel on their input device
    *
    * @param amount the amount of scroll that happened
    */
  case class MouseScroll(amount: Float) extends Event

  /**
    * Sent when the player clicks an object
    *
    * @param objectId id of the object
    * @param buttonId button pressed
    */
  case class ObjectIdClick(objectId: Int, buttonId: KeyCode) extends Event

  /**
    * Sent when an object receives a click and identifies itself
    *
    * @param gameObject the object click
    * @param buttonId   the button used
    */
  case class ObjectClick(gameObject: GameObject, buttonId: KeyCode) extends Event

}
