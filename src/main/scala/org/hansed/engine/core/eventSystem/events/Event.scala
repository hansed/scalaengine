package org.hansed.engine.core.eventSystem.events

/**
  * Any event that is passed through the event system
  *
  * @author Hannes Sederholm
  */
trait Event
