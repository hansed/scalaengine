package org.hansed.engine.core.eventSystem.events

import org.hansed.engine.core.{GameComponent, GameObject, GameScene}

/**
  * Contains events to do with the scene hierarchy
  *
  * @author Hannes Sederholm
  */
object HierarchyEvents {

  /**
    * Sent when component is removed from a parent
    *
    * @param component the component removed
    * @param parent    the ex parent object of the component
    */
  case class ComponentRemoved(component: GameComponent, parent: GameObject) extends Event

  /**
    * Sent when a component is added to a parent
    *
    * @param component the component added
    * @param parent    the parent of the component
    */
  case class ComponentAdded(component: GameComponent, parent: GameObject) extends Event

  /**
    * Sent when an object is added as a child to another object
    *
    * @param parentObject the object that was added to
    * @param childObject  the object that was added
    */
  case class ObjectAdded(parentObject: GameObject, childObject: GameObject) extends Event

  /**
    * Sent when an object is removed from another object
    *
    * @param parentObject the object that the object was removed from
    * @param childObject  the object that was removed
    */
  case class ObjectRemoved(parentObject: GameObject, childObject: GameObject) extends Event

  /**
    * Sent when the hierarchy scene is changed
    *
    * @param newScene the scene now in use
    */
  case class SceneSwapped(newScene: GameScene) extends Event

}
