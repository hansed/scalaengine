package org.hansed.engine.core.eventSystem

import org.hansed.engine.core.eventSystem.events.Event

/**
  * A trait for dealing with events
  *
  * @author Hannes Sederholm
  */
trait EventListener {

  /**
    * Deal with an event
    *
    * @param event an event sent by an EventDispatcher
    */
  def processEvent(event: Event): Unit

  /**
    * If this is true the events will be sent to this listener
    *
    * @return whether the listener wants to know about whats going on
    */
  def condition: Boolean = true

}
