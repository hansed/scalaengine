package org.hansed.engine.core.eventSystem

import org.hansed.engine.Flags
import org.hansed.engine.Flags.VERBOSE_DEBUG
import org.hansed.engine.core.eventSystem.events.Event
import org.hansed.engine.core.eventSystem.events.HierarchyEvents._
import org.hansed.engine.rendering.RenderEngine
import org.hansed.util.Logger

import scala.collection.mutable


/**
  * Events object allows for registering and listening for events. This may be used for anything from listening for
  * input to receiving notifications about an entity moving
  *
  * @author Hannes Sederholm
  */
object Events {

  /**
    * List of all the listeners know to the event handler
    */
  var listeners: mutable.HashSet[EventListener] = new mutable.HashSet()

  /**
    * A buffer of events that will be handled on next game tick
    */
  var eventBuffer: mutable.Buffer[Event] = mutable.Buffer()

  /**
    * A buffer that may be swapped into use so that no events go missed while handling other events
    */
  var swapBuffer: mutable.Buffer[Event] = mutable.Buffer()

  /**
    * Let the system know an event has occurred and add it to the processing queue
    *
    * @param event     the event
    * @param instantly if true, we won't wait for next game tick, but signal the event right away, beware of
    *                  deadlocking the engine with this
    */
  def signal(event: Event, instantly: Boolean = false): Unit = {
    Logger.log(event.toString, this, condition = () => Flags(VERBOSE_DEBUG))
    event match {
      case event: ComponentAdded =>
        event.component match {
          case listener: EventListener => listeners += listener
          case _ =>
        }
        // Only notify render engine if the entity is in the game
        event.parent.parent.foreach(_ => RenderEngine.updateObject(event.parent))
      case event: ObjectAdded =>
        listeners ++= event.childObject.collectEventListeners(mutable.Buffer())
        RenderEngine.updateObject(event.childObject)
      case event: ComponentRemoved =>
        event.component match {
          case listener: EventListener => listeners -= listener
          case _ =>
        }
        event.parent.parent.foreach(_ => RenderEngine.updateObject(event.parent))
      case event: ObjectRemoved =>
        listeners --= event.childObject.collectEventListeners(mutable.Buffer())
        RenderEngine.removeObject(event.childObject)
      case event: SceneSwapped =>
        listeners = mutable.HashSet() ++ event.newScene.rootObject.collectEventListeners(mutable.Buffer())
        RenderEngine.loadScene(event.newScene)
      case _ =>
    }

    if (instantly) {
      listeners.filter(_.condition).foreach(_.processEvent(event))
    } else {
      eventBuffer += event
    }
  }

  def processQueue(): Unit = {
    val tempBuffer = eventBuffer
    eventBuffer = swapBuffer
    swapBuffer = tempBuffer

    val activeListeners = listeners.filter(_.condition)

    activeListeners.foreach(listener => swapBuffer.foreach(listener.processEvent))
    swapBuffer.clear()
  }

}
