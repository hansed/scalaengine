package org.hansed.engine.core

import org.hansed.engine.rendering.camera.Camera
import org.hansed.engine.saving.Atmosphere
import org.hansed.engine.saving.DefaultSaveProtocol.AtmosphereLoader

/**
  * Represents a certain state in the game, like a menu etc.
  *
  * @author Hannes Sederholm
  */
case class GameScene(camera: Camera, rootObject: GameObject = new GameObject,
                     atmosphere: Atmosphere = AtmosphereLoader.config("default")) {

  assert(rootObject != null, "Cannot have a scene without a root object!")
  assert(camera != null, "Cannot have a scene without a camera!")

  rootObject.addChild(camera)

  /**
    * Update the root object
    */
  def update(): Unit = {
    rootObject.update()
  }
}
