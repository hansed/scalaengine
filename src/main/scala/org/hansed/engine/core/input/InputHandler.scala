package org.hansed.engine.core.input

import org.hansed.engine.core.eventSystem.Events
import org.hansed.engine.core.eventSystem.events.InputEvents._
import org.lwjgl.glfw.GLFW._
import org.lwjgl.glfw._

import scala.collection.mutable

/**
  * Used for processing GLFW input
  *
  * @author Hannes Sederholm
  */
object InputHandler {

  /*
   * Store the last cursor positions for components to access
   */
  var lastCursorX: Double = 0
  var lastCursorY: Double = 0

  val MOUSE_CURSOR_CALLBACK: GLFWCursorPosCallback = GLFWCursorPosCallback.create((_: Long, xPos: Double,
                                                                                   yPos: Double) => {
    val deltaX = xPos - lastCursorX
    val deltaY = yPos - lastCursorY
    lastCursorX = xPos
    lastCursorY = yPos
    Events.signal(MouseMove(deltaX.toFloat, deltaY.toFloat), instantly = true)
    Events.signal(CursorPosition(xPos.toFloat, yPos.toFloat), instantly = true)
  })

  val MOUSE_SCROLL_CALLBACK: GLFWScrollCallback = GLFWScrollCallback.create((_: Long, _: Double, scroll: Double) => {
    Events.signal(MouseScroll(scroll.toFloat), instantly = true)
  })

  private val downKeys = mutable.HashSet[KeyCode]()
  private val upKeys = mutable.HashSet[KeyCode]()
  private val heldKeys = mutable.HashSet[KeyCode]()

  val KEY_CALLBACK: GLFWKeyCallback = GLFWKeyCallback.create((_, key, _, action, _) => {
    if (action == GLFW_PRESS) {
      downKeys += key
    } else if (action == GLFW_RELEASE) {
      upKeys += key
    }
  })

  val TEXT_CALLBACK: GLFWCharCallback = GLFWCharCallback.create((_, key) => {
    Events.signal(TextTyped(key), instantly = true)
  })


  val MOUSE_BUTTON_CALLBACK: GLFWMouseButtonCallback = GLFWMouseButtonCallback.create((_, button, action, _)
  => {
    if (action == GLFW_PRESS) {
      downKeys += button
    } else if (action == GLFW_RELEASE) {
      upKeys += button
    }
  })

  /**
    * Send all collected events since the last calling of this method
    */
  def sendEvents(): Unit = {
    heldKeys --= upKeys
    heldKeys.foreach(key => Events.signal(KeyHold(key), instantly = true))
    downKeys --= upKeys
    //for all the new keys that went down, send the signal
    downKeys.filter(heldKeys).foreach(key => Events.signal(KeyDown(key), instantly = true))
    upKeys.foreach(key => Events.signal(KeyUp(key), instantly = true))
    heldKeys.clear()
    upKeys.clear()
    heldKeys ++= downKeys
  }
}
