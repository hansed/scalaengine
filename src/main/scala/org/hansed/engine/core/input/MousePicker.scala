package org.hansed.engine.core.input

import java.awt.Color
import java.awt.image.BufferedImage

import org.hansed.engine.Engine
import org.hansed.engine.core.eventSystem.events.Event
import org.hansed.engine.core.eventSystem.events.InputEvents.{KeyUp, ObjectClick}
import org.hansed.engine.core.eventSystem.{EventListener, Events}
import org.hansed.engine.core.{GameComponent, GameObject}
import org.hansed.engine.rendering.FrameBuffer
import org.hansed.engine.rendering.shaders.HitBoxShader
import org.lwjgl.BufferUtils
import org.lwjgl.glfw.GLFW._
import org.lwjgl.opengl.GL11._
import org.lwjgl.opengl.GL30.GL_COLOR_ATTACHMENT0

/**
  * Sends object click events when the player clicks the mouse over an object
  *
  * @author Hannes Sederholm
  */
object MousePicker extends GameComponent with EventListener {

  private val width: Int = Engine.windowSize._1
  private val height: Int = Engine.windowSize._2

  /**
    * If this is false no events will be sent and no frame updating will occur
    */
  var enabled = false

  /**
    * The minimum time in seconds between frame redraws
    */
  val updateDelay = 0.5f

  /**
    * Timestamp of the last frame update
    */
  var lastUpdate = 0L

  /**
    * The fbo used for rendering
    */
  val frameBuffer = new FrameBuffer("PickerBuffer", width, height)

  /**
    * A buffer of pixels
    */
  private val pixels = BufferUtils.createByteBuffer(width * height * 4)

  /**
    * The image used for picking
    */
  private val image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB)


  /**
    * Send a click event
    *
    * @param keyCode mouse button used
    */
  def click(keyCode: Int): Unit = {
    val x = InputHandler.lastCursorX.toInt
    val y = InputHandler.lastCursorY.toInt

    if (x >= width || x < 0 || y >= height || y < 0)
      return

    val pixel = image.getRGB(x, y)

    val r = (pixel >> 16) & 0xFF
    val g = (pixel >> 8) & 0xFF
    val b = pixel & 0xFF

    val id = r + g * 256 + b * 256 * 256

    if (id != 0) {
      Engine.scene.rootObject.forId(id).foreach((clicked: GameObject) => Events.signal(ObjectClick(clicked, keyCode)))
    }

  }

  /**
    * Checks if we need to update the frame and updates if needed
    *
    */
  def checkUpdate(): Unit = {
    if (System.nanoTime() - lastUpdate > updateDelay * Engine.UNIT) {
      updateFrame()
      lastUpdate = System.nanoTime()
    }
  }

  /**
    * Render the scene onto the fbo
    */
  def updateFrame(): Unit = {
    frameBuffer.bind()
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    HitBoxShader.renderAll()
    glReadBuffer(GL_COLOR_ATTACHMENT0)
    glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, pixels)
    frameBuffer.unbind()
    for {
      x <- 0 until width
      y <- 0 until height
    } {
      val i = (x + (width * y)) * 4
      val r = pixels.get(i) & 0xFF
      val g = pixels.get(i + 1) & 0xFF
      val b = pixels.get(i + 2) & 0xFF
      image.setRGB(x, height - (y + 1), new Color(r, g, b).getRGB)
    }
  }

  /**
    * Deal with an event
    *
    * @param event an event sent by an EventDispatcher
    */
  override def processEvent(event: Event): Unit = {
    event match {
      case event: KeyUp =>
        if (enabled) {
          /*
           * When clicks happen check if we need to update frame and send event
           */
          event.keyCode match {
            case GLFW_MOUSE_BUTTON_LEFT | GLFW_MOUSE_BUTTON_MIDDLE | GLFW_MOUSE_BUTTON_RIGHT =>
              checkUpdate()
              click(event.keyCode)
            case _ =>
          }
        }
      case _ =>
    }
  }
}
