package org.hansed.engine.core

/**
  * Represents a property of a [[GameObject]]
  *
  * @author Hannes Sederholm
  */
class GameComponent extends Transform[GameObject] {

  /**
    * It's useful to be able to run actions after the component has been added to a parent object
    *
    * @param p parent object
    */
  override def parent_=(p: GameObject): Unit = {
    super.parent_=(p)
    prepare(p)
  }

  /**
    * Removes and thus stops updating this component
    * In most cases also frees the component for GC as the parent will stop referencing it
    */
  def remove(): Unit = {
    parent.foreach(p => {
      p.removeComponent(this)
    })

  }

  /**
    * Called when the component is created. Should be used for initiating the component
    *
    * @param parent the parent object of this component
    */
  def prepare(parent: GameObject): Unit = {}

  /**
    * Called once every update tick for all components
    */
  def update(): Unit = {}

}
