package org.hansed.engine.core.components

import org.hansed.engine.core.eventSystem.EventListener
import org.hansed.engine.core.eventSystem.events.Event
import org.hansed.engine.core.eventSystem.events.HierarchyEvents.{ComponentAdded, ComponentRemoved, ObjectAdded,
  ObjectRemoved}
import org.hansed.engine.core.{GameComponent, Transform}

import scala.collection.mutable

/**
  * A component that tracks entities of a given type
  *
  * @author Hannes Sederholm
  */
abstract class EntityTracker[T] extends GameComponent with EventListener {

  /**
    * List of targets in the scene
    */
  val targets: mutable.Set[T] = mutable.HashSet[T]()

  /**
    * Collect the wanted type from a given transform
    *
    * @param transform the transform
    * @return Some(target) or None if can't extract; eg. the transform isn't of desired type
    */
  def extract(transform: Transform[_]): Option[T]

  /**
    * Add a transform to the target list if it is possible with the extract function
    *
    * @param transform the transform
    */
  def add(transform: Transform[_]): Unit = {
    extract(transform).foreach(targets += _)
  }

  /**
    * Remove a given transform from the target list
    *
    * @param transform the transform to remove
    */
  def remove(transform: Transform[_]): Unit = {
    extract(transform).foreach(targets -= _)
  }

  /**
    * Deal with an event
    *
    * @param event an event sent by an EventDispatcher
    */
  override def processEvent(event: Event): Unit = {
    event match {
      case event: ObjectAdded =>
        add(event.childObject)
      case event: ObjectRemoved =>
        remove(event.childObject)
      case event: ComponentAdded =>
        add(event.component)
      case event: ComponentRemoved =>
        remove(event.component)
      case _ =>
    }
  }
}
