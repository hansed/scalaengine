package org.hansed.engine.core.components

import org.hansed.engine.Engine
import org.hansed.engine.core.GameComponent

/**
  * A timer component that calls a tick method once every given seconds
  *
  * Has a resolution defined by the interval of update method
  *
  * @param delay the length of a tick in seconds
  * @author Hannes Sederholm
  */
abstract class Timer(val delay: Float) extends GameComponent {

  private var lastTick = 0L

  private val tickLength: Float = Engine.UNIT * delay

  /**
    * Calls the tick method if time since last tick is greater than the targeted tick length
    */
  override def update(): Unit = {
    if (System.nanoTime() - lastTick >= tickLength) {
      lastTick = System.nanoTime()
      tick()
    }
  }

  /**
    * Called when the timer ticks
    */
  def tick(): Unit
}
