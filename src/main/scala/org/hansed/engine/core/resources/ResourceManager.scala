package org.hansed.engine.core.resources

import java.util.concurrent.CopyOnWriteArrayList

import org.hansed.engine.Flags
import org.hansed.engine.Flags.RESOURCE_CLEAR_REQUIRED
import org.hansed.util.Logger

import scala.collection.mutable

/**
  * Manages the resources used by the program
  * Gets notified when a resource is cleaned by GC so that it can keep track of if a resource is still needed.
  * Allows the reuse of already loaded resources and improves memory usage and overall performance thanks to that.
  *
  * @author Hannes Sederholm
  */
object ResourceManager {

  type ResourceTypeName = String

  /**
    * Map resources by resource types so that we may use file paths as keys, as no same type of resource can be
    * different if loaded from the same file
    */
  private val resources = mutable.HashMap[ResourceTypeName, mutable.HashMap[String, Resource]]()

  /**
    * Resources waiting releasing
    */
  private val clearQueue = new CopyOnWriteArrayList[Resource]()

  /**
    * Store a [[Resource]] in the manager
    *
    * @param resource the resource to be stored
    */
  def addResource(resource: Resource): Unit = {
    resources.getOrElseUpdate(resource.typeName, mutable.HashMap()).put(resource.key, resource)
  }

  /**
    * Remove a a [[Resource]]. Called when a resource is unloaded
    *
    * @param resource the unloaded resource
    */
  def removeResource(resource: Resource): Unit = {
    resources.getOrElseUpdate(resource.typeName, mutable.HashMap()).remove(resource.key)
    clearQueue.add(resource)
    Flags(RESOURCE_CLEAR_REQUIRED) = true
  }

  /**
    * Clear the resources that have become obsolete
    */
  def clearResources(): Unit = {
    val count = clearQueue.size()
    clearQueue.forEach(_.clearResource())
    clearQueue.clear()
    Logger.log(s"Cleared $count resources", this)
  }

  /**
    * Checks if a [[Resource]] has already been loaded and returns it if it has
    *
    * @param typeName name of the resource type
    * @param key      identifier for the specific resource
    * @return a [[Resource]] wrapped in Option or None if this is the first time loading the resource (or it has been
    *         unloaded)
    */
  def getResource[T <: Resource](typeName: ResourceTypeName, key: String): Option[T] = {
    resources.get(typeName).flatMap(_.get(key)).map(_.asInstanceOf[T])
  }

  /**
    * Releases all of the resources stored. To be used by [[org.hansed.engine.Engine]] when the program is about to
    * close
    */
  def releaseAll(): Unit = {
    resources.values.foreach(_.values.foreach(_.clearResource()))
  }

}
