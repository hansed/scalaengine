package org.hansed.engine.core.resources

import java.io.File

import org.hansed.util.Logger
import spray.json.JsValue

import scala.collection.mutable

/**
  * A class that helps with loading json files
  *
  * @param inputRoot   root of the json files, all json files under this directory should represent T
  * @param parseConfig It seems to be impossible to use generic parameters to convert json to an object with the
  *                    library in use so this method does it without them
  * @tparam T type of config to be loaded
  * @author Hannes Sederholm
  */
class ConfigLoader[T](inputRoot: String, val parseConfig: JsValue => T) {

  val root: String = (if (inputRoot.endsWith("/")) inputRoot else
    inputRoot + "/").drop(if (inputRoot.startsWith("/")) 1 else 0)

  /**
    * Map of configs that were found during instance creation
    */
  val configs: mutable.HashMap[String, T] = mutable.HashMap[String, T]()

  //find the config files
  Logger.logTime("Loading configs from " + root, this) {
    loadJsonFiles()
  }

  /**
    * Get a set of all the possible config names of this loader
    *
    * @return set of config names
    */
  def configNames: Set[String] =
    configs.keys.map(path => path.substring(path.lastIndexOf("/") + 1, path.lastIndexOf("."))).toSet

  /**
    * Walks the file structure starting from the root directory, looking for json files and tries to load them as
    * configs of specified type
    */
  private def loadJsonFiles(): Unit = {
    ResourceLoader.findMatching(("""^\/""" + root + """.*\.json$""").r).foreach(path => {
      val relativePath = path.toString
        .replace(root, "")
        .replaceAllLiterally(File.separator, "/")
      try {

        configs += relativePath -> {
          Logger.logTime(s"Parsing $relativePath", this) {
            parseConfig(ResourceLoader.loadJson(path))
          }
        }
      } catch {
        case t: Throwable => throw new Exception("Error loading config: " + relativePath + " \n" + t.getMessage)
      }
    })
  }

  /**
    * Get a config
    *
    * @param key name of config
    * @return a loaded config
    */
  def config(key: String): T = {
    configs(s"/$key.json")
  }

}



