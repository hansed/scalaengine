package org.hansed.engine.core.resources

import java.awt.Font
import java.awt.image.BufferedImage
import java.io._
import java.net.URI
import java.nio.ByteBuffer
import java.nio.file.{FileSystems, Files, Path, Paths}
import java.util.Collections

import javax.imageio.ImageIO
import org.hansed.util.Logger
import org.lwjgl.BufferUtils
import spray.json._

import scala.collection.mutable
import scala.io.{BufferedSource, Source}
import scala.util.matching.Regex

/**
 * Contains various utility methods for dealing with files.
 *
 * @author Hannes Sederholm
 */
object ResourceLoader {

  /**
   * Represents a group of paths that may be used for loading resources
   */
  case class ResourcePool(root: URI) {

    /**
     * List of relative paths contained in this pool
     */
    val paths: mutable.HashSet[String] = mutable.HashSet()

    var path: Path =
      if (root.getPath.endsWith(".jar")) {
        FileSystems.newFileSystem(URI.create("jar:" + root), Collections.emptyMap[String, Object]()).getPath("/")
      } else {
        Paths.get(root)
      }

    Files.walk(path).filter(path => !path.toString.endsWith(".class")).forEach(path => {
      var relativePath = path.toString
        .replace(root.getPath.dropRight(if (root.getPath.endsWith(".jar")) 6 else 1), "")
        .replaceAllLiterally(File.separator, "/").replaceAll("/$", "")
      paths += relativePath
    })
  }

  /**
   * A list of pools that the user may freely add to
   */
  val pools: mutable.HashSet[ResourcePool] = mutable.HashSet()

  /**
   * Add the engine resource pool
   */
  pools += ResourcePool(getClass.getProtectionDomain.getCodeSource.getLocation.toURI)

  /**
   * Add the user resource pool
   */
  pools += ResourcePool(getClass.getResource("/").toURI)

  Logger.log(s"Loaded ${pools.size} resource pools with ${pools.map(_.paths.size).sum} resources", this)

  /**
   * Read a TrueType or OpenType font from a file
   *
   * @param path to font
   * @return loaded font
   */
  def loadFont(path: String): Font = Font.createFont(Font.TRUETYPE_FONT, getInputStream(path))

  /**
   * Convert a relative path string into an absolute Path object
   *
   * @param relative relative location to source root
   * @return absolute Path object
   */
  def getPath(relative: String): Path = {
    val cleanedPath = relative.replaceAll("^/|/$", "")
    val result = pools.find(_.paths.contains("/" + cleanedPath)).map(_.path.resolve(cleanedPath))
    if (result.isEmpty) {
      throw new FileNotFoundException("No matching path found in resource pools: " + relative)
    } else {
      result.get
    }
  }

  /**
   * Returns a set of paths that match given regex
   *
   * @param matcher the regex to match
   * @return unique paths matching given regex
   */
  def findMatching(matcher: Regex): Set[String] = {
    pools.flatMap(_.paths.filter(_.matches(matcher.regex))).toSet
  }


  /**
   * Create a new input stream of a path
   *
   * @param path path to resource
   * @return resource as input stream
   */
  def getInputStream(path: String): InputStream = {
    new BufferedInputStream(Files.newInputStream(getPath(path)))
  }

  /**
   * Resolve a path in the classpath
   *
   * @param relativePath      the path relative to classpath
   * @param isDirectory       is the file a directory? used when creating the file/directories
   * @param createDirectories whether or not to create missing directories
   * @param createFile        whehter or not to create missing file
   * @return the resolved path
   */
  def resolvePath(relativePath: String, isDirectory: Boolean, createDirectories: Boolean, createFile: Boolean): Path = {
    val path: Path = Paths.get(getClass.getResource("/").getPath).resolve(relativePath)
    val directory = if (isDirectory) path else path.getParent
    if (createDirectories && Files.notExists(directory)) Files.createDirectories(directory)
    if (!isDirectory && createFile) Files.createFile(path)
    path
  }

  /**
   * Use an object input stream
   *
   * Will close the stream after calling the callback
   *
   * @param relativePath path to file for the stream
   * @param callback     function that uses the stream
   * @tparam T return type for callback
   * @return Option[T], None if the stream can't be opened or there is an error during the callback
   */
  def useObjectInputStream[T](relativePath: String)(callback: ObjectInputStream => T): Option[T] = {
    val path = resolvePath(relativePath, isDirectory = false, createDirectories = true, createFile = false)
    if (Files.notExists(path)) return None
    val stream = new ObjectInputStream(new BufferedInputStream(Files.newInputStream(path)))
    var result: Option[T] = None
    try {
      result = Some(callback(stream))
      result
    } finally {
      stream.close()
      return result
    }
  }

  /**
   * Use an object output stream
   *
   * Will close the stream after calling the callback
   *
   * @param relativePath path to file for the stream
   * @param callback     function that uses the stream
   */
  def useObjectOutputStream(relativePath: String)(callback: ObjectOutputStream => Unit): Unit = {
    val path = resolvePath(relativePath, isDirectory = false, createDirectories = true, createFile = false)
    val stream = new ObjectOutputStream(new BufferedOutputStream(Files.newOutputStream(path)))
    try {
      callback(stream)
    } finally {
      stream.close()
    }
  }

  /**
   * Write a string into a file
   * Overwrites the whole file
   *
   * @param relativePath path to file
   * @param lines        to be file contents
   */
  def writeLines(relativePath: String, lines: String): Unit = {
    val path: Path = Paths.get(getClass.getResource("/").getPath).resolve(relativePath)
    if (!Files.exists(path))
      Files.createFile(path)
    Files.write(path, lines.getBytes)
  }


  /**
   * Use a resource, automatically closes the resource after the callback is called
   *
   * @param path     path of resource
   * @param callback a function that takes in a [[scala.io.BufferedSource]] and does something with it
   * @tparam T return type of the callback
   * @return result of callback
   */
  private def useResource[T](path: String, callback: BufferedSource => T): T = {
    val resource = Source.fromInputStream(getInputStream(path))
    try {
      val result = callback(resource)
      result
    } finally {
      resource.close()
    }
  }

  private def useInputStream[T](path: String, callback: InputStream => T): T = {
    val stream = getInputStream(path)
    try {
      val result = callback(stream)
      result
    } finally stream.close()
  }

  /**
   * Read a file one by one
   *
   * @param path     path to file
   * @param callback a function that uses the lines iterator
   */
  def readLines(path: String, callback: Iterator[String] => Unit): Unit = {
    useResource(path, source => callback(source.getLines()))
  }

  /**
   * Get the lines of a file as an array
   *
   * @param path path to file
   * @return array of strings containing the lines of the file
   */
  def lines(path: String): Array[String] = {
    useResource(path, _.getLines().toArray)
  }

  /**
   * Load an object from a file
   *
   * @param path path to file
   * @return object
   */
  def loadJson(path: String): JsValue = {
    ResourceLoader.lines(path).mkString("\n").parseJson
  }

  /**
   * Represents a image the data of which has been put into a [[ByteBuffer]]
   *
   * @param width  width of image in pixels
   * @param height height of image in pixels
   * @param buffer buffer of size width*height containing the data for each pixel
   * @author Hannes Sederholm
   */
  case class ByteBufferImage(width: Int, height: Int, buffer: ByteBuffer)

  /**
   * Load an image and convert it to [[ByteBufferImage]]
   *
   * @param path the path of image
   * @return ByteBufferImage
   */
  def readImageToByteBuffer(path: String): ByteBufferImage = {
    convertImageToByteBuffer(loadImage(path))
  }

  /**
   * Extract the bytes of an image into a byte buffer
   *
   * @param image buffered image
   * @return a ByteBufferImage containing the bytes of the image
   */
  def convertImageToByteBuffer(image: BufferedImage): ByteBufferImage = {
    val width = image.getWidth()
    val height = image.getHeight()

    val pixels = image.getRGB(0, 0, width, height, null, 0, width)
    val buffer = BufferUtils.createByteBuffer(width * height * 4)
    for {
      y <- 0 until height
      x <- 0 until width
    } {
      val pixel = pixels(y * width + x)
      buffer.put(((pixel >> 16) & 0xFF).toByte)
      buffer.put(((pixel >> 8) & 0xFF).toByte)
      buffer.put((pixel & 0xFF).toByte)
      if (image.getColorModel.hasAlpha) {
        buffer.put(((pixel >> 24) & 0xFF).toByte)
      } else {
        buffer.put(0xFF.toByte)
      }
    }
    buffer.flip()
    ByteBufferImage(width, height, buffer)
  }

  /**
   * Read a buffered image
   *
   * @param path path to image
   * @return a buffered image
   */
  def loadImage(path: String): BufferedImage = {
    useInputStream(path, ImageIO.read)
  }

}
