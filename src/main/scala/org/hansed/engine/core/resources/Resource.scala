package org.hansed.engine.core.resources

import org.hansed.engine.core.resources.ResourceManager.ResourceTypeName

/**
  * Represents a resource that is used by a [[org.hansed.engine.core.resources.ResourceUser]]
  * These are automatically cleaned by garbage control and [[org.hansed.engine.core.resources.ResourceManager]] or
  * finally by
  * the [[org.hansed.engine.Engine]] on shutdown
  *
  * @author Hannes Sederholm
  * @param key an unique key for the type of the resource. This is used to identify whether the resource needs to be
  *            loaded or is already loaded
  */
abstract class Resource(val key: String) {

  /**
    * Number of references to this resource
    */
  var references = 0

  //let the resource manager know this resource exists, so that it can clean it on close
  ResourceManager.addResource(this)

  /**
    * Remove a reference to this resource
    *
    * @return if no references to this resource exist and it should be removed
    */
  def removeReference(): Boolean = {
    references -= 1
    references <= 0
  }

  /**
    * Increase the reference count of this resource
    */
  def addReference(): Unit = {
    references += 1
  }

  /**
    * Free the resources reserved for this resource
    */
  def clearResource(): Unit

  /**
    * Returns the TypeName of this resource
    */
  def typeName: ResourceTypeName = {
    getClass.getTypeName
  }
}
