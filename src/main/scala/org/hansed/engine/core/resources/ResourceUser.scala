package org.hansed.engine.core.resources

import java.lang.reflect.ParameterizedType
import java.util.UUID


/**
  * Represents a class that uses a [[org.hansed.engine.core.resources.Resource]]
  * Acts as an utility for automatically managing the resource
  *
  * @author Hannes Sederholm
  * @tparam T type of resource used
  * @param resourceKey unique key for a resource
  * @param unique      a boolean flag that allows forcing a new resource object for this user, this is useful in
  *                    certain cases
  */
abstract class ResourceUser[T <: Resource](resourceKey: String, unique: Boolean = false) {

  //try to find a cached resource
  private var resOption: Option[T] = ResourceManager.getResource[T](reduceClass(this.getClass)
    .getGenericSuperclass
    .asInstanceOf[ParameterizedType].getActualTypeArguments.head.getTypeName, resourceKey + (if
  (unique) UUID.randomUUID() else ""))

  //if no cached resource was found we need to load a new one
  if (resOption.isEmpty) {
    resOption = loadResource(resourceKey)
  }
  resOption.foreach(_.addReference())

  if (resOption.isEmpty) throw new IllegalStateException("Resource should always be set after resource user has been " +
    "constructed!")

  //extract the resource from option for convenience
  def resource: T = resOption.get

  /**
    * Loads the resource
    * Called when no cached resource was found
    *
    * @param key resource key
    * @return resource wrapped in Option
    */
  def loadResource(key: String): Option[T]

  /**
    * Set this resource as current, whatever that might mean depends on the type of the resource
    */
  def bind(): Unit

  /**
    * Return from what bind does to the default state
    * Note that if the system related to the resource wasn't at default state before bind the state will be different
    */
  def unbind(): Unit

  /**
    * Walk up the class hierarchy until we find what is the type that extends ResourceUser
    *
    * @param cIn highest class
    * @return lowest class
    */
  def reduceClass(cIn: Class[_]): Class[_] = {
    var c = cIn
    while (!c.getSuperclass.getCanonicalName.equals(classOf[ResourceUser[_]].getCanonicalName)) {
      c = c.getSuperclass
    }
    c
  }

  /**
    * If a resource user gets GC:d decrease the number of references to the resource used via resource manager
    */
  override def finalize(): Unit = {
    resOption.foreach(r => if (r.removeReference()) ResourceManager.removeResource(r))
  }

}



