package org.hansed.engine.core

import org.hansed.engine.core.Transform._
import org.hansed.util._
import org.joml.{Matrix4f, Quaternionf, Vector3f}

/**
  * Represents a transformation of a point in a space  (location, rotation, scale)
  *
  * The coordinate system is a right handed system where y is up x is right and z is back. This means that to move
  * "forward" you will have to move along the negative z axis
  *
  * @author Hannes Sederholm
  */
class Transform[T <: Transform[_]] {

  private val _position = new Vector3f()
  private val _rotation = new Quaternionf()
  private val _scale = new Vector3f(1, 1, 1)

  private var _parent: Option[T] = None

  /**
    * Rotate towards a direction, while ignoring the up axis
    *
    * @param direction direction to look to
    */
  def lookAlong(direction: Vector3f): Unit = {
    _rotation.identity().lookAlong(direction, Y_AXIS).invert()
  }

  /**
    * Look at a point in space
    *
    * @param point the point
    */
  def lookAt(point: Vector3f): Unit = {
    _rotation.lookAlong(position.negate(new Vector3f()).add(point).normalize(), Y_AXIS)
  }

  /**
    * Rotate around an axis
    *
    * @param angle angle of rotation in degrees
    * @param axis  axis of rotation
    */
  def rotate(angle: Float, axis: Vector3f): Unit = {
    if (Math.abs(angle) > precision)
      _rotation.rotateAxis(Math.toRadians(angle).toFloat, axis)
  }

  /**
    * Rotate around a global (that is world space) axis
    *
    * @param angle angle of rotation in degrees
    * @param axis  axis of rotation
    */
  def rotateGlobal(angle: Float, axis: Vector3f): Unit = {
    if (Math.abs(angle) > precision) {
      _rotation.set(new Quaternionf().rotateAxis(Math.toRadians
      (angle).toFloat, axis).mul(_rotation).normalize())
    }
  }

  /**
    * Rotates the transform around its own x axis
    *
    * @param angle angle of rotation in degrees
    */
  def rotateX(angle: Float): Unit = rotate(angle, X_AXIS)

  /**
    * Rotates the transform around its own y axis
    *
    * @param angle angle of rotation in degrees
    */
  def rotateY(angle: Float): Unit = rotate(angle, Y_AXIS)

  /**
    * Rotates the transform around its own z axis
    *
    * @param angle angle of rotation in degrees
    */
  def rotateZ(angle: Float): Unit = rotate(angle, Z_AXIS)

  /**
    * Rotates the transform around global (world space) x axis
    *
    * @param angle angle of rotation in degrees
    */
  def rotateGlobalX(angle: Float): Unit = rotateGlobal(angle, X_AXIS)

  /**
    * Rotates the transform around global (world space) x axis
    *
    * @param angle angle of rotation in degrees
    */
  def rotateGlobalY(angle: Float): Unit = rotateGlobal(angle, Y_AXIS)

  /**
    * Rotates the transform around global (world space) z axis
    *
    * @param angle angle of rotation in degrees
    */
  def rotateGlobalZ(angle: Float): Unit = rotateGlobal(angle, Z_AXIS)

  /**
    * Move to a direction
    *
    * @param axis  direction of movement
    * @param range how far along the axis should we move
    * @return the position after movement
    */
  def move(axis: Vector3f, range: Float): Vector3f = {
    if (Math.abs(range) > precision)
      _position.add(axis.rotate(_rotation, new Vector3f()).normalize(range))
    else _position
  }

  /**
    * Move this transforms to a relative position
    *
    * @param position new position
    * @return new position
    */
  def moveTo(position: Vector3f): Vector3f = {
    _position.set(position)
  }

  /**
    * Move to an abolute position in the world
    *
    * @param position the position
    * @return new local position
    */
  def moveToAbsolute(position: Vector3f): Vector3f = {
    _position.set(position.sub(parent.map(_.position).getOrElse(new Vector3f())))
  }

  /**
    * Move this transforms to a relative position
    *
    * @param x new relative x
    * @param y new relative y
    * @param z new relative z
    * @return new position
    */
  def moveTo(x: Float, y: Float, z: Float): Vector3f = {
    _position.set(x, y, z)
  }

  /**
    * Move along the local X axis by a range
    *
    * @param range distance to be covered
    * @return new position
    */
  def moveX(range: Float): Vector3f = move(X_AXIS, range)

  /**
    * Move along the local Y axis by a range
    *
    * @param range distance to be covered
    * @return new position
    */
  def moveY(range: Float): Vector3f = move(Y_AXIS, range)

  /**
    * Move along the local Z axis by a range
    *
    * @param range distance to be covered
    * @return new position
    */
  def moveZ(range: Float): Vector3f = move(Z_AXIS, range)

  /**
    * Moves the transforms without relative rotation applied
    *
    * @param xOffset movement along the global X axis
    * @param yOffset movement along the global Y axis
    * @param zOffset movement along the global Z axis
    * @return the position after movement
    */
  def offset(xOffset: Float, yOffset: Float, zOffset: Float): Vector3f = _position.add(xOffset, yOffset, zOffset)

  /**
    * Creates and calculates a transformation matrix representing this transform
    *
    * @return Transformation matrix
    */
  def matrix: Matrix4f = {
    new Matrix4f().translate(position).rotate(rotation).scale(scale)
  }

  /**
    * Return the transformation matrix of the parent of this transform or identity matrix if no parent is specified
    *
    * @return 4x4 transformation matrix
    */
  def parentRotation: Quaternionf = {
    parent.map(_.rotation).getOrElse(new Quaternionf())
  }

  /**
    * The result of this method may not be altered as it may reference the actual rotation object of this transform.
    * This is to reduce the number of new objects created and therefore improve performance.
    *
    * Returns the rotation of this transform relative to its parent
    *
    * @return the local rotation of this transform
    */
  def localRotation: Quaternionf = _rotation

  /**
    * The result of this method may not be altered as it may reference the actual rotation object of this transform.
    * This is to reduce the number of new objects created and therefore improve performance.
    *
    * @return the absolute rotation of this transform
    */
  def rotation: Quaternionf = _parent.map(_.rotation.mul(_rotation, new Quaternionf())).getOrElse(_rotation)

  /**
    * The result of this method may not be altered as it may reference the actual position object of this transform.
    * This is to reduce the number of new objects created and therefore improve performance.
    *
    * @return the absolute position of this transform
    */
  def position: Vector3f = _parent.map(_.position.add(_position, new Vector3f())).getOrElse(_position)

  /**
    * The position of this transform relative to its parent
    *
    * @return the local position vector
    */
  def localPosition: Vector3f = _position

  /**
    * Set the local position of this transform
    *
    * @param position new position
    */
  def position_=(position: Vector3f): Unit = _position.set(position)

  /**
    * Move this locally by offset
    *
    * @param offset the offset vector
    */
  def moveLocal(offset: Vector3f): Unit = _position.add(offset)

  /**
    * The result of this method may not be altered as it may reference the actual scale object of this transform.
    * This is to reduce the number of new objects created and therefore improve performance.
    *
    * @return the absolute scale of this transform
    */
  def scale: Vector3f = _parent.map(_.scale.mul(_scale, new Vector3f())).getOrElse(_scale)

  /**
    * Set the scale of this transform on x,y,z axis
    *
    * @param scale a vector that describes the new scaling on axis basis
    */
  def scale_=(scale: Vector3f): Unit = _scale.set(scale)

  /**
    * Set the parent of this transform
    *
    * @param parent new parent
    */
  def parent_=(parent: T): Unit = _parent = Some(parent)

  /**
    * Get the parent of this transform wrapped in Option
    *
    * @return Some(parent) or None if parent hasn't been set yet
    */
  def parent: Option[T] = _parent

  /**
    * Set the transformation of this to equal that of the given matrix
    *
    * @param transformation the new transformation
    */
  def set(transformation: Matrix4f): Unit = {
    transformation.getScale(_scale)
    transformation.getNormalizedRotation(_rotation)
    transformation.getTranslation(_position)
  }

}

object Transform {
  val X_AXIS = new Vector3f(1, 0, 0)
  val Y_AXIS = new Vector3f(0, 1, 0)
  val Z_AXIS = new Vector3f(0, 0, 1)
}
