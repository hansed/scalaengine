package org.hansed.engine.core

import org.hansed.engine.core.GameObject._
import org.hansed.engine.core.eventSystem.events.HierarchyEvents.{ComponentAdded, ComponentRemoved, ObjectAdded, ObjectRemoved}
import org.hansed.engine.core.eventSystem.{Clickable, EventListener, Events}

import scala.collection.mutable
import scala.reflect.ClassTag

/**
  * Represents an object in the world. Allows for describing the world as a graph
  *
  * @author Hannes Sederholm
  */
class GameObject extends Transform[GameObject] {

  /**
    * Child objects of this object
    */
  protected val children: mutable.Buffer[GameObject] = mutable.Buffer[GameObject]()

  /**
    * Components attached to this object
    */
  protected val components: mutable.Buffer[GameComponent] = mutable.Buffer[GameComponent]()

  /**
    * Components to be removed before next cycle
    */
  private val queuedRemoves = mutable.HashSet[GameComponent]()

  /**
    * Children to be removed before next cycle
    */
  private val queuedChildRemoves = mutable.HashSet[GameObject]()

  /**
    * Unique id for this object
    */
  val uniqueId: Int = nextId

  /**
    * List of shaders this object will render with
    */
  var shaders: mutable.Buffer[String] = mutable.Buffer[String]()

  /**
    * The id that should be encoded into the rendering of this game object to recognize the object clicked with
    */
  def clickId: Int = if (isInstanceOf[Clickable]) uniqueId else parent.map(_.clickId).getOrElse(-1)

  /**
    * Find the game object for a given id
    *
    * @param id the id of the object
    * @return the object or None if not found
    */
  def forId(id: Int): Option[GameObject] = {
    if (uniqueId == id) Some(this)
    else children.flatMap(_.forId(id)).headOption
  }

  /**
    * Add a child object to this. Child will have its parent set to this
    *
    * @param child a new child
    */
  def addChild(child: GameObject): Unit = {
    children += child
    child.parent = this
    Events.signal(ObjectAdded(this, child))
  }

  /**
    * Add a component to this object. The component will have its parent changed to this
    *
    * @param component a component to add
    */
  def addComponent(component: GameComponent): Unit = {
    components += component
    component.parent = this
    Events.signal(ComponentAdded(component, this))
  }

  /**
    * Recursively apply an function onto all of the components that are related to this object
    *
    */
  def onComponents(f: GameComponent => Unit): Unit = {
    children.foreach(_.onComponents(f))
    components.foreach(f)
  }

  /**
    * Update all children recursively and all of the components of each object
    */
  def update(): Unit = {
    components --= queuedRemoves
    children --= queuedChildRemoves
    queuedChildRemoves.foreach(c => Events.signal(ObjectRemoved(this, c)))
    queuedRemoves.foreach(c => Events.signal(ComponentRemoved(c, this)))
    queuedRemoves.clear()
    queuedChildRemoves.clear()
    children.foreach(_.update())
    components.foreach(_.update())
  }

  /**
    * Remove a component from this object
    *
    * @param component to remove
    */
  def removeComponent(component: GameComponent): Unit = queuedRemoves += component

  /**
    * Remove a child object from this object
    *
    * @param gameObject object to remove
    */
  def removeChild(gameObject: GameObject): Unit = queuedChildRemoves += gameObject

  /**
    * Does this object have the given component attached
    *
    * @param component component
    * @return if attached
    */
  def containsComponent(component: GameComponent): Boolean = components.contains(component)

  /**
    * Remove this object from its parent
    */
  def remove(): Unit = {
    parent.foreach(_.removeChild(this))
  }

  /**
    * Collect all the event listeners including this and any element below this element in the scene hierarchy
    *
    * @param listeners buffer of listeners that is to be filled
    * @return the given listeners buffer for convenience
    */
  def collectEventListeners(listeners: mutable.Buffer[EventListener]): mutable.Buffer[EventListener] = {
    this match {
      case listener: EventListener => listeners += listener
      case _ =>
    }
    children.foreach(_.collectEventListeners(listeners))
    components.foreach {
      case listener: EventListener => listeners += listener
      case _ =>
    }
    listeners
  }

  /**
    * Collect all the transforms of type T that the extractor turns into Some(transform) objects
    *
    * @param extractor  the extractor function
    * @param transforms the buffer of transforms
    * @tparam T the type of the transform
    * @return buffer of all the transforms in the tree matching the extractor
    */
  def collectTransforms[T](extractor: Transform[_] => Option[T],
                           transforms: mutable.Buffer[T] = mutable.Buffer[T]()): mutable.Buffer[T] = {
    children.foreach(child => {
      child.collectTransforms(extractor, transforms)
    })
    components.flatMap(component => extractor(component)).foreach(transforms += _)
    extractor(this).foreach(transforms += _)
    transforms
  }

  /**
    * Find the first instance of a component of the given type
    *
    * @tparam T component type
    * @return optional component
    */
  def findComponent[T: ClassTag]: Option[T] = {
    components.flatMap({
      case value: T => Some(value)
      case _ => None
    }).headOption
  }

  /**
    * Find all the components matching the given matcher
    *
    * @param matcher a function that returns true for the desired components
    * @return buffer of components
    */
  def findMatchingComponents(matcher: GameComponent => Boolean): mutable.Buffer[GameComponent] = {
    components.filter(matcher)
  }

  /**
    * Collect all components below this object in the scene graph
    *
    * @return a buffer of game components
    */
  def collectComponents(): mutable.Buffer[GameComponent] = {
    components ++ children.flatMap(_.collectComponents())
  }

  /**
    * Override equals to compare with the unique id as thats all we need
    *
    * @param other the other object
    * @return if other equals this
    */
  override def equals(other: scala.Any): Boolean = {
    other match {
      case other: GameObject => other.uniqueId == uniqueId
      case _ => false
    }
  }
}

object GameObject {

  private val iterator = Iterator.from(1)

  /**
    * Get an unique id
    *
    * @return an unique integer that is greater than the previous one
    */
  def nextId: Int = iterator.next()

}