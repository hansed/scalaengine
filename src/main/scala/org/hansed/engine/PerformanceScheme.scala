package org.hansed.engine

import org.hansed.engine.Engine.UNIT

/**
  * Describes how the engine functions
  *
  * This allows for on the fly changing of tick lengths to for example allow for speeding a game
  *
  * @author Hannes Sederholm
  */
case class PerformanceScheme(FPS: Int, TPS: Int, render: Boolean) {

  val tickLength: Long = UNIT / TPS

  val frameLength: Long = UNIT / FPS

}

object PerformanceScheme {

  val DEFAULT: PerformanceScheme = PerformanceScheme(60, 100, render = true)

  val DEFAULT_10_1000: PerformanceScheme = PerformanceScheme(10, 1000, render = true)

  val HEADLESS_100: PerformanceScheme = PerformanceScheme(60, 100, render = false)

  val HEADLESS_1000: PerformanceScheme = PerformanceScheme(60, 1000, render = false)

}