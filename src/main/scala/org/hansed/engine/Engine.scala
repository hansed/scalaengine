package org.hansed.engine

import org.hansed.engine.Flags._
import org.hansed.engine.core.eventSystem.Events
import org.hansed.engine.core.eventSystem.events.HierarchyEvents.SceneSwapped
import org.hansed.engine.core.eventSystem.events.InputEvents.CursorPosition
import org.hansed.engine.core.input.InputHandler
import org.hansed.engine.core.resources.ResourceManager
import org.hansed.engine.core.{GameObject, GameScene}
import org.hansed.engine.rendering.camera.Camera
import org.hansed.engine.rendering.{RenderEngine, Window}
import org.hansed.util.Logger
import org.joml.Matrix4f
import org.lwjgl.glfw.GLFW.glfwPollEvents
import org.lwjgl.opengl.GL11.{GL_COLOR_BUFFER_BIT, GL_DEPTH_BUFFER_BIT, glClear}

/**
  * The engine behind running everything
  *
  * Most of the methods here require init to be called before calling them and will throw an exception if this is not
  * the case
  *
  * @author Hannes Sederholm
  */
object Engine {

  /**
    * Nanosecond
    */
  val UNIT: Long = 1000000000

  /**
    * The current performance scheme in use
    */
  var performanceScheme: PerformanceScheme = PerformanceScheme.DEFAULT

  /**
    * Length of a game tick as unit defined by the UNIT value
    */
  val GAME_TICK: Long = performanceScheme.tickLength

  /**
    * Length of a frame tick as unit defined by the UNIT value
    */
  private val FRAME_TICK = performanceScheme.frameLength

  /**
    * The current engine time
    *
    * used for timing when we want to allow a change of game tick lengths on the fly
    */
  var time: Long = 0L

  /**
    * The current scene of the game
    */
  private var _scene: GameScene = _

  /**
    * The window used to display the game
    */
  var window: Option[Window] = None

  /**
    * Camera that the player watches the world through
    */
  def camera: Camera = {
    checkInit()
    scene.camera
  }

  /**
    * Returns the current scene
    * Throws an exception if the scene hasn't been set yet
    *
    * @return scene
    */
  def scene: GameScene = {
    checkInit()
    _scene
  }

  /**
    * Set the current scene
    *
    * @param scene new scene
    */
  def scene_=(scene: GameScene): Unit = {
    _scene = scene
    Events.signal(SceneSwapped(scene))
    //Just makes buttons hover when you switch between scenes and don't move cursor
    Events.signal(CursorPosition(InputHandler.lastCursorX.toFloat, InputHandler.lastCursorY.toFloat))
  }

  /**
    * Title of the window and name of the local storage folder
    */
  var title: String = "Game"

  /**
    * The time passed since the last update (at the time of update tick)
    * The unit is nanoseconds
    */
  var updateDelta: Long = 1

  /**
    * Initialize the engine and create a window to display it in
    *
    * @param windowWidth  width of the window
    * @param windowHeight height of the window
    * @param title        title of the window
    */
  @throws[IllegalStateException]
  def init(windowWidth: Int, windowHeight: Int, title: String): Unit = {
    if (window.nonEmpty) throw new IllegalStateException("Init may only be called once")
    this.title = title
    this.window = Some(new Window(windowWidth, windowHeight, title))
    window.foreach(_.update())
  }

  /**
    * Initialize the engine and create a window to display it in
    *
    * @param windowWidth  width of the window
    * @param windowHeight height of the window
    * @param title        title of the window
    */
  @throws[IllegalStateException]
  def defaultInit(windowWidth: Int, windowHeight: Int, title: String): GameScene = {
    init(windowWidth, windowHeight, title)
    scene = GameScene(new Camera(new Matrix4f().perspective(Math.toRadians(45.0).toFloat, windowWidth.toFloat /
      windowHeight, 0.01f, 500.0f)), new GameObject)
    scene
  }

  /**
    * Checks if init has been called and throws an exception if this isn't the case. Used by most methods in this class
    */
  @throws[IllegalStateException]
  def checkInit(): Unit = {
    if (window.isEmpty || _scene == null)
      throw new IllegalStateException("Init should have been called already.")
  }

  /**
    * Start running the game. The game loop runs on the thread this is called from so this will not return before the
    * game is closed
    */
  def run(): Unit = {
    checkInit()
    try {
      var lastRender: Long = System.nanoTime()
      var lastUpdate: Long = System.nanoTime()

      while (window.exists(!_.shouldClose) && !Flags(SHUTDOWN)) {
        val renderDelta = System.nanoTime() - lastRender
        updateDelta = System.nanoTime() - lastUpdate
        if (renderDelta >= FRAME_TICK) {
          glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
          Logger.logTime("Rendering scene", this, () => Flags(TICK_TIMERS)) {
            RenderEngine.render()
            window.foreach(_.update())
          }
          lastRender = System.nanoTime()
        }
        if (updateDelta >= GAME_TICK) {
          glfwPollEvents()
          Logger.logTime("Updating scene", this, () => Flags(TICK_TIMERS)) {
            scene.update()
          }
          InputHandler.sendEvents()
          lastUpdate = System.nanoTime()
          Events.processQueue()
          if (Flags(RESOURCE_CLEAR_REQUIRED)) {
            ResourceManager.clearResources()
            Flags(RESOURCE_CLEAR_REQUIRED) = false
          }
          //increment the engine clock
          time += 1
        }
        Thread.sleep(1)
      }
    } finally {
      Flags(SHUTDOWN) = true
      cleanUp()
    }
  }

  /**
    * Release all the resources allocated for the game
    */
  def cleanUp(): Unit = {
    window.foreach(_.cleanUp())
    ResourceManager.releaseAll()
  }

  /**
    * Fetch the size of the window currently used by the engine
    *
    * @return window size as a pair of Int values
    */
  def windowSize: (Int, Int) = {
    window.map(window => (window.width, window.height)).getOrElse((1, 1))
  }

}