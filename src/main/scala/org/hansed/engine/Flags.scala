package org.hansed.engine

import scala.collection.mutable

object Flags {

  type FlagType = String

  val SHUTDOWN: FlagType = "ENGINE_SHUTDOWN"

  val GAME_OVER: FlagType = "GAME_OVER"

  val DEBUG: FlagType = "DEBUG_ENABLED"

  val VERBOSE_DEBUG: FlagType = "VERBOSE_DEBUG_ENABLED"

  val RESOURCE_CLEAR_REQUIRED: FlagType = "RESOURCE_CLEAR"

  val RENDER_FRAMES: FlagType = "RENDER_FRAMES"

  val TICK_TIMERS: FlagType = "TICK_TIMERS"

  /**
    * A map of boolean flags for all the classes to use
    */
  private val _flags: mutable.HashMap[FlagType, Boolean] = mutable.HashMap[FlagType, Boolean]()


  /**
    * Get a flag value, defaults to false
    *
    * @param flag name of the flag
    * @return value of the flag or false if not set yet
    */
  def apply(flag: FlagType): Boolean = {
    _flags.getOrElseUpdate(flag, false)
  }

  /**
    * Set a flag
    *
    * @param flag  name of the flag
    * @param value new value
    */
  def update(flag: FlagType, value: Boolean): Unit = {
    _flags(flag) = value
  }

  _flags(RENDER_FRAMES) = true
  _flags(DEBUG) = true
}
