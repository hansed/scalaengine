package org.hansed.engine.saving

import org.joml.Vector3f

case class Atmosphere(fogColor: Vector3f, skyboxFogLow: Float, skyboxFogHigh: Float, terrainFogLow: Float,
                      terrainFogHigh: Float, ambientColor: Vector3f, ambientFactor: Float) {


}
