package org.hansed.engine.saving

import org.hansed.engine.core.resources.ConfigLoader
import org.joml.Vector3f
import spray.json._

/**
  * Deals with configuration and usage of the json library used
  *
  * @author Hannes Sederholm
  */
object DefaultSaveProtocol extends DefaultJsonProtocol {

  /**
    * Json format for parsing JOML vectors
    */
  implicit object VectorFormat extends JsonFormat[Vector3f] {

    override def read(json: JsValue): Vector3f = json match {
      case JsArray(Vector(JsNumber(x), JsNumber(y), JsNumber(z))) => new Vector3f(x.toFloat, y.toFloat, z.toFloat)
      case _ => deserializationError("Vector expected")

    }

    override def write(vector: Vector3f): JsValue = {
      JsArray(Vector(JsNumber(vector.x), JsNumber(vector.y), JsNumber(vector.z)))
    }
  }

  implicit val material: RootJsonFormat[MaterialDefinition] = jsonFormat3(MaterialDefinition)
  implicit val model: RootJsonFormat[ModelDefinition] = jsonFormat4(ModelDefinition)
  implicit val theme: RootJsonFormat[Theme] = jsonFormat10(Theme)
  implicit val atmosphere: RootJsonFormat[Atmosphere] = jsonFormat7(Atmosphere)

  /**
    * Loads the decorative objects
    */
  object ModelDefinitionLoader extends ConfigLoader[ModelDefinition]("/game/models/", _.convertTo[ModelDefinition])

  /**
    * Loads ui themes
    */
  object ThemeLoader extends ConfigLoader[Theme]("/game/themes/", _.convertTo[Theme])

  /**
    * Load atmosphere configs (fog etc.)
    */
  object AtmosphereLoader extends ConfigLoader[Atmosphere]("/game/atmospheres/", _.convertTo[Atmosphere])

  /**
    * Load materials
    */
  object MaterialLoader extends ConfigLoader[MaterialDefinition]("/game/materials/", _.convertTo[MaterialDefinition])

  /**
    * Make sure that floats are actually loaded in as floats and not integers
    */
  implicit object FloatFormat extends JsonFormat[Float] {

    def write(x: Float): JsNumber = JsNumber(BigDecimal.decimal(x).rounded)

    def read(value: JsValue): Float = DefaultJsonProtocol.FloatJsonFormat.read(value)
  }

}