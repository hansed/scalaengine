package org.hansed.engine.saving

import org.hansed.engine.rendering.textures.Material

/**
  * Defines the graphical representation of a 3d object
  *
  * @param model              if animated path to the model group, otherwise path to model of the object
  * @param value              if animated path to the animation, otherwise path to material of the object
  * @param animated           whether or not the object is animated
  * @param materialDefinition the material definition for this model, has priority over value material if defined
  * @author Hannes Sederholm
  */
case class ModelDefinition(model: String, value: String, animated: Boolean, materialDefinition: Option[MaterialDefinition]) {

  /**
    * Get the material defined in this model definition
    *
    * @return new material
    */
  def material: Material = {
    materialDefinition.map(new Material(_)).getOrElse(new Material(value))
  }
}
