package org.hansed.engine.saving

/**
  * A color theme used for drawing the UI
  *
  * @author Hannes Sederholm
  */
case class Theme(
                  outline: String,
                  primaryBackground: String,
                  secondaryBackground: String,
                  accent: String,
                  highlight: String,
                  foreground: String,
                  font: String,
                  fontSize: Int,
                  externalFont: Boolean,
                  cornerRadius: Int)
