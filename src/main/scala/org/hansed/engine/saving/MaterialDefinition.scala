package org.hansed.engine.saving

/**
  * Defines a material
  *
  * @param diffuse    path to diffuse color image
  * @param normal path to normal map image
  */
case class MaterialDefinition(diffuse: String, normal: Option[String], reflectivity: Float = 1.0f) {

  def normalPath: String = normal.getOrElse("textures/flatNormal.png")

}
